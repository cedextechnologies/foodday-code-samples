<?php

return [
    'whitelist' => [
        'admin.customers.updateStatus',
        'admin.users.updateStatus',
        'admin.item-categories.updateStatus',
        'admin.items.updateStatus',
        'admin.modifiers.create',
        'admin.modifiers.store',
        'item-categories.updateStatus',
        'item-categories.destroy',
        'admin.coupons.updateStatus',
        'admin.modifiers.destroy',
        'admin.faqs.updateStatus',
        'admin.reviews.updateStatus',
        'admin.reservations.updateStatus',
        'admin.item-size.create',
        'admin.item-size.store',
        'admin.sales-graph-data',
        'admin.cities.updateStatus',
        'cart.stripe.pay',
        'cart.paypal.get-url',
        'customer.order.confirmation',
        'cart.set-location',
        'customer.login',
        'customer.home',
        'admin.zones.custome-delivey-fee.create',
        'customer.zones.get-by-geo',
        'customer.zone.update',
        'admin.common-modifiers-to-item.create',
        'admin.common-modifiers-to-item.store'
    ],
];
