  <footer class="main-footer">
    <strong>&copy; 2019 <a href="https://foodday.co">{{ getStoreSettings('footer_text') }}</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> V4.0 
    </div>
  </footer>