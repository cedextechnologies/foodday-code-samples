  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{ route('admin.dashboard') }}" class="brand-link">
     <img src="{{ asset('images/logo.png') }}" alt="Foodday Logo" class="brand-image">
     <span class="brand-text font-weight-light"><img src="{{ asset('images/foodday_logo.png') }}" alt="Foodday Logo" class="brand-image"
      style="opacity: .8"></span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{ asset('dist/img/user2-160x160.jpg') }}" class="img-circle elevation-2" alt="User Image">
        </div>


        <div class="info">
          <a href="javascript:void(0);" class="d-block">{{ ucfirst($user->name) }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">

          @foreach($navigations as $navigation)

          @if(isset($navigation['subMenus']))

          <li class="nav-item has-treeview menu-close">
            <a href="#" class="nav-link">
              <i class="nav-icon {{ $navigation['icon'] }}"></i>
              <p>
                {{ $navigation['label'] }}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">

              @foreach($navigation['subMenus'] as $subMenu)
              <li class="nav-item">
                <a href="{{ $subMenu['url'] }}" class="navigation-lnk nav-link {{ Request::url()==$subMenu['url'] ? 'active sub-nav-active' : '' }}">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{ $subMenu['label'] }}</p>
                </a>
              </li>
              @endforeach
            </ul>
          </li>
          @else
          <li class="nav-item">
            <a href="{{ $navigation['url'] }}" class="nav-link {{ Request::url()==$navigation['url'] ? 'active' : '' }}">
              <i class="nav-icon {{ $navigation['icon'] }}"></i> <p>{{ $navigation['label'] }}</p>
            </a>
          </li>
          @endif

          @endforeach

          <li class="nav-item">
            <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();">
              <i class="nav-icon fas fa-power-off"></i><p>Logout</p>
            </a>

            <form id="logout-form-sidebar" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </li>
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>