
<div class="modal-dialog modal-lg">
  <div class="modal-content large-modal-content">
    {{ !empty($form)?$form:'' }}
    {{ !empty($slot)?$slot:'' }}
    <div class="modal-header">
      <h4 class="modal-title">{{ !empty($heading)?$heading:'' }}</h4>
      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
        <span aria-hidden="true">&times;</span>
      </button>
    </div>
    <div class="modal-body">
      {{ !empty($body)?$body:'' }}
    </div>
  </form>
  </div>
</div>
</div>
<!-- End Modal -->