<!-- Navigation -->
<div class="navigation fixed-top">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <nav id="navbar" class="navbar navbar-expand-lg navbar-light p-0">
                    <a class="navbar-brand p-0" href="index.html">
                        <img src="{{ asset('customer/assets/images/logo.png') }}" alt="Logo">
                    </a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse " id="navbarSupportedContent">
                        <ul class="navbar-nav ml-auto">
                            <li class="nav-item {{ Request::segment(1)==''?'active':'' }}">
                                <a class="nav-link" href="/">Home <span
                                    class="sr-only">(current)</span>
                                </a>
                            </li>
                            <li class="nav-item {{ Request::segment(1)=='items' || Request::segment(1)=='categories'?'active':'' }}">
                                <a class="nav-link" href="{{ route('items.index') }}">Menu</a>
                            </li>
                            <li class="nav-item {{ Request::segment(1)=='reservation'?'active':'' }}">
                                <a class="nav-link" href="{{ route('customer.reservation.create') }}">Reservation</a>
                            </li>
                            <li class="nav-item {{ Request::segment(1)=='faq'?'active':'' }}">
                                <a class="nav-link" href="{{ route('customer.faq.index') }}">Faq</a>
                            </li>
                            <li class="nav-item {{ Request::segment(1)=='contact'?'active':'' }}">
                                <a class="nav-link" href="{{ route('customer.contact.index') }}">Contact</a>
                            </li>
                            @auth
                            <li class="nav-item {{ Request::segment(1)=='account'?'active':'' }}">
                                <a class="nav-link" href="{{ route('customer.account.index') }}">Account</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();">Logout</a>
                                <form id="logout-form-sidebar" action="{{ route('logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                          </li>
                          @else
                          <li class="nav-item {{ Request::segment(2)=='login'?'active':'' }}">
                            <a class="nav-link" href="{{ route('customer.login') }}">Login</a>
                        </li>
                        <li class="nav-item {{ Request::segment(2)=='register'?'active':'' }}">
                            <a class="nav-link" href="{{ route('customer.register') }}">Sign up</a>
                        </li>
                        @endauth
                        <li class="nav-item cart">
                            <a class="nav-link" href="{{ route(
                            'customer.cart.index') }}">Cart<i class="fa fa-shopping-cart"
                            aria-hidden="true"></i><span class="cart-number">@if(isset($cartItemsCount)){{ $cartItemsCount }}@else 0 @endif</span>
                        </a>
                    </li>
                </ul>
            </div>
        </nav>
    </div>
</div>
</div>
</div>