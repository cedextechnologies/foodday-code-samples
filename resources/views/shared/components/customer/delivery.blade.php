<div class="row">
    <div class="col-md-12">
        <div class="p-4 ">
        	
        	@if(Session::get('deliveryLocation'))
        		<span class="zone-link">Your current choosen delivery location is </span>
        		<i class="fa fa-map-marker" aria-hidden="true"></i>	
        		<span class="red-link">{{ Session::get('deliveryLocation') }}</span>
      			&nbsp;&nbsp;&nbsp;
      			<i class="fa fa-edit" aria-hidden="true"></i>	
        		<a href="javascript:void(0);" class="blue-link red-link" data-toggle="modal" data-target="#zoneModal">Change Delivery Location</a>	
        	@else
        		<p class="zone-link">Please choose a delivery location from here!
        		<a href="javascript:void(0);" class="zone-link" data-toggle="modal" data-target="#zoneModal">Add Delivery Location</a>	        		
        	@endIf

        	</p>
         </div>
    </div>
</div>