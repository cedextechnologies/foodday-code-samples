<!-- Footer -->

<footer class="footer">
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="footer-block">
                        <h6>About us</h6>
                        <p>We are here with the dream of bringing a great food for the people around, Our Approaches reflects the people we serve, We are dedicated, passionate, methodical, deliver on promises and yes practical , and believe that a handshake seals the deal. It is with these principals that we approach every aspect of our business from the quality of our products, to the unparalleled service we provide our customers.
                        </p>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-block hours">
                        <h6>Opening hours</h6>
                        <p>Open For Breakfast, Lunch & Dinner</p>
                        <ul>
                            @foreach($storeHours as $storeHour)
                            <li>{{ $storeHour->getDay() }}<span>{{ $storeHour->status==0? 'Closed' : getTimeString($storeHour->open_hour).' - '.getTimeString($storeHour->close_hour)}}</span></li>
                            @endforeach
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="footer-block mb-4">
                        <h6>Contact us</h6>
                        Name: {{ getStoreSettings('contact_person') }}<br>
                        <a href="mailto:foodcart2019@gmail.com">Email: {{ getStoreSettings('contact_email') }}</a><br>
                        <a href="tel:09544123123">Phone: {{ getStoreSettings('contact_number') }}</a>
                    </div>
                    <div class="social-nav">
                        <ul>
                            <li><a href="{{ getStoreSettings('youtube') }}" target="blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('pinterest') }}" target="blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('instagram') }}" target="blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('twitter') }}" target="blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('facebook') }}" target="blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <div class="container">
            <p class="copyright">&copy; Copyright 2019 {{ getStoreSettings('footer_text') }}</p>
        </div>
    </div>
</footer>