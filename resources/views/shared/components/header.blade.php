  <nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
      </li>
    </ul>
    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" id="notification-class">
          <i class="far fa-bell"></i>
          <span class="badge badge-warning navbar-badge notification-count">0</span>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" id="notification-orders" onclick="javascript:$('#notification-class').attr('class','nav-link');window.location.href='{{ route('admin.orders') }}'">
          <div class="dropdown-divider"></div>
          <a href="#" class="dropdown-item">
            <i class="far fa-bell mr-2"></i><span class="notification-count badge badge-primary">0</span> New order received
          </a>
          <div class="dropdown-divider"></div>
        </div>
      </li>
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
          <i class="far fa-sun"></i>
        </a>
        <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
          <a href="{{ route('admin.profile') }}" class="dropdown-item">
            <i class="fas fa-user mr-2"></i>My Account
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{ route('admin.change_password') }}" class="dropdown-item">
            <i class="fas fa-shield-alt mr-1"></i>Change Password
          </a>
          <div class="dropdown-divider"></div>
          <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();" class="dropdown-item dropdown-footer"><b><i class="fas fa-power-off mr-1"></i>Logout</b></a>
        </div>
      </li>
    </ul>
  </nav>
