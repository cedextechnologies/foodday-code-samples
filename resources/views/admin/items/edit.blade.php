@extends('layouts.admin')
@section('title', 'Items')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.item-categories.index') }}">Items</a></li>
	<li class="breadcrumb-item active">Update Item</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Update Item') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.items.update',[$item]) }}" enctype="multipart/form-data">
				@csrf
				@method('PUT')
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Name') }}</label>
								<div class="col-sm-12">
									<input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name',$item->name) }}">
									@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Description') }}</label>
								<div class="col-sm-12">
									<textarea name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="10">{{ old('description',$item->description) }}</textarea>
									@if ($errors->has('description'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('description') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Item Type?') }}</label>
								<div class="col-sm-12">
									<div class="col-sm-12 {{ $errors->has('item_type') ? ' is-invalid' : '' }}">
										<span class="mr-2"><input type="radio" name="item_type" id="not_applicable" value="1" {{ old('item_type',$item->item_type)==1?'checked':'' }}>
											<label for="not_applicable">Not applicable</label>
										</span>
										<span class="ml-2 mr-2"><input type="radio" name="item_type" id="veggy" value="2" {{ old('item_type',$item->item_type)==2?'checked':'' }}>
											<label for="veggy">Veggy</label>
										</span>
										<span class="ml-3 mr-2"><input type="radio" name="item_type" id="spicy" value="3" {{ old('item_type',$item->item_type)==3?'checked':'' }}>
											<label for="spicy">Spicy</label>
										</span>
									</div>
									@if ($errors->has('item_type'))
									<span class="invalid-feedback" role="alert" style="display: block">
										<strong>{{ $errors->first('item_type') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Has Different Size ?') }}</label>
								<div class="col-sm-12">
									<select name="has_different_size" class="form-control{{ $errors->has('has_different_size') ? ' is-invalid' : '' }}" id="has_different_size">
										<option value="1" {{ old('has_different_size',$item->has_different_size)==1?'selected':'' }}>Yes</option>
										<option value="0" {{ old('has_different_size',$item->has_different_size)==0?'selected':'' }}>No</option>
									</select>
									@if ($errors->has('has_different_size'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('has_different_size') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group" id="price_div" style="{{ old('has_different_size',$item->has_different_size)==1?'display:none':'' }}">
								<label class="col-sm-4 control-label">{{ __('Price') }}</label>
								<div class="col-sm-12">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="">{{ getCurrencySymbol() }}</i></span>
										</div>
										<input type="text" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" value="{{ old('price',$item->price) }}">
										@if ($errors->has('price'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('price') }}</strong>
										</span>
										@endif
									</div>
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Category') }}</label>
								<div class="col-sm-12">
									<select name="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }} select2">
										<option value="" disabled selected>Select Category</option>
										@forelse($categories as $category)
										<option value="{{ $category->id }}" {{ old('category_id',$item->category_id)==$category->id?'selected':'' }}>{{ $category->name }}</option>
										@empty
										<option value="" disabled>Please add item categories</option>
										@endforelse
									</select>
									@if ($errors->has('category_id'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('category_id') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Featured?') }}</label>
								<div class="col-sm-12">
									<select name="is_featured" class="form-control{{ $errors->has('is_featured') ? ' is-invalid' : '' }}">
										<option value="1" {{ old('is_featured',$item->is_featured)==1?'selected':'' }}>Yes</option>
										<option value="0" {{ old('is_featured',$item->is_featured)==0?'selected':'' }}>No</option>
									</select>
									@if ($errors->has('is_featured'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('is_featured') }}</strong>
									</span>
									@endif
								</div>
							</div>
							
							<div class="form-group ">
								<img class="img-fluid mb-3 ml-2" width="50%" height="50%" id="image_upload_preview" src="{{ asset('storage/'.$item->image) }}"
								alt="Photo">
							</div>
							<small class="ml-2">Please upload an image with 400*300 ratio for better results</small>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Image') }}</label>
								<div class="col-sm-12">
									<div class="upload-btn-wrapper">
										<button class="upload-btn">Upload a file</button>
										<input type="file" name="image" id="image_upload" class="form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" />
									</div>
									@if ($errors->has('image'))
									<span class="invalid-feedback" role="alert" style="display:block !important;">
										<strong>{{ $errors->first('image') }}</strong>
									</span>
									@endif
								</div>
							</div>


							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Status') }}</label>
								<div class="col-sm-12">
									<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
										<option value="1" {{ old('status',$item->status)==1?'selected':'' }}>Active</option>
										<option value="0" {{ old('status',$item->status)==0?'selected':''}}>Inactive</option>
									</select>
									@if ($errors->has('status'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group" style="min-height: 20%;">
								<label for="inputEmail3" class="col-sm-4 control-label">Customize Dish</label>
								<div class="col-sm-12">
									<a href="#" class="btn btn-primary btn-sm btn-modal" data-toggle="modal" data-target="#large-modal" modal-url="admin.modifiers.create" modal-data="{{ $item->id }}"><i
										class="fa fa-plus mr-1"></i>Add Modifier</a>
									</div>
									@forelse($modifiers as $modifier)
									<div class="col-sm-12 mt-3">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th colspan="2">
														{{ $modifier->title }}
														<div style="float:right">
															<a href="#" data-toggle="modal" data-target="#large-modal" data-url="{{ route('admin.modifiers.edit',[$item,$modifier])}}"
															class="btn btn-primary btn-sm btn-edit-modifier"><i class="fas fa-edit"></i></a>
															<a href="#" class="btn btn-danger btn-sm delete-btn" delete-url="{{ route('admin.modifiers.destroy',[$item,$modifier]) }}" data-toggle="modal" data-target="#modal-delete"
															data-toggle="tooltip" data-placement="bottom" data-original-title="Delete"><i
															class="fa fa-trash"></i></a>
														</div>
													</th>
												</tr>
											</thead>

											<tbody>
												@foreach($modifier->ItemModifierDetails as $modifierItem)
												<tr>
													<td width="80%">{{ $modifierItem->name }}</td>
													<td width="40%">{{ format_currency($modifierItem->price) }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									@empty
									@endforelse

									<div id="size_div" @if($item->has_different_size==0) style="display:none" @endif>
										<hr class="ml-2 mr-2">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 control-label">Customize Dish Size</label>
											<div class="col-sm-12">
												<a href="#" class="btn btn-primary btn-sm btn-modal" data-toggle="modal" data-target="#large-modal" modal-url="admin.item-size.create" modal-data="{{ $item->id }}"><i
													class="fa fa-plus mr-1"></i>Add Size</a>
												</div>
											</div>
											<div class="col-sm-12 mt-3">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="40%">Size</th>
															<th width="30%">Price</th>
															<th width="30%" colspan="2">Action</th>
														</tr>
													</thead>
													<tbody>
														@forelse($item->sizes as $size)
														<tr>
															<td>{{ $size->size->size }}</td>
															<td>{{ format_currency($size->price) }}</td>
															<td>
																<a href="#" data-toggle="modal" data-target="#large-modal" data-url="{{ route('admin.item-size.edit',[$item,$size])}}"
																class="btn btn-primary btn-sm btn-edit-modifier"><i class="fas fa-edit"></i></a>
															</td>
															<td>
																<a href="#" class="btn btn-danger btn-sm delete-btn" delete-url="{{ route('admin.item-size.destroy',[$item,$size]) }}" data-toggle="modal" data-target="#modal-delete"
																data-toggle="tooltip" data-placement="bottom" data-original-title="Delete"><i
																class="fa fa-trash"></i></a>
															</td>
														</tr>
														@empty
														<tr>
															<td colspan="3">
																No records found
															</td>
														</tr>
														@endforelse
													</tbody>
												</table>
											</div>
										</div>
									</div>


							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Common Modifiers</label>
								<div class="col-sm-12">
									<a href="#" class="btn btn-primary btn-sm btn-modal" data-toggle="modal" data-target="#large-modal" modal-url="admin.common-modifiers-to-item.create" modal-data="{{ $item->id }}"><i
										class="fa fa-plus mr-1"></i>Add Modifier</a>
									</div>
									@forelse($commonModifiers as $commonModifier)
									<div class="col-sm-12 mt-3">
										<table class="table table-bordered">
											<thead>
												<tr>
													<th colspan="2">
														{{ $commonModifier->title }}
														<div style="float:right">
															<a href="#" data-toggle="modal" data-target="#large-modal" data-url="{{ route('admin.common-modifiers-to-item.edit',[$item,$commonModifier])}}"
															class="btn btn-primary btn-sm btn-edit-modifier"><i class="fas fa-edit"></i></a>
															<a href="#" class="btn btn-danger btn-sm delete-btn" delete-url="{{ route('admin.common-modifiers-to-item.destroy',[$item,$commonModifier]) }}" data-toggle="modal" data-target="#modal-delete"
															data-toggle="tooltip" data-placement="bottom" data-original-title="Delete"><i
															class="fa fa-trash"></i></a>
														</div>
													</th>
												</tr>
											</thead>

											<tbody>
												@foreach($commonModifier->itemModifierDetails as $modifierItem)
												<tr>
													<td width="80%">{{ $modifierItem->name }}</td>
													<td width="40%">{{ format_currency($modifierItem->price) }}</td>
												</tr>
												@endforeach
											</tbody>
										</table>
									</div>
									@empty
									@endforelse

									<div id="size_div" @if($item->has_different_size==0) style="display:none" @endif>
										<hr class="ml-2 mr-2">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 control-label">Customize Dish Size</label>
											<div class="col-sm-12">
												<a href="#" class="btn btn-primary btn-sm btn-modal" data-toggle="modal" data-target="#large-modal" modal-url="admin.item-size.create" modal-data="{{ $item->id }}"><i
													class="fa fa-plus mr-1"></i>Add Size</a>
												</div>
											</div>
											<div class="col-sm-12 mt-3">
												<table class="table table-bordered">
													<thead>
														<tr>
															<th width="40%">Size</th>
															<th width="30%">Price</th>
															<th width="30%" colspan="2">Action</th>
														</tr>
													</thead>
													<tbody>
														@forelse($item->sizes as $size)
														<tr>
															<td>{{ $size->size->size }}</td>
															<td>{{ format_currency($size->price) }}</td>
															<td>
																<a href="#" data-toggle="modal" data-target="#large-modal" data-url="{{ route('admin.item-size.edit',[$item,$size])}}"
																class="btn btn-primary btn-sm btn-edit-modifier"><i class="fas fa-edit"></i></a>
															</td>
															<td>
																<a href="#" class="btn btn-danger btn-sm delete-btn" delete-url="{{ route('admin.item-size.destroy',[$item,$size]) }}" data-toggle="modal" data-target="#modal-delete"
																data-toggle="tooltip" data-placement="bottom" data-original-title="Delete"><i
																class="fa fa-trash"></i></a>
															</td>
														</tr>
														@empty
														<tr>
															<td colspan="3">
																No records found
															</td>
														</tr>
														@endforelse
													</tbody>
												</table>
											</div>
										</div>
									</div>



								</div>
							</div>
						</div>
						<!-- /.card-body -->
						<div class="card-footer">
							<button type="submit" class="btn btn-success">Save</button>
						</div>
						<!-- /.card-footer -->
					</form>
				</div>
			</div>
		</div>
		@endsection