
<!-- /.modal -->
<form class="form-horizontal" method="POST" id="edit-modifier-form" action-url="{{ route('admin.item-size.update',[$item,$size]) }}">
  @csrf
  <div class="modal-header">
    <h4 class="modal-title">{{__('Edit Item Size')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-sm-8">
        <label class="col-sm-4 control-label">{{ __('Size') }}</label>
        <select class="form-control" name="size" id="size">
          @foreach($sizes as $itemSize)
          <option value="{{ $itemSize->id }}" {{ $itemSize->id==$size->size->id?'selected':'' }}>{{ $itemSize->size }}</option>
          @endforeach
        </select>
        <span class="invalid-feedback" role="alert" id="size_error" style="display: block">
          <strong class="error"></strong>
        </span>
      </div>
      <div class="col-sm-4">
        <label class="col-sm-4 control-label">{{ __('Price') }}</label>
        <input type="number" class="form-control ml-2" name="price" value="{{ $size->price }}" placeholder="Price" required>
        <span class="invalid-feedback" role="alert" id="price_error" style="display: block">
          <strong class="error"></strong>
        </span>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary" id="save-modifier" value="Save Item Size">
  </div>
</form>

<script type="text/javascript">

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

// Ajax wrapper
window.makeApiCall = function(type, url, data, callbackSuccess) {
  $.ajax({
    url: url,
    type: type,
    data: data,
    dataType: "json",
    success: function(response) {
      callbackSuccess(response);
    }
  });
}

$(document).ready(function() {
  $("#edit-modifier-form").submit(function(e){
    e.preventDefault();
    $('.error').text('');
    url=$(this).attr('action-url');
    makeApiCall('PUT', url, $(this).serialize(), function(response) {
      if(response.status === 422) {
        $.each(response.errors, function (key, val) {
          console.log(val);
          $("#" + key + "_error").find('strong').text(val[0]);
        });
      }
      if(response.status==1){
        location.reload();
      }
    });
  });
});
</script>