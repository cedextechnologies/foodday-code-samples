
<!-- /.modal -->
<form class="form-horizontal" method="POST" id="edit-modifier-form" action-url="{{ route('admin.modifiers.update',[$item,$modifier]) }}">
  @csrf
  <div class="modal-header">
    <h4 class="modal-title">{{__('Edit Modifier')}}</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-sm-4 control-label">{{ __('Title') }}</label>
          <div class="col-sm-12">
            <input type="text" name="title" class="form-control" value="{{ $modifier->title }}">
            <span class="invalid-feedback" role="alert" id="title_error" style="display: block">
              <strong class="error"></strong>
            </span>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Minimum</label>
          <div class="row">
            <div class="col-md-3 ml-2">
              <input type="text" name="minimum" class="form-control" value="{{ $modifier->minimum }}">
              <span class="invalid-feedback" role="alert" id="minimum_error" style="display: block">
                <strong class="error"></strong>
              </span>
            </div>
            <div class="col-dm-9 ml-3">Minimum no of items customer MUST choose. </div>
          </div>
        </div>
        <div class="form-group">
          <label class="col-sm-4 control-label">Maximum</label>
          <div class="row">
            <div class="col-md-3 ml-2">
              <input type="text" name="maximum" class="form-control" value="{{ $modifier->maximum }}">
              <span class="invalid-feedback" role="alert" id="maximum_error" style="display: block">
                <strong class="error"></strong>
              </span>
            </div>
            <div class="col-dm-9 ml-3">Maximum no of items customer CAN choose.</div>
          </div>
        </div>
        <hr>
        <a href="#" class="btn btn-primary btn-sm ml-2" id="add-modifier-rows"><i class="fa fa-plus"></i>Add More</a>
        <div class="form-group mt-3" id="modifiers">
            @foreach($modifier->ItemModifierDetails as $modifierItem)
            <div class="row mt-3">
            <div class="col-sm-8">
              <input type="text" class="form-control ml-2" name="item_name[]" placeholder="Item Name" value="{{ $modifierItem->name }}" required>
            </div>
            <div class="col-sm-3">
              <input type="number" class="form-control ml-2" name="item_price[]" placeholder="Price" value="{{ $modifierItem->price }}" required>
            </div>
            @if($loop->iteration!=1)
            <div class="col-sm-1">
              <span class="btn btn-danger btn-sm ml-2 btn-delete-row"><i class="fa fa-trash"></i></span>
            </div>
            @else
            <div class="col-sm-1"></div>
            @endif
            </div>
            @endforeach
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary" id="save-modifier" value="Save New Modifier">
  </div>
</form>

<script type="text/javascript">

  $.ajaxSetup({
    headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
  });

// Ajax wrapper
window.makeApiCall = function(type, url, data, callbackSuccess) {
  $.ajax({
    url: url,
    type: type,
    data: data,
    dataType: "json",
    success: function(response) {
      callbackSuccess(response);
    }
  });
}

$(document).ready(function() {
  $("#edit-modifier-form").submit(function(e){
    e.preventDefault();
    $('.error').text('');
    url=$(this).attr('action-url');
    makeApiCall('PUT', url, $(this).serialize(), function(response) {
      if(response.status === 422) {
        $.each(response.errors, function (key, val) {
          console.log(val);
          $("#" + key + "_error").find('strong').text(val[0]);
        });
      }
      if(response.status==1){
        location.reload();
      }
    });
  });

  $('#add-modifier-rows').click(function(){
    var markup='<div class="row mt-3"> <div class="col-sm-8"> <input type="text" class="form-control ml-2" name="item_name[]" placeholder="Item Name" required> </div> <div class="col-sm-3"> <input type="number" class="form-control ml-2" value="0" name="item_price[]" placeholder="Price" required> </div> <div class="col-sm-1 btn-delete-row"><span class="btn btn-danger btn-sm ml-2 btn-delete-row"><i class="fa fa-trash"></i></span></div></div>';

    $('#modifiers').append(markup);
  });

  $(document).on("click", ".btn-delete-row", function () {
    $(this).parent().parent().remove();
  });
});



</script>