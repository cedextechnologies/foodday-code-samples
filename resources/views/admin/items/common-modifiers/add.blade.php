
<!-- /.modal -->
<form class="form-horizontal" method="POST" id="add-common-modifier-to-item-form" form-data="{{ $item->id }}">
  @csrf
  <div class="modal-header">
    <h4 class="modal-title">Add New Modifier</h4>
    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
      <span aria-hidden="true">&times;</span>
    </button>
  </div>
  <div class="modal-body">
    <div class="row">
      <div class="col-md-12">
        <div class="form-group">
          <label class="col-sm-4 control-label">{{ __('Modifier') }}</label>
          <div class="col-sm-12">
            <select name="common_modifier" class="form-control">

                <option value="">Select</option>


                @foreach($commonModifiers as $key => $commonModifier)

                  <option value="{{ $commonModifier->id }}"> {{ $commonModifier->title }} </option>

                @endforeach
            
            </select>  
            <span class="invalid-feedback" role="alert" id="common_modifier_error" style="display: block">
              <strong class="error"></strong>
            </span>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="modal-footer justify-content-between">
    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
    <input type="submit" class="btn btn-primary" id="save-modifier" value="Save New Modifier">
  </div>
</form>

<script type="text/javascript">

$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

// Ajax wrapper
window.makeApiCall = function(type, url, data, callbackSuccess) {
  $.ajax({
    url: url,
    type: type,
    data: data,
    dataType: "json",
    success: function(response) {
      callbackSuccess(response);
    }
  });
}

$(document).ready(function() {
  $("#add-common-modifier-to-item-form").submit(function(e){
    e.preventDefault();
    $('.error').text('');
    url=route('admin.common-modifiers-to-item.store',$(this).attr('form-data'));
    makeApiCall('POST', url, $(this).serialize(), function(response) {
      if(response.status === 422) {
        $.each(response.errors, function (key, val) {
          console.log(val);
          $("#" + key + "_error").find('strong').text(val[0]);
        });
      }
      if(response.status==1){
        location.reload();
      }
    });
  });

  $('#add-modifier-rows').click(function(){
    var markup='<div class="row mt-3"> <div class="col-sm-8"> <input type="text" class="form-control ml-2" name="item_name[]" placeholder="Item Name" required> </div> <div class="col-sm-3"> <input type="number" class="form-control ml-2" value="0" name="item_price[]" placeholder="Price" required> </div> <div class="col-sm-1 btn-delete-row"><span class="btn btn-danger btn-sm ml-2 btn-delete-row"><i class="fa fa-trash"></i></span></div></div>';

    $('#modifiers').append(markup);
  });

  $(document).on("click", ".btn-delete-row", function () {
    $(this).parent().parent().remove();
  });
});



</script>