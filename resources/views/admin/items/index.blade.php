@extends('layouts.admin')
@section('title', 'Dish Items')


@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item active">Items</li>
</ol>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a></li>
	<li class="breadcrumb-item active">{{ __('Items') }}</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Items") }}</h3>
				<div class="card-tools"><a href="{{ route('admin.items.create') }}" class="btn btn-primary btn-sm"><i
					class="fa fa-plus mr-1"></i>Add New Item</a></div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped datatable">
							<thead>
								<tr>
									<th width="20%">{{ __("Name") }}</th>
									<th>{{ __("Description") }}</th>
									<th width="5%">{{ __("Category") }}</th>
									<th width="3%">{{ __("Featured?") }}</th>
									<th width="5%">{{ __("Enable/Disable") }}</th>
									<th width="5%">{{ __("Action") }}</th>
								</tr>
							</thead>
							<tbody>

								@forelse($items as $key => $item)
								<tr>
									<td>{{$item->name}}</td>
									<td>{{$item->description}}</td>
									<td>{{$item->itemCategory->name}}</td>
									<td>{{$item->isFeatured()}}</td>
									<td>
										<div class="form-group">
											<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
												<input type="checkbox" class="custom-control-input status-toggle" status-data="{{ $item->id }}" status-url="admin.items.updateStatus" id="customSwitch-{{$key}}" {{$item->isActive()?'checked':''}}>
												<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
											</div>
										</div>
									</td>
									<td><a href="{{route('admin.items.edit',[$item])}}" data-toggle="tooltip" title="Edit"><i
										class="fas fa-edit mr-3"></i></a>
										<a href="#" class="delete-btn" delete-url="{{ route('admin.items.destroy',[$item]) }}" data-toggle="modal" data-target="#modal-delete" title="Delete"><i
											class="fas fa-trash"></i></a>
										</td>
									</td>
								</tr>

								@empty
								<tr>
									<td colspan="6">No records found</td>
								</tr>
								@endforelse
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection