@extends('layouts.admin')
@section('title', 'Items')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.item-categories.index') }}">Items</a></li>
	<li class="breadcrumb-item active">Create Item</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Create Item') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.items.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Name') }}</label>
								<div class="col-sm-12">
									<input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}">
									@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Description') }}</label>
								<div class="col-sm-12">
									<textarea name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="10">{{ old('description') }}</textarea>
									@if ($errors->has('description'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('description') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Item Type?') }}</label>
								<div class="col-sm-12">
									<div class="col-sm-12 {{ $errors->has('item_type') ? ' is-invalid' : '' }}">
										<span class="mr-2"><input type="radio" name="item_type" id="not_applicable" value="1" {{ old('item_type')==1?'checked':'' }}>
											<label for="not_applicable">Not applicable</label>
										</span>
										<span class="ml-2 mr-2"><input type="radio" name="item_type" id="veggy" value="2" {{ old('item_type')==2?'checked':'' }}>
											<label for="veggy">Veggy</label>
										</span>
										<span class="ml-3 mr-2"><input type="radio" name="item_type" id="spicy" value="3" {{ old('item_type')==3?'checked':'' }}>
											<label for="spicy">Spicy</label>
										</span>
									</div>
									@if ($errors->has('item_type'))
									<span class="invalid-feedback" role="alert" style="display: block">
										<strong>{{ $errors->first('item_type') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Has Different Size ?') }}</label>
								<div class="col-sm-12">
									<select name="has_different_size" class="form-control{{ $errors->has('has_different_size') ? ' is-invalid' : '' }}" id="has_different_size">
										<option value="1" {{ old('has_different_size')==1?'selected':'' }}>Yes</option>
										<option value="0" {{ old('has_different_size')==0?'selected':'' }}>No</option>
									</select>
									@if ($errors->has('has_different_size'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('has_different_size') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group" id="price_div" style="{{ old('has_different_size')==1?'display:none':'' }}">
								<label class="col-sm-4 control-label">{{ __('Price') }}</label>
								<div class="col-sm-12">
									<div class="input-group">
										<div class="input-group-prepend">
											<span class="input-group-text"><i class="">{{ getCurrencySymbol() }}</i></span>
										</div>
										<input type="text" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" value="{{ old('price') }}">
										@if ($errors->has('price'))
										<span class="invalid-feedback" role="alert">
											<strong>{{ $errors->first('price') }}</strong>
										</span>
										@endif
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Category') }}</label>
								<div class="col-sm-12">
									<select name="category_id" class="form-control{{ $errors->has('category_id') ? ' is-invalid' : '' }} select2">
										<option value="" disabled selected>Select Category</option>
										@forelse($categories as $category)
										<option value="{{ $category->id }}" {{ old('category_id')==$category->id?'selected':'' }}>{{ $category->name }}</option>
										@empty
										<option value="" disabled>Please add item categories</option>
										@endforelse
									</select>
									@if ($errors->has('category_id'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('category_id') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Featured?') }}</label>
								<div class="col-sm-12">
									<select name="is_featured" class="form-control{{ $errors->has('is_featured') ? ' is-invalid' : '' }}">
										<option value="1" {{ old('is_featured')==1?'selected':'' }}>Yes</option>
										<option value="0" {{ old('is_featured')==0?'selected':'' }}>No</option>
									</select>
									@if ($errors->has('is_featured'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('is_featured') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group ">
								<img class="img-fluid mb-3 ml-2" width="50%" height="50%" id="image_upload_preview" src="{{ asset('images/default.png') }}"
								alt="Photo">
							</div>
							<small class="ml-2">Please upload an image with 400*300 ratio for better results</small>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Image') }}</label>
								<div class="col-sm-12">
									<div class="upload-btn-wrapper">
										<button class="upload-btn">Upload a file</button>
										<input type="file" name="image" id="image_upload" class="form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" />
									</div>
									@if ($errors->has('image'))
									<span class="invalid-feedback" role="alert" style="display:block !important;">
										<strong>{{ $errors->first('image') }}</strong>
									</span>
									@endif
								</div>
							</div>
							
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Status') }}</label>
								<div class="col-sm-12">
									<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
									@if ($errors->has('status'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Save</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>
@endsection