@extends('layouts.admin')
@section('title', 'Dashboard')

@section('content')


<!-- Info boxes -->
<div class="row">
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"> {{ __("Today's Orders") }}</span>
        <span class="info-box-number">
          {{ $cards['todays-orders'] }}
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-book"></i></span>

      <div class="info-box-content">
        <span class="info-box-text"> {{ __("Today's Reservations") }}</span>
        <span class="info-box-number">{{ $cards['todays-reservations'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <!-- fix for small devices only -->
  <div class="clearfix hidden-md-up"></div>

  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-rupee-sign"></i></span>
      <div class="info-box-content">
        <span class="info-box-text">{{ __("Today's Sales") }}</span>
        <span class="info-box-number">{{ $cards['todays-sales'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box">
      <span class="info-box-icon bg-info elevation-1"><i class="fas fa-shopping-cart"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ __("This Week's Orders") }}</span>
        <span class="info-box-number">
          {{ $cards['this-weeks-orders'] }}
        </span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<div class="row">

  <!-- /.col -->
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-book"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ __("This Week's Reservations") }}</span>
        <span class="info-box-number">{{ $cards['this-weeks-reservations'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->

  <!-- fix for small devices only -->
  <div class="clearfix hidden-md-up"></div>

  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-rupee-sign"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ __("This Week's Sales") }}</span>
        <span class="info-box-number">{{ $cards['this-weeks-sales'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-success elevation-1"><i class="fas fa-rupee-sign"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ __("Sale in last 30 Days") }}</span>
        <span class="info-box-number">{{ $cards['this-month-sales'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <div class="col-12 col-sm-6 col-md-3">
    <div class="info-box mb-3">
      <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-rupee-sign"></i></span>

      <div class="info-box-content">
        <span class="info-box-text">{{ __("Total Sales") }}</span>
        <span class="info-box-number">{{ $cards['total-sales'] }}</span>
      </div>
      <!-- /.info-box-content -->
    </div>
    <!-- /.info-box -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">
        <h5 class="card-title">{{ __("Sales Report") }}</h5>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <div class="btn-group">
            <button type="button" class="btn btn-tool dropdown-toggle" data-toggle="dropdown">
              <i class="fas fa-wrench"></i>
            </button>
            <div class="dropdown-menu dropdown-menu-right" role="menu">
              <a href="#" value="six-month" class="dropdown-item sales-graph-option">{{ __("Last 6 Months") }}</a>
              <a href="#" value="this-year" class="dropdown-item sales-graph-option">{{ __("This Year") }}</a>
              <a href="#" value="previous-year" class="dropdown-item sales-graph-option">{{ __("Previous Year") }}</a>
            </div>
          </div>
          <button type="button" class="btn btn-tool" data-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body">
        <div class="row">
          <div class="col-md-12">
            <p class="text-center">
              <strong>{{ __("Sales") }}: <span id="sale-date-range">1 Jan, 2014 - 30 Jul, 2014</span></strong>
            </p>

            <div class="chart">
              <!-- Sales Chart Canvas -->
              <canvas id="salesChart" height="180" style="height: 180px;"></canvas>
            </div>
            <!-- /.chart-responsive -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->

<!-- Main row -->
<div class="row">
  <!-- Left col -->
  <div class="col-md-12">
    <!-- MAP & BOX PANE -->
    <div class="card">

      <!-- /.card-header -->

      <!-- /.card-body -->
    </div>
    <!-- /.card -->
    <div class="row">
      <div class="col-md-6">
        <!-- USERS LIST -->

      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->

    <!-- TABLE: LATEST ORDERS -->
    <div class="card">
      <div class="card-header border-transparent">
        <h3 class="card-title">{{ __("Latest Orders") }}</h3>

        <div class="card-tools">
          <button type="button" class="btn btn-tool" data-widget="collapse">
            <i class="fas fa-minus"></i>
          </button>
          <button type="button" class="btn btn-tool" data-widget="remove">
            <i class="fas fa-times"></i>
          </button>
        </div>
      </div>
      <!-- /.card-header -->
      <div class="card-body p-0">
        <div class="table-responsive">
          <table class="table m-0">
            <thead>
              <tr>
                <th>{{ __("Order ID") }}</th>
                <th>{{ __("Name") }}</th>
                <th>{{ __("Status") }}</th>
                <th>{{ __("On") }}</th>
                <th>{{ __("Edit") }}</th>
              </tr>
            </thead>
            <tbody>
              @forelse($orders as $order)
              <tr>
                <td>#{{ $order->order_reference }}</td>
                <td>{{ $order->customer->getName() }}</td>
                <td><span class="{{ $order->getStatusClass() }}">{{ $order->getStatus() }}</span></td>
                <td>
                  <div class="sparkbar" data-color="#00a65a" data-height="20">{{ getDateString($order->created_at) }}</div>
                </td>
                <td><a href="{{route('admin.orders.edit',[$order])}}" class="btn btn-primary btn-sm"><i class="fa fa-edit mr-2"></i>Edit</a></td>
              </tr>
              @empty
              <tr>
                <td colspan="5">
                  No orders found
                </td>
              </tr>
              @endforelse
            </tbody>
          </table>
        </div>
        <!-- /.table-responsive -->
      </div>
      <!-- /.card-footer -->
    </div>
    <!-- /.card -->
  </div>
  <!-- /.col -->
</div>
<!-- /.row -->
@endsection    