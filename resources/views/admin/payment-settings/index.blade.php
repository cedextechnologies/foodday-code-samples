@extends('layouts.admin')
@section('title', 'Payment and Checkout Settings')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Payment and Checkout Settings") }}</li>
</ol>
@endsection

@section('content')

<!-- /.content-header -->

<!-- Main content -->
<section class="content">
	<!-- /.col -->
	<div class="row">
		<div class="col-md-12">
			<div class="card">
				<div class="card-header p-2">
					<ul class="nav nav-pills">
						<li class="nav-item"><a class="nav-link {{ (Session::get('active_settings')!='stripe' && Session::get('active_settings')!='braintree' && Session::get('active_settings')!='paypal')?'active':''}}" href="#payment" data-toggle="tab">Payment</a>
						</li>
						<li class="nav-item"><a class="nav-link {{Session::get('active_settings')=='stripe'?'active':''}}" href="#stripe" data-toggle="tab">Stripe</a>
						</li>
						<!-- <li class="nav-item"><a class="nav-link {{Session::get('active_settings')=='braintree'?'active':''}}" href="#braintree" data-toggle="tab">Braintree</a></li> -->
						<li class="nav-item"><a class="nav-link {{Session::get('active_settings')=='paypal'?'active':''}}" href="#paypal" data-toggle="tab">Paypal</a></li>
					</ul>
				</div><!-- /.card-header -->
				<div class="card-body">
					<div class="tab-content">
						<!-- .tab-pane -->
						<div class="tab-pane {{ (Session::get('active_settings')!='stripe' && Session::get('active_settings')!='braintree' && Session::get('active_settings')!='paypal')?'active':''}}" id="payment">
							<form class="form-horizontal" method="post" action="{{ route('admin.payment-settings.update-payment-settings') }}" enctype='multipart/form-data'>
								@csrf
								<input type="hidden" name="_method" value="PUT">
								<div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="accept_cash" id="accept_cash" {{ $settings['accept_cash']==1?'checked':'' }}>
											Accept cash
										</span>
									</div>
								</div>
								<div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="stripe_payment" id="accept_cash" {{ $settings['stripe_payment']==1?'checked':'' }}>
											Stripe Payment Gateway
										</span>
									</div>
								</div>
								<!-- <div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="braintree_payment" id="accept_cash" {{ $settings['braintree_payment']==1?'checked':'' }}>
											Braintree Payment Gateway
										</span>
									</div>
								</div> -->
								<div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="paypal_payment" id="accept_cash" {{ $settings['paypal_payment']==1?'checked':'' }}>
											Paypal Gateway

										</span>
									</div>
								</div>
								<div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="checkout_delivery" id="accept_cash" {{ $settings['checkout_delivery']==1?'checked':'' }}>
											Checkout - Delivery
										</span>
									</div>
								</div>
								<div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="checkout_carry_out" id="accept_cash" {{ $settings['checkout_carry_out']==1?'checked':'' }}>
											Checkout - Carry-out
										</span>
									</div>
								</div>
								<!-- <div class="row">
									<div class="checkbox">
										<span>
											<input type="checkbox" name="checkout_dine_in" id="accept_cash" {{ $settings['checkout_dine_in']==1?'checked':'' }}>
											Checkout - Dine-in
										</span>
									</div>
								</div> -->
								<div class="row mt-4">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</form>
						</div>
						<!-- /.tab-pane -->
						<!-- .tab-pane -->
						<div class="tab-pane {{Session::get('active_settings')=='stripe'?'active':''}}" id="stripe">
							<form class="form-horizontal" method="post" action="{{ route('admin.payment-settings.update-stripe-settings') }}" enctype='multipart/form-data'>
								@csrf
								<input type="hidden" name="_method" value="PUT">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label for="inputEmail3" class="col-sm-4 control-label">Secret Key</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('stripe_secret_key') ? ' is-invalid' : '' }}" id="inputEmail3" name="stripe_secret_key" value="{{ old('stripe_secret_key', $settings['stripe_secret_key']) }}">

												@if ($errors->has('stripe_secret_key'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('stripe_secret_key') }}</strong>
												</span>
												@endif

											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Publishable Key</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('stripe_publishable_key') ? ' is-invalid' : '' }}" id="inputEmail3" name="stripe_publishable_key" value="{{ old('stripe_publishable_key', $settings['stripe_publishable_key']) }}">

												@if ($errors->has('stripe_publishable_key'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('stripe_publishable_key') }}</strong>
												</span>
												@endif

											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</form>
						</div>
						<!-- /.tab-pane -->
						<!-- .tab-pane -->
						<!-- <div class="tab-pane {{Session::get('active_settings')=='braintree'?'active':''}}" id="braintree">
							<form class="form-horizontal" method="post" action="{{ route('admin.payment-settings.update-braintree-settings') }}" enctype='multipart/form-data'>
								@csrf
								<input type="hidden" name="_method" value="PUT">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">Environment</label>
											<div class="col-sm-12">
												<select class="form-control{{ $errors->has('braintree_environment') ? ' is-invalid' : '' }}" id="inputEmail3" name="braintree_environment">
													<option value="1" {{ $settings['braintree_environment']==1?'selected':'' }}>Production</option>
													<option value="2" {{ $settings['braintree_environment']==2?'selected':'' }}>Sandbox</option>
												</select>

												@if ($errors->has('braintree_environment'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('braintree_environment') }}</strong>
												</span>
												@endif

											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Merchant ID</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('marchant_id') ? ' is-invalid' : '' }}" id="inputEmail3" name="marchant_id" value="{{ old('marchant_id', $settings['marchant_id']) }}">

												@if ($errors->has('marchant_id'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('marchant_id') }}</strong>
												</span>
												@endif

											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">Public Key
											</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('braintree_public_key') ? ' is-invalid' : '' }}" id="inputEmail3" name="braintree_public_key" value="{{ old('braintree_public_key', $settings['braintree_public_key']) }}">

												@if ($errors->has('braintree_public_key'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('braintree_public_key') }}</strong>
												</span>
												@endif

											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Private Key
											</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('private_key') ? ' is-invalid' : '' }}" id="inputEmail3" name="private_key" value="{{ old('private_key', $settings['private_key']) }}">

												@if ($errors->has('private_key'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('private_key') }}</strong>
												</span>
												@endif

											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</form>
						</div> -->
						<!-- /.tab-pane -->
						<!-- .tab-pane -->
						<div class="tab-pane {{Session::get('active_settings')=='paypal'?'active':''}}" id="paypal">
							<form class="form-horizontal" method="post" action="{{ route('admin.payment-settings.update-paypal-settings') }}" enctype='multipart/form-data'>
								@csrf
								<input type="hidden" name="_method" value="PUT">
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
											<label class="col-sm-4 control-label">Environment</label>
											<div class="col-sm-12">
												<select class="form-control{{ $errors->has('paypal_environment') ? ' is-invalid' : '' }}" id="inputEmail3" name="paypal_environment">
													<option value="1" {{ $settings['paypal_environment']==1?'selected':'' }}>Production</option>
													<option value="2" {{ $settings['paypal_environment']==2?'selected':'' }}>Sandbox</option>
												</select>

												@if ($errors->has('paypal_environment'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('paypal_environment') }}</strong>
												</span>
												@endif

											</div>
										</div>
										<div class="form-group">
											<label class="col-sm-4 control-label">Paypal Client ID</label>
											<div class="col-sm-12">
												<input type="text" class="form-control{{ $errors->has('paypal_cient_id') ? ' is-invalid' : '' }}" id="inputEmail3" name="paypal_cient_id" value="{{ old('paypal_cient_id', $settings['paypal_cient_id']) }}">

												@if ($errors->has('paypal_cient_id'))
												<span class="invalid-feedback" style="display: block !important;" role="alert">
													<strong>{{ $errors->first('paypal_cient_id') }}</strong>
												</span>
												@endif

											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-offset-2 col-sm-10">
									<button type="submit" class="btn btn-success">Update</button>
								</div>
							</form>
						</div>
						<!-- /.tab-pane -->
					</div>
					<!-- /.tab-content -->
				</div><!-- /.card-body -->
			</div>
			<!-- /.nav-tabs-custom -->
		</div>
	</div>
	<!--/. container-fluid -->
</section>


@endsection


