@extends('layouts.admin')
@section('title', 'Cities')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.cities.index') }}">{{ __("Cities") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Edit City") }}</li>
</ol>
@endsection

@section('content')


<div class="">
	<!-- Horizontal Form -->
	<form class="form-horizontal" method="post" action="{{ route('admin.cities.update', $city) }}">
		
		@csrf
		@method('PUT')

		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Edit City</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->

			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Name') }}</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $city->name) }}">

								@if ($errors->has('name'))
								<span class="invalid-feedback" style="display: block !important;" role="alert">
									<strong>{{ $errors->first('name') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Status') }}</label>
							<div class="col-sm-12">
								<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
									<option {{$city->status ==1?'selected':''}} value="1">Active</option>
									<option {{$city->status ==0?'selected':''}} value="0">Inactive</option>
								</select>
								@if ($errors->has('status'))
								<span class="invalid-feedback" style="display: block !important;" role="alert">
									<strong>{{ $errors->first('status') }}</strong>
								</span>
								@endif
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">{{ __('Update') }}</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>
@endsection            