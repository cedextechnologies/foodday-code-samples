@extends('layouts.admin')
@section('title', 'Cities')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Cities") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Cities") }}</h3>

				<div class="card-tools row">	

					<div class="col-md-7">
						<form method="get" action="{{route('admin.cities.index')}}">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-search"></i></span>
								</div>
								<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control">
								<input type="submit" style="display: none;">
							</div>
						</form>
					</div>

					<div class="col-md-5">
						<a href="{{ route('admin.cities.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus mr-1"></i>{{ __('Add New City') }}</a>
					</div>

				</div>
				
			</div>
			<!-- /.card-header -->
			<div class="card-body">

				@if(!$cities->isEmpty())

				<table id="example1" class="table table-bordered table-striped">
					<thead>
						<tr>
							<th>{{ __("Name") }}</th>
							<th width="5%">{{ __("Enable/Disable") }}</th>
							<th>{{ __("Action") }}</th>
						</tr>
					</thead>
					<tbody>

						@foreach($cities as $key => $city)

						<tr>
							<td>{{$city->name}}</td>
							<td>
								<div class="form-group">
									<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
										<input type="checkbox" class="custom-control-input js-coupon-status status-toggle" status-data="{{ $city->id }}" status-url="admin.cities.updateStatus" id="customSwitch-{{$key}}" {{$city->isActive()?'checked':''}}>
										<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
									</div>
								</div>
							</td>
							<td>
								<a href="{{route('admin.cities.edit',[$city])}}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
								
								<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="js-delete-coupon"><i class="fas fa-trash"></i></a>
								<form method="post" action="{{route('admin.cities.destroy', $city)}}" class="js-form-delete-coupon">
									@csrf
									<input type="hidden" name="_method" value="DELETE">
								</form>		
							</td>
						</tr>

						@endforeach

						<tfoot>
						</tfoot>
					</table>
					@endIf		

				</div>
			</div>
		</div>
	</div>

	@endsection