@extends('layouts.admin')
@section('title', 'Profile')

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Change Password') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.update_password') }}">
				@csrf
				@method('PUT')
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Current Password') }}</label>
								<div class="col-sm-12">
									<input type="text" name="current_password" class="form-control{{ $errors->has('current_password') ? ' is-invalid' : '' }}" value="{{ old('current_password') }}">
									@if ($errors->has('current_password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('current_password') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('New Password') }}</label>
								<div class="col-sm-12">
									<input type="text" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" value="{{ old('password') }}">
									@if ($errors->has('password'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Confirm Password') }}</label>
								<div class="col-sm-12">
									<input type="text" name="password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" value="{{ old('password_confirmation') }}">
									@if ($errors->has('password_confirmation'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('password_confirmation') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>
@endsection