@extends('layouts.admin')
@section('title', 'Email Settings')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Email Settings") }}</li>
</ol>
@endsection

@section('content')

<div class="">
	<!-- Horizontal Form -->
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Email Settings</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" method="post" action="{{ route('admin.email-settings.update') }}">
            @csrf
            <input type="hidden" name="_method" value="PUT">

			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">From Email</label>
							<div class="col-sm-12">
								<input type="email" class="form-control{{ $errors->has('from_email') ? ' is-invalid' : '' }}" id="inputEmail3" name="from_email" value="{{ old('from_email', $settings['from_email']) }}">

		                        @if ($errors->has('from_email'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('from_email') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">From Name</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('from_email_name') ? ' is-invalid' : '' }}" id="inputEmail3" name="from_email_name" value="{{ old('from_email_name', $settings['from_email_name']) }}">

		                        @if ($errors->has('from_email_name'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('from_email_name') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">SMTP Host</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('smtp_host') ? ' is-invalid' : '' }}" id="inputEmail3" name="smtp_host" value="{{ old('smtp_host', $settings['smtp_host']) }}">

		                        @if ($errors->has('smtp_host'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('smtp_host') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">SMTP Port</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('smtp_port') ? ' is-invalid' : '' }}" id="inputEmail3" name="smtp_port" value="{{ old('smtp_port', $settings['smtp_port']) }}">

		                        @if ($errors->has('smtp_port'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('smtp_port') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">SMTP Username</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('smtp_username') ? ' is-invalid' : '' }}" id="inputEmail3" name="smtp_username" value="{{ old('smtp_username', $settings['smtp_username']) }}">

		                        @if ($errors->has('smtp_username'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('smtp_username') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">SMTP Password</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('smtp_password') ? ' is-invalid' : '' }}" id="inputEmail3" name="smtp_password" value="{{ old('smtp_password', $settings['smtp_password']) }}">

		                        @if ($errors->has('smtp_password'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('smtp_password') }}</strong>
		                        </span>
		                        @endif
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">Update</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>

@endsection