@extends('layouts.admin')
@section('title', 'Faq')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.faqs.index') }}">{{ __("Faq") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Add Faq") }}</li>
</ol>
@endsection

@section('content')

<div class="container-fluid">
	<div class="">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Add Faq</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="post" action="{{ route('admin.faqs.store') }}">
				@csrf
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Title</label>
								<div class="col-sm-12">
									<input type="text" class="form-control{{ $errors->has('title') ? ' is-invalid' : '' }}" name="title" value="{{ old('title') }}" autocomplete="off">

									@if ($errors->has('title'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Description</label>
								<div class="col-sm-12">
									<textarea id="" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" name="description" value="{{ old('description') }}" rows="10">{{ old('description') }}</textarea>

									@if ($errors->has('description'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('description') }}</strong>
									</span>
									@endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Status</label>
								<div class="col-sm-12">
									<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
										<option {{ old('status')==1?'selected':'' }} value="1">Active</option>
										<option {{ old('status')!=1?'selected':'' }} value="0">Inactive</option>
									</select>
									@if ($errors->has('status'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>

@endsection
