@extends('layouts.admin')
@section('title', 'Working Hours')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Working Hours") }}</li>
</ol>
@endsection

@section('content')

<div class="">
	<!-- Horizontal Form -->
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Working Hours</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" method="POST" action="{{ route('admin.working-hours.update') }}">
			@csrf
			@method('PUT')
			<div class="card-body">
				<div class="row">
					<div class="table-responsive">
						<table class="table-condensed table-hover" cellspacing="2" width="100%">
							<thead>
								<tr>
									<th width="15%">Day</th>
									<th width="25%">Opening</th>
									<th width="25%">Closing</th>
									<th width="25%">OFF</th>
								</tr>
							</thead>
							<tbody>

								<tr>
									<td>Monday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_monday') ? ' is-invalid' : '' }}" name="open_hour_monday" value="{{ old('open_hour_monday', $workingHours['open_hour_monday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_monday') ? ' is-invalid' : '' }}" name="close_hour_monday" value="{{ old('close_hour_monday', $workingHours['close_hour_monday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_monday" {{ $workingHours['status_monday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Tuesday</td>
									<td>
										<div class="input-group" data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_tuesday') ? ' is-invalid' : '' }}" name="open_hour_tuesday" value="{{ old('open_hour_tuesday', $workingHours['open_hour_tuesday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_tuesday') ? ' is-invalid' : '' }}" name="close_hour_tuesday" value="{{ old('close_hour_tuesday', $workingHours['close_hour_tuesday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_tuesday" {{ $workingHours['status_tuesday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Wednesday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_wednesday') ? ' is-invalid' : '' }}" name="open_hour_wednesday" value="{{ old('open_hour_wednesday', $workingHours['open_hour_wednesday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_wednesday') ? ' is-invalid' : '' }}" name="close_hour_wednesday" value="{{ old('close_hour_wednesday', $workingHours['close_hour_wednesday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_wednesday" {{ $workingHours['status_wednesday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Thursday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_thursday') ? ' is-invalid' : '' }}" name="open_hour_thursday" value="{{ old('open_hour_thursday', $workingHours['open_hour_thursday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_thursday') ? ' is-invalid' : '' }}" name="close_hour_thursday" value="{{ old('close_hour_thursday', $workingHours['close_hour_thursday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_thursday" {{ $workingHours['status_thursday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Friday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_friday') ? ' is-invalid' : '' }}" name="open_hour_friday" value="{{ old('open_hour_friday', $workingHours['open_hour_friday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_friday') ? ' is-invalid' : '' }}" name="close_hour_friday" value="{{ old('close_hour_friday', $workingHours['close_hour_friday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_friday" {{ $workingHours['status_friday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Saturday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_saturday') ? ' is-invalid' : '' }}" name="open_hour_saturday" value="{{ old('open_hour_saturday', $workingHours['open_hour_saturday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_saturday') ? ' is-invalid' : '' }}" name="close_hour_saturday" value="{{ old('close_hour_saturday', $workingHours['close_hour_saturday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_saturday" {{ $workingHours['status_saturday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>
								<tr>
									<td>Sunday</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('open_hour_sunday') ? ' is-invalid' : '' }}" name="open_hour_sunday" value="{{ old('open_hour_sunday', $workingHours['open_hour_sunday']) }}">

											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<div class="input-group  " data-placement="right" data-align="top" data-autoclose="true">
											<input type="text" class="timeonly clockpicker form-control{{ $errors->has('close_hour_sunday') ? ' is-invalid' : '' }}" name="close_hour_sunday" value="{{ old('close_hour_sunday', $workingHours['close_hour_sunday']) }}">
											<span class="input-group-addon">
												<span class="glyphicon glyphicon-time"></span>
											</span>
										</div>
									</td>
									<td>
										<input type="checkbox" value="1" name="status_sunday" {{ $workingHours['status_sunday']==0?'checked':'' }} style="margin-left:5px;">
									</td>
								</tr>

							</tbody>
						</table>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">Update</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>

@endsection