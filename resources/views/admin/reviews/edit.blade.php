@extends('layouts.admin')
@section('title', 'Edit Reviews')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Edit Reviews") }}</li>
</ol>
@endsection

@section('content')

<div class="container-fluid">
	<div class="">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Edit Review</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form role="form" action="{{ route('admin.reviews.update',$review) }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">

				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label">Rating</label>
								<div class="col-sm-12">
									<select class="form-control{{ $errors->has('rating') ? ' is-invalid' : '' }}" name="rating">
										<option {{ $review->rating==1?'selected':'' }} value="1">1</option>
										<option {{ $review->rating==2?'selected':'' }} value="2">2</option>
										<option {{ $review->rating==3?'selected':'' }} value="3">3</option>
										<option {{ $review->rating==4?'selected':'' }} value="4">4</option>
										<option {{ $review->rating==5?'selected':'' }} value="5">5</option>
									</select>

									@if ($errors->has('rating'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('rating') }}</strong>
									</span>
									@endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Comments</label>
								<div class="col-sm-12">
									<textarea name="comment" id="" class="form-control{{ $errors->has('comment') ? ' is-invalid' : '' }}" rows="10">{{ $review->comment }}</textarea>

									@if ($errors->has('comment'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('comment') }}</strong>
									</span>
									@endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputPassword3" class="col-sm-4 control-label" name="status">Status</label>
								<div class="col-sm-12">
									<select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
										<option {{ $review->status==1?'selected':'' }} value="1">Active</option>
										<option {{ $review->rating!=1?'selected':'' }} value="0">Inactive</option>
									</select>

									@if ($errors->has('status'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>

@endsection