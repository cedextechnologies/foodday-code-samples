@extends('layouts.admin')
@section('title', 'Customer Reviews')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Customer Reviews") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Customer Reviews</h3>
				<div class="card-tools row">
					<form method="get" action="{{route('admin.reviews')}}">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-search"></i></span>
							</div>
							<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control">
							<input type="submit" style="display: none;">
						</div>
					</form>
				</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>{{ __("Customer Name") }}</th>
								<th>{{ __("Order Id") }}</th>
								<th>{{ __("Comments") }}</th>
								<th>{{ __("Rating") }}</th>
								<th width="5%">{{ __("Enable/Disable") }}</th>
								<th>{{ __("Action") }}</th>
							</tr>
						</thead>
						<tbody>

							@forelse($reviews as $key => $review)
							
							<tr role="row" class="odd">
								<td width="20%" class="sorting_1">{{ $review->customer->first_name }} {{ $review->customer->last_name }}</td>
								<td>{{ $review->order->order_reference }}</td>
								<td>{{ $review->comment }}</td>
								<td>{{ $review->rating }}</td>
								<td>
									<div class="form-group">
										<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											<input type="checkbox" class="custom-control-input js-user-status status-toggle" status-data="{{  $review->id  }}" status-url="admin.reviews.updateStatus" id="customSwitch-{{$key}}" {{$review->isActive()?'checked':''}}>
											<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
										</div>
									</div>
								</td>
								<td>
									<a href="{{route('admin.reviews.edit',[$review])}}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>

									<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="js-delete-user"><i class="fas fa-trash"></i></a>
									<form method="post" action="{{route('admin.reviews.destroy', $review)}}" class="js-form-delete-user">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
									</form>	


								</td>
							</tr>

							@empty
							<p>Sorry No reviews found!</p>
							@endforelse

						</tbody>
					</table>
				</div>
			</div>
		</div>
		<!-- /.card-body -->
	</div>
</div>	

@endsection