@extends('layouts.admin')
@section('title', 'Common Modifiers')


@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item active">Common Modifiers</li>
</ol>
@endsection

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }}</a></li>
	<li class="breadcrumb-item active">{{ __('Custome Modifiers') }}</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Custome Modifiers") }}</h3>
				<div class="card-tools"><a href="{{ route('admin.common-modifiers.create') }}" class="btn btn-primary btn-sm"><i
					class="fa fa-plus mr-1"></i>Add Modifier</a></div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped datatable">
							<thead>
								<tr>
									<th width="40%">{{ __("Name") }}</th>
									<th width="20%">{{ __("Minimum No. of Order") }}</th>
									<th width="20%">{{ __("Maximum No. of Order") }}</th>
									<th width="20%">{{ __("Action") }}</th>
								</tr>
							</thead>
							<tbody>

								@forelse($modifiers as $key => $modifier)
								<tr>
									<td>{{$modifier->title}}</td>
									<td>{{$modifier->minimum}}</td>
									<td>{{$modifier->maximum}}</td>
									<td><a href="{{route('admin.common-modifiers.edit',[$modifier])}}" data-toggle="tooltip" title="Edit"><i
										class="fas fa-edit mr-3"></i></a>
										<a href="#" class="delete-btn" delete-url="{{ route('admin.common-modifiers.destroy',[$modifier]) }}" data-toggle="modal" data-target="#modal-delete" title="Delete"><i
											class="fas fa-trash"></i></a>
										</td>
									</td>
								</tr>

								@empty
								<tr>
									<td colspan="4">No records found</td>
								</tr>
								@endforelse
							</tbody>
							<tfoot>
							</tfoot>
						</table>
					</div>
				</div>
			</div>
		</div>
	</div>
	@endsection