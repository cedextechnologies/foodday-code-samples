@extends('layouts.admin')
@section('title', 'Modifiers')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.common-modifiers.index') }}">Common Modifiers</a></li>
	<li class="breadcrumb-item active">Create Modiifer</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-6">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Create Modifier') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.common-modifiers.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
				    <div class="row">
				      <div class="col-md-12">
				        <div class="form-group">
				          <label class="col-sm-4 control-label">{{ __('Title') }}</label>
				          <div class="col-sm-12">
				            <input type="text" name="title" class="form-control">
									
									@if ($errors->has('title'))
									<span class="invalid-feedback"  style="display: block !important;" role="alert">
										<strong>{{ $errors->first('title') }}</strong>
									</span>
									@endif
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-4 control-label">Minimum</label>
				          <div class="row">
				            <div class="col-md-3 ml-2">
				              <input type="text" name="minimum" class="form-control" value="1">

									@if ($errors->has('minimum'))
									<span class="invalid-feedback"  style="display: block !important;" role="alert">
										<strong>{{ $errors->first('minimum') }}</strong>
									</span>
									@endif


				            </div>
				            <div class="col-dm-9 ml-3">Minimum no of items customer MUST choose. </div>
				          </div>
				        </div>
				        <div class="form-group">
				          <label class="col-sm-4 control-label">Maximum</label>
				          <div class="row">
				            <div class="col-md-3 ml-2">
				              <input type="text" name="maximum" class="form-control" value="1">

				              		@if ($errors->has('maximum'))
									<span class="invalid-feedback"  style="display: block !important;" role="alert">
										<strong>{{ $errors->first('maximum') }}</strong>
									</span>
									@endif


				            </div>
				            <div class="col-dm-9 ml-3">Maximum no of items customer CAN choose.</div>
				          </div>
				        </div>


				        <a href="#" class="btn btn-primary btn-sm ml-2" id="add-more-modifier-items-rows"><i class="fa fa-plus"></i>Add More</a>
				        <div class="form-group mt-3" id="modifiers">
				          <div class="row">
				            <div class="col-sm-8">
				              <input type="text" class="form-control ml-2" name="item_name[]" placeholder="Item Name" required>
				            </div>
				            <div class="col-sm-3">
				              <input type="number" class="form-control ml-2" value="0" name="item_price[]" placeholder="Price" required>
				            </div>
				            <div class="col-sm-1">

				            </div>
				          </div>
				        </div>


				      </div>
				    </div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Save</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>



@endsection