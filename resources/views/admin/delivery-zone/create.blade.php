@extends('layouts.admin')
@section('title', 'Delivery Zones')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.zones.index') }}">Delivery Zones</a></li>
	<li class="breadcrumb-item active">Create Delivery Zone</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Create Delivery Zone') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.zones.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="card-body">

					<div class="row">
						<div class="col-md-12">
							<div id="map" style="height: 300px !important;border: 1px solid #3872ac;"></div>
							<small>Please draw a poligon over the desired area by click on the map</small>
							<input type="hidden" id="polygondata" name="polygondata">
							@if ($errors->has('polygondata'))
							<span class="invalid-feedback" role="alert" style="display:block">
								<strong>{{ $errors->first('polygondata') }}</strong>
							</span>
							@endif
						</div>
					</div>
					<hr>
					<div class="row" id="deliverid" style="display: none">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Zone Name') }}</label>
								<div class="col-sm-12">
									<input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" placeholder="Zone Name" required>
									@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Delivery availiable') }}</label>
								<div class="col-sm-12">
									<select name="delivery_available" class="js-delivery-available form-control{{ $errors->has('delivery_available') ? ' is-invalid' : '' }}">
										<option value="1">Yes</option>
										<option value="0">No</option>
									</select>
									@if ($errors->has('delivery_available'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('delivery_available') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group js-default-delivery-charge">
								<label class="col-sm-4 control-label">{{ __('Default Delivery Charge') }}</label>
								<div class="col-sm-12">
									<input type="text" name="default_delivery_charge" class="form-control{{ $errors->has('default_delivery_charge') ? ' is-invalid' : '' }}" value="{{ old('default_delivery_charge') }}" placeholder="Default Delivery Charge">
									@if ($errors->has('default_delivery_charge'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('default_delivery_charge') }}</strong>
									</span>
									@endif
								</div>
							</div>							
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success" id="btnSubmit" style="display: none">Save</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>

@push('scripts')
<script>
	var geocoder;
	var map;
	var polygonArray = [];

	function initialize() {
		map = new google.maps.Map(document.getElementById("map"), {
			center: new google.maps.LatLng(9.9816, 76.2999),
			zoom: 13,
			mapTypeId: google.maps.MapTypeId.ROADMAP
		});

		var drawingManager = new google.maps.drawing.DrawingManager({
			drawingMode: google.maps.drawing.OverlayType.POLYGON,
			drawingControl: true,
			drawingControlOptions: {
				position: google.maps.ControlPosition.TOP_CENTER,
				drawingModes: [
				google.maps.drawing.OverlayType.POLYGON
				]
			},
			polygonOptions: {
				fillColor: '#BCDCF9',
				fillOpacity: 0.5,
				strokeWeight: 2,
				strokeColor: '#57ACF9',
				clickable: false,
				editable: false,
				zIndex: 1
			}
		});
		drawingManager.setMap(map)

		google.maps.event.addListener(drawingManager, 'polygoncomplete', function(polygon) {
			for (var i = 0; i < polygon.getPath().getLength(); i++) {
				document.getElementById('polygondata').value += polygon.getPath().getAt(i).toUrlValue(6) + "<br>";
			}
			$('#deliverid').show();
			$("#btnSubmit").show();
			polygonArray.push(polygon);
		});
	}
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLeMPncsAM5MEU92wPtziaC8tDoVG_PlQ&libraries=drawing&callback=initialize"
async defer></script>
@endpush
@endsection