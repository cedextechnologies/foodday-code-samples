@extends('layouts.admin')
@section('title', 'Custome Delivery Fees')


@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Custome Delivery Fees</li>
    </ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Custome Delivery Fees") }}</h3>
				<div class="card-tools">
					<a href="javascript:void(0)" class="btn btn-primary btn-sm" id="js-save-custome-delivery-charge"><i class="fa fa-save mr-1"></i>Save</a>
				</div>	

			</div>
			<!-- /.card-header -->
			<div class="card-body">

				<form id="js-add-delivery-charge" data-zone="{{ $zone->id }}">
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th width="40%">{{ __("Order Amount") }}</th>
							<th width="40%">{{ __("Delivery Fee Amount") }}</th>
							<th width="20%">{{ __("Action") }}</th>
						</tr>
					</thead>
					<tbody id="js-add-custome-delivery-charge-table">
						
						<tr>
							<td><input type="text" class="form-control js-order-amount" name="order_amount[]"> </td>
							<td><input type="text" class="form-control js-order-fee-amount" name="delivery_fee_amount[]"> </td>
							<td><a href="javascript:void(0)" class="btn btn-primary btn-sm" id="js-add-custome-delivery-charge"><i class="fa fa-plus mr-1"></i></a></td>
						</tr>


						@foreach($customeDeliveryFees as $key => $customeDeliveryFee)
						<tr>
							<td><input type="text" class="form-control js-order-amount" name="order_amount[]" value="{{ $customeDeliveryFee->order_amount }}"></td>
							<td><input type="text" class="form-control js-order-fee-amount" name="delivery_fee_amount[]" value="{{ $customeDeliveryFee->delivery_fee_amount }}"></td>
							<td><a href="javascript:void(0)" class="delete-btn js-remove-custome-charge-raw" title="Delete"><i
									class="fas fa-trash"></i></a>
								</td>
							</td>
						</tr>
						@endforeach
					</tbody>


						<tfoot>
						</tfoot>
					</table>
				</form>	
				</div>
			</div>
		</div>
	</div>
	@endsection