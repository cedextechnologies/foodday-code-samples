@extends('layouts.admin')
@section('title', 'Delivery Zones')


@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
        <li class="breadcrumb-item active">Delivery Zones</li>
    </ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Delivery Zones") }}</h3>
				<div class="card-tools"><a href="{{ route('admin.zones.create') }}" class="btn btn-primary btn-sm"><i
                    class="fa fa-plus mr-1"></i>Add New Delivery Zone</a></div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">

				<table class="table table-bordered table-striped datatable">
					<thead>
						<tr>
							<th width="40%">{{ __("Name of the area") }}</th>
							<th width="15%">{{ __("Default delivery charge") }}</th>
							<th width="15%">{{ __("Delivery possible") }}</th>
							<th width="15%">{{ __("Delivery fee") }}</th>
							<th width="15%">{{ __("Action") }}</th>
						</tr>
					</thead>
					<tbody>

						@forelse($zones as $key => $zone)
						<tr>
							<td>{{ $zone->name }}</td>
							<td>{{ $zone->default_delivery_charge }}</td>
							<td>{{ $zone->delivery_available==1?'Yes':'No' }}</td>
							<td>
								@if($zone->delivery_available==1)
									<a href="{{ route('admin.zones.custome-delivey-fee',[$zone]) }}">Customise delivery fee</a>
								@endIf
							</td>
							<td>
								<a href="#" class="delete-btn" delete-url="{{ route('admin.zones.destroy',[$zone]) }}" data-toggle="modal" data-target="#modal-delete" title="Delete"><i
									class="fas fa-trash"></i></a>
							</td>
							</td>
						</tr>

						@empty
						<tr>
							<td colspan="4">No records found</td>
						</tr>
						@endforelse

						<tfoot>
						</tfoot>
					</table>
				</div>
			</div>
		</div>
	</div>
	@endsection