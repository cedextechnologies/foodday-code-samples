@extends('layouts.admin')
@section('title', 'Cuisines')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">Dashboard</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.item-categories.index') }}">Cuisines</a></li>
	<li class="breadcrumb-item active">Create Cuisine</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __('Create Cuisine') }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form class="form-horizontal" method="POST" action="{{ route('admin.item-categories.store') }}" enctype="multipart/form-data">
				@csrf
				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Name') }}</label>
								<div class="col-sm-12">
									<input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}">
									@if ($errors->has('name'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Description') }}</label>
								<div class="col-sm-12">
									<textarea name="description" class="form-control{{ $errors->has('description') ? ' is-invalid' : '' }}" rows="9">{{ old('description') }}</textarea>
									@if ($errors->has('description'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('description') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<img class="img-fluid mb-3 ml-2" width="50%" height="50%" id="image_upload_preview" src="{{ asset('images/default.png') }}"
								alt="Photo">
							</div>
							<small class="ml-2">Please upload an image with 400*300 ratio for better results</small>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Image') }}</label>
								<div class="col-sm-12">
									<div class="upload-btn-wrapper">
										<button class="upload-btn">Upload a file</button>
										<input type="file" name="image" id="image_upload" class="form-control {{ $errors->has('image') ? ' is-invalid' : '' }}" />
									</div>
									@if ($errors->has('image'))
									<span class="invalid-feedback" role="alert" style="display:block !important;">
										<strong>{{ $errors->first('image') }}</strong>
									</span>
									@endif
								</div>
							</div>
							<div class="form-group">
								<label class="col-sm-4 control-label">{{ __('Status') }}</label>
								<div class="col-sm-12">
									<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
										<option value="1">Active</option>
										<option value="0">Inactive</option>
									</select>
									@if ($errors->has('status'))
									<span class="invalid-feedback" role="alert">
										<strong>{{ $errors->first('status') }}</strong>
									</span>
									@endif
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Save</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>
@endsection