@extends('layouts.admin')
@section('title', 'Cuisines')


@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Cuisines") }}</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Cuisines") }}</h3>
				<div class="card-tools"><a href="{{ route('admin.item-categories.create') }}" class="btn btn-primary btn-sm"><i
					class="fa fa-plus mr-1"></i>Add New Cuisine</a></div>
				</div>
				<!-- /.card-header -->
				<div class="card-body">
					<div class="table-responsive">
						<table class="table table-bordered table-striped datatable">
							<thead>
								<tr>
									<th width="20%">{{ __("Name") }}</th>
									<th>{{ __("Description") }}</th>
									<th width="5%">{{ __("Enable/Disable") }}</th>
									<th width="5%">{{ __("Action") }}</th>
								</tr>
							</thead>
							<tbody>

								@forelse($categories as $key => $category)
								<tr>
									<td>{{$category->name}}</td>
									<td>{{$category->description}}</td>
									<td>
										<div class="form-group">
											
											<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
												<input type="checkbox" class="custom-control-input status-toggle" status-data="{{ $category->id }}" status-url="admin.item-categories.updateStatus" id="customSwitch-{{$key}}" {{$category->isActive()?'checked':''}}>
												<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
											</div>

										</div>
									</td>
									<td><a href="{{route('admin.item-categories.edit',[$category])}}" data-toggle="tooltip" title="Edit"><i
										class="fas fa-edit mr-3"></i></a>
										<a href="#" class="delete-btn" delete-url="{{ route('admin.item-categories.destroy',[$category]) }}" data-toggle="modal" data-target="#modal-delete" title="Delete"><i
											class="fas fa-trash"></i></a>
										</td>
									</td>
								</tr>

								@empty
								<tr>
									<td colspan="4">No records found</td>
								</tr>
								@endforelse

								<tfoot>
								</tfoot>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
		@endsection