@extends('layouts.admin')
@section('title', 'Order Settings')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Order Settings") }}</li>
</ol>
@endsection

@section('content')

<div class="container-fluid">
	<div class="">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Order Settings</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
            <form class="form-horizontal" method="post" action="{{ route('admin.order-settings.update') }}">
            @csrf
            <input type="hidden" name="_method" value="PUT">

				<div class="card-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Minimum Order</label>
								<div class="col-sm-12">
									<input type="text" class="form-control{{ $errors->has('minimum_order') ? ' is-invalid' : '' }}" id="inputEmail3" name="minimum_order" value="{{ old('minimum_order', $settings['minimum_order']) }}">

		                        @if ($errors->has('minimum_order'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('minimum_order') }}</strong>
		                        </span>
		                        @endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Tax</label>
								<div class="col-sm-12">
									<input type="text" class="form-control{{ $errors->has('tax') ? ' is-invalid' : '' }}" id="inputEmail3" name="tax" value="{{ old('tax', $settings['tax']) }}">

		                        @if ($errors->has('tax'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('tax') }}</strong>
		                        </span>
		                        @endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Delivery Charges</label>
								<div class="col-sm-12">
									<input type="text" class="form-control{{ $errors->has('delivery_charges') ? ' is-invalid' : '' }}" id="inputEmail3" name="delivery_charges" value="{{ old('delivery_charges', $settings['delivery_charges']) }}">

		                        @if ($errors->has('delivery_charges'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('delivery_charges') }}</strong>
		                        </span>
		                        @endif

								</div>
							</div>
							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">Allow Pre-orders</label>
								<div class="col-sm-12">
									<select class="form-control{{ $errors->has('allow_pre_order') ? ' is-invalid' : '' }}" name="allow_pre_order" value="{{ old('allow_pre_order', $settings['allow_pre_order']) }}">
										<option {{ $settings['allow_pre_order'] == 1?'selected':'' }} value="1">Yes</option>
										<option {{ $settings['allow_pre_order'] == 0?'selected':'' }} value="0">No</option>
									</select>
									
		                        @if ($errors->has('allow_pre_order'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('allow_pre_order') }}</strong>
		                        </span>
		                        @endif

								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- /.card-body -->
				<div class="card-footer">
					<button type="submit" class="btn btn-success">Update</button>
				</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>

@endsection