@extends('layouts.admin')
@section('title', 'Store Settings')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
  <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
  <li class="breadcrumb-item active">{{ __("Store Settings") }}</li>
</ol>
@endsection

@section('content')

<!-- /.content-header -->

<!-- Main content -->
<section class="content">
  <!-- /.col -->
  <div class="row">
    <div class="col-md-12">
      <div class="card">
        <div class="card-header p-2">
          <ul class="nav nav-pills">
            <li class="nav-item"><a class="nav-link {{ (Session::get('active_settings')!='contact' && Session::get('active_settings')!='other')?'active':''}}" href="#basic" data-toggle="tab">{{ __('Basc Info') }}</a>
            </li>
            <li class="nav-item"><a class="nav-link {{Session::get('active_settings')=='contact'?'active':''}}" href="#contact" data-toggle="tab">{{ __('Contact Details') }}</a>
            </li>
            <li class="nav-item"><a class="nav-link {{Session::get('active_settings')=='other'?'active':''}}" href="#other" data-toggle="tab">{{ __('Other Settings') }}</a></li>
          </ul>
        </div><!-- /.card-header -->
        <div class="card-body">
          <div class="tab-content">
            <!-- .tab-pane -->
            <div class="tab-pane {{ (Session::get('active_settings')!='contact' && Session::get('active_settings')!='other')?'active':''}}" id="basic">
              <form class="form-horizontal" method="post" action="{{ route('admin.store-settings.update-basic-info') }}" enctype='multipart/form-data'>
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">{{ __('Store Name') }}</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control{{ $errors->has('store_name') ? ' is-invalid' : '' }}" id="inputEmail3" name="store_name" value="{{ old('store_name', $settings['store_name']) }}">

                        @if ($errors->has('store_name'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('store_name') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">{{ __('Store Banner') }}</label>
                      <div class="col-sm-12">
                        <input type="file" class="" name="store_image">

                        @if ($errors->has('store_image'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('store_image') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>

                    @if($settings['store_image'] && file_exists(public_path('storage/'.$settings['store_image'])))

                    <div class="form-group ">
                      <img class="img-fluid mb-3 ml-2" width="50%" height="50%" src="{{ asset('storage/'.$settings['store_image']) }}"
                      alt="Photo">
                    </div>
                    @endif  
                  </div>
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update</button>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
            <!-- .tab-pane -->
            <div class="tab-pane {{Session::get('active_settings')=='contact'?'active':''}}" id="contact">
              <form class="form-horizontal" method="post" action="{{ route('admin.store-settings.update-contact-details') }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="inputEmail3" class="col-sm-4 control-label">Contact Person</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control{{ $errors->has('contact_person') ? ' is-invalid' : '' }}" id="inputEmail3" name="contact_person" value="{{ old('contact_person', $settings['contact_person']) }}">
                        
                        @if ($errors->has('contact_person'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('contact_person') }}</strong>
                        </span>
                        @endif                        
                      
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Contact Email</label>
                      <div class="col-sm-12">
                        <input type="email" class="form-control{{ $errors->has('contact_email') ? ' is-invalid' : '' }}" id="inputEmail3"  name="contact_email" value="{{ old('contact_email', $settings['contact_email']) }}">

                        @if ($errors->has('contact_email'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('contact_email') }}</strong>
                        </span>
                        @endif  
                      
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Contact Number</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control{{ $errors->has('contact_number') ? ' is-invalid' : '' }}" id="inputEmail3" name="contact_number" value="{{ old('contact_number', $settings['contact_number']) }}">
                     
                        @if ($errors->has('contact_number'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('contact_number') }}</strong>
                        </span>
                        @endif 

                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Contact Address</label>
                      <div class="col-sm-12">
                        <textarea class="form-control{{ $errors->has('contact_address') ? ' is-invalid' : '' }}" rows="5" name="contact_address">{{ old('contact_address', $settings['contact_address']) }}</textarea>
                     
                        @if ($errors->has('contact_address'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('contact_address') }}</strong>
                        </span>
                        @endif 

                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update</button>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
            <!-- .tab-pane -->
            <div class="tab-pane {{Session::get('active_settings')=='other'?'active':''}}" id="other">
              <form class="form-horizontal" method="post" action="{{ route('admin.store-settings.update-other-settings') }}">
                @csrf
                <input type="hidden" name="_method" value="PUT">

                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Maintenance Mode</label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('maintenance_mode') ? ' is-invalid' : '' }}" name="maintenance_mode">
                          <option {{ $settings['maintenance_mode'] == 1?'selected':'' }} value="1">ON</option>
                          <option {{ $settings['maintenance_mode'] == 0?'selected':'' }} value="0">OFF</option>
                        </select>

                        @if ($errors->has('maintenance_mode'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('maintenance_mode') }}</strong>
                        </span>
                        @endif 

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Store Currency</label>
                      <div class="col-sm-12">
                        
                        <select class="form-control{{ $errors->has('store_currency') ? ' is-invalid' : '' }}" name="store_currency">

                        @foreach($currencies as $currency)
                            <option {{$settings['store_currency'] == $currency->id?'selected':''}} value="{{$currency->id}}">{{$currency->name}}</option>
                        @endforeach  

                        </select>

                        @if ($errors->has('store_currency'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('store_currency') }}</strong>
                        </span>
                        @endif                         
                          
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Timezone America/Managua
                      </label>
                      <div class="col-sm-12">

                        <select class="form-control{{ $errors->has('timezone') ? ' is-invalid' : '' }} select2" name="timezone" style="width: 100%">

                        @foreach($timeZones as $timeZone)
                            <option {{$settings['timezone'] == $timeZone->id?'selected':''}} value="{{$timeZone->id}}">{{$timeZone->name}}</option>
                        @endforeach  

                        </select>

                        @if ($errors->has('timezone'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('timezone') }}</strong>
                        </span>
                        @endif                          

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Currency Code Position
                      </label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('currency_code_position') ? ' is-invalid' : '' }}" name="currency_code_position">
                          <option {{ $settings['currency_code_position']==0?'selected':'' }} value="0">Left</option>
                          <option {{ $settings['currency_code_position']==1?'selected':'' }} value="1">Right</option>
                        </select>

                        @if ($errors->has('currency_code_position'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('currency_code_position') }}</strong>
                        </span>
                        @endif 

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Decimal Places
                      </label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('decimal_places') ? ' is-invalid' : '' }}" name="decimal_places">
                          <option {{ $settings['decimal_places']==2?'selected':'' }} value="2">2</option>
                          <option {{ $settings['decimal_places']==3?'selected':'' }} value="3">3</option>
                          <option {{ $settings['decimal_places']==4?'selected':'' }} value="4">4</option>
                          <option {{ $settings['decimal_places']==5?'selected':'' }} value="5">5</option>
                          <option {{ $settings['decimal_places']==6?'selected':'' }} value="6">6</option>
                        </select>

                        @if ($errors->has('decimal_places'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('decimal_places') }}</strong>
                        </span>
                        @endif 

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Use 1000 Separators(,)
                      </label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('use_thousand_separators') ? ' is-invalid' : '' }}" name="use_thousand_separators">
                          <option {{ $settings['use_thousand_separators']==1?'selected':'' }} value="1">ON</option>
                          <option {{ $settings['use_thousand_separators']==2?'selected':'' }} value="2">OFF</option>
                        </select>

                        @if ($errors->has('use_thousand_separators'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('use_thousand_separators') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Thousand Separators
                      </label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('thousand_separator') ? ' is-invalid' : '' }}" name="thousand_separator">
                          <option {{ $settings['thousand_separator']==1?'selected':'' }} value="1">Dot</option>
                          <option {{ $settings['thousand_separator']==2?'selected':'' }} value="2">Comma</option>
                        </select>

                        @if ($errors->has('thousand_separator'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('thousand_separator') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Decimal Separators
                      </label>
                      <div class="col-sm-12">
                        <select class="form-control{{ $errors->has('decimal_separator') ? ' is-invalid' : '' }}" name="decimal_separator">
                          <option {{ $settings['decimal_separator']==1?'selected':'' }} value="1">Dot</option>
                          <option {{ $settings['decimal_separator']==2?'selected':'' }} value="2">Comma</option>
                        </select>

                        @if ($errors->has('decimal_separator'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('decimal_separator') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Footer Text</label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control{{ $errors->has('footer_text') ? ' is-invalid' : '' }}" id="inputEmail3" name="footer_text" value="{{old('footer_text', $settings['footer_text'])}}">
                      
                        @if ($errors->has('footer_text'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('footer_text') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <!-- <div class="form-group">
                      <label class="col-sm-4 control-label">Comments Plugin App ID
                      </label>
                      <div class="col-sm-12">
                        <input type="text" class="form-control{{ $errors->has('comments_plugin_app_id') ? ' is-invalid' : '' }}" id="inputEmail3" name="comments_plugin_app_id" value="{{ old('comments_plugin_app_id', $settings['comments_plugin_app_id']) }}">
                      
                        @if ($errors->has('comments_plugin_app_id'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('comments_plugin_app_id') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Google Analytics</label>
                      <div class="col-sm-12">
                        <textarea class="form-control{{ $errors->has('google_analytics') ? ' is-invalid' : '' }}" rows="6" name="google_analytics" value="{{ old('google_analytics', $settings['google_analytics']) }}">{{ old('google_analytics', $settings['google_analytics']) }}</textarea>

                        @if ($errors->has('google_analytics'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('google_analytics') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-sm-4 control-label">Zopim Chat</label>
                      <div class="col-sm-12">
                        <textarea class="form-control{{ $errors->has('zopim_chat') ? ' is-invalid' : '' }}" rows="6" name="zopim_chat" value="{{ old('zopim_chat', $settings['zopim_chat']) }}">{{ old('zopim_chat', $settings['zopim_chat']) }}</textarea>

                        @if ($errors->has('zopim_chat'))
                        <span class="invalid-feedback" style="display: block !important;" role="alert">
                          <strong>{{ $errors->first('zopim_chat') }}</strong>
                        </span>
                        @endif

                      </div>
                    </div> -->
                  </div>
                </div>
                <div class="col-sm-offset-2 col-sm-10">
                  <button type="submit" class="btn btn-success">Update</button>
                </div>
              </form>
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div><!-- /.card-body -->
      </div>
      <!-- /.nav-tabs-custom -->
    </div>
  </div>
  <!--/. container-fluid -->
</section>

@endsection          