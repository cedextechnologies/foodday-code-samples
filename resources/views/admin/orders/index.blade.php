@extends('layouts.admin')
@section('title', 'Orders')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Orders") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Latest Orders</h3>
				<div class="card-tools">
					<form method="get" action="{{route('admin.orders')}}">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-search"></i></span>
							</div>
							<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control">
							<input type="submit" style="display: none;">
						</div>
					</form>
				</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="row mb-2 mt-0">
					<div class="col-sm-12 mb-3">
						<span class="float-right">
							<span class="btn p-2" style="background-color: rgb(236, 236, 236)"></span> : Pending
							<span class="btn p-2" style="background-color: rgb(255, 208, 193)"></span> : Confirmed
							<span class="btn p-2" style="background-color: rgb(255, 182, 182)"></span> : Cancelled
							<span class="btn p-2" style="background-color: rgba(40, 167, 69, 0.3)"></span> : Delivered
							<span class="btn p-2" style="background-color: rgba(0, 243, 255, 0.3)"></span> : Out for delivery
						</span>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12">
						<div class="table-responsive">
							<table id="example1" class="table table-bordered table-striped">
								<thead>
									<tr>
										<th>{{ __("#") }}</th>
										<th>{{ __("Order Reference") }}</th>
										<th>{{ __("Customer") }}</th>
										<th>{{ __("Phone") }}</th>
										<th>{{ __("Address") }}</th>
										<th>{{ __("Grand Total") }}</th>
										<th>{{ __("Date") }}</th>
										<th>{{ __("Status") }}</th>	
										<th>{{ __("Action") }}</th>
									</tr>
								</thead>
								<tbody>

									@forelse($orders as $key => $order)

									<tr role="row" class="odd" style="background-color: {{ $order->rowColor() }}">
										<td>{{ $loop->iteration }}</td>
										<td>{{ $order->order_reference }}</td>
										<td>{{ $order->customer->getName() }}</td>
										<td>{{ $order->customer->phone }}</td>
										<td>
											<address>
												{{ $order->customer->company }}<br>
												{{ $order->customer->door }}<br>
												{{ $order->customer->address_line_1 }}<br>
												{{ $order->customer->address_line_2 }}<br>
												Pin : {{ $order->customer->zipcode }}
											</address>
										</td>
										<td>{{ format_currency($order->payed_amount) }}</td>
										<td>{{ getDateString($order->created_at) }}</td>
										<td>
											<span class="{{ $order->statusBadge() }}"> {{ $order->getStatus() }}</span>
										</td> 
										<td> <a href="{{route('admin.orders.edit',[$order])}}"
											data-toggle="tooltip" class="mt-1" title="Edit"><i class="fas fa-edit"></i></a>
											<a
											href="{{route('admin.orders.invoice',[$order])}}" data-toggle="tooltip" class=" mt-1"
											title="Get PDF Invoice" target="blank">&nbsp;&nbsp;<i class="fas fa-print mr-1"></i></i>Receipt</a>
										</td> </tr>

										@empty
										<p>Sorry No order found!</p>
										@endforelse

									</tbody>
								</table>
								{{ $orders->links() }}
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
		</div>
	</div>	

	@endsection