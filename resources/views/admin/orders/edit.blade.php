@extends('layouts.admin')
@section('title', 'Edit Order')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
  <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.orders') }}">{{ __("Orders") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Edit Order") }}</li>
</ol>
@endsection

@section('content')
<div class="row">
  <div class="col-lg-12">
    <div class="card">
      <div class="card-header">
        <div class="card-title">Order Details</div>
      </div>
      <div class="card-body">
        <!-- Main content -->
        <div class="">

          <!-- info row -->
          <div class="row invoice-info">
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <address>
                <strong>
                {{ $order->customer->getName() }}</strong><br>
                {{ $order->customer->company }}<br>
                {{ $order->customer->door }}<br>
                {{ $order->customer->address_line_1 }}<br>
                {{ $order->customer->address_line_2 }}<br>
                Pin : {{ $order->customer->zipcode }}<br>
                Phone : {{ $order->customer->phone }}<br>
                Email : {{ $order->customer->email }}<br>
              </address>
            </div>
            <!-- /.col -->
            <div class="col-sm-4 invoice-col">
              <div class="row">
                <div class="col">Order ID :</div>
                <div class="col">#{{ $order->order_reference }}</div>
                <div class="w-100"></div>
                <div class="col">Order Type : </div>
                <div class="col"><span class="badge badge-warning">{{ $order->isPreOrder()?'Pre-order' :'Normal' }}</span></div>
                <div class="w-100"></div>
                @if($order->isPreOrder())
                <div class="col">Preorder Date & Time : </div>
                <div class="col"><span class="badge badge-warning">{{ getDateString($order->preorder_date) }} {{ getTimeString($order->preorder_time) }}</span></div>
                @endif
                <div class="w-100"></div>
                <div class="col">Order Date : </div>
                <div class="col">{{ getDateString($order->created_at) }}</div>
                <div class="w-100"></div>
                <div class="col">Order Time : </div>
                <div class="col">{{ getTimeString($order->created_at) }}</div>
                <div class="w-100"></div>
                <div class="col">Payment Method : </div>
                <div class="col"><span class="badge badge-info">
                  {{ $order->paymentMethod() }}
                </span></div>
                <div class="w-100"></div>
                <div class="col">Payment Status : </div>
                <div class="col">
                 {{$order->paymentStatus()}}
               </div>

             </div>
           </div>
           <div class="col-sm-4 invoice-col">

            <form method="post" action="{{route('admin.orders.updateStatus', $order)}}">
              <input type="hidden" name="_method" value="PUT">
              @csrf 
              <div class="row">
                <div class="col">Checkout : </div>
                <div class="col">
                 {{$order->checkoutType()}}
               </div>
               <div class="w-100"></div>
               <div class="col">Status : </div>
               <div class="col"><span class="badge badge-warning">
                 {{ $order->getStatus() }}
               </span></div>
               <div class="w-100 mt-2"></div> 
               <div class="col">Change status : </div>

               <div class="col">
                <select class="form-control select2 select2-hidden-accessible" style="width: 100%;" data-select2-id="1" tabindex="-1" aria-hidden="true" name="status">
                  <option {{ $order->status == 0?'selected':'' }} value="0">Pending</option>
                  <option {{ $order->status == 1?'selected':'' }} value="1">Confirmed</option>
                  <option {{ $order->status == 2?'selected':'' }} value="2">Cancelled</option>
                  <option {{ $order->status == 3?'selected':'' }} value="3">Delivered</option>
                  <option {{ $order->status == 4?'selected':'' }} value="4">Out for Delivery</option>
                </select>
              </div> 
              <div class="w-100 mt-2"></div>
              <div class="col"></div>
              <div class="col">
                <input type="submit" class="btn btn-block btn-success btn-sm swalDefaultSuccess" value="Update">
              </div>
            </div>
            <div class="row">
              <div class="col"></div>
              <div class="col">
                <a
                href="{{route('admin.orders.invoice',[$order])}}" class="btn btn-default btn-sm mt-2" data-toggle="tooltip"
                title="Get PDF Invoice" target="blank">&nbsp;&nbsp;<i class="fas fa-print mr-1"></i>Print Receipt</a>
              </div>
            </div>
          </form> 
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <!-- Table row -->
      <div class="row mt-3">
        <div class="col-12 table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Name</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Subtotal</th>
              </tr>
            </thead>
            <tbody>
             @foreach($order->orderItems as $orderItem)
             <tr>
              <td>{{ $loop->iteration }}.</td>
              <td>{{ $orderItem->item->name }} {{ $orderItem->getSizeText() }}<br>
                <small><i>{{ $orderItem->instruction }}</i></small>


                @if($orderItem->modifiers->count() > 0)
                <table class="table mt-3">
                  <tbody>
                    @foreach($orderItem->modifiers as $modifierItem)

                    <tr>
                      <td width="2%">{{ $loop->iteration }}.</td>
                      <td>{{ $modifierItem->item_name }}</td>
                      <td>{{ format_currency($modifierItem->item_price) }}</td>
                    </tr>
                    @endforeach 
                  </tbody>
                </table>
                @endif  
              </td>
              <td>{{ $orderItem->quantity }}</td>
              <td>{{ format_currency($orderItem->price) }}</td>
              <td>{{ format_currency($orderItem->subtotal) }}</td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
    <hr>
    <div class="row">
      <!-- accepted payments column -->
      <div class="col-6">

      </div>
      <div class="col-6">
        <p class="lead">Amount</p>

        <div class="table-responsive">
          <table class="table">
            <tbody><tr>
              <th style="width:50%">Subtotal</th>
              <td>{{ format_currency($order->subtotal) }}</td>
            </tr>
            <tr>
              <th>Tax ({{ $order->tax }}%)</th>
              <td>{{ format_currency($order->tax_amount) }}</td>
            </tr>
            <tr>
              <th>Delivery Charge</th>
              <td>{{ format_currency($order->delivery_charge) }}</td>
            </tr>
            @if($order->coupon_code!='')
            <tr>
              <th>Coupon Discount ({{ getCouponString($order->coupon_code) }})</th>
              <td>-{{ format_currency($order->coupon_discount) }}</td>
            </tr>
            @endif
            <tr>
              <th>Total</th>
              <td>{{ format_currency($order->payed_amount) }}</td>
            </tr>
          </tbody></table>
        </div>
      </div>
      <!-- /.col -->
    </div>
    <!-- /.row -->
    <!-- /.col -->
  </div>
  <!-- /.row -->
  <div class="row">
    <!-- this row will not appear when printing -->
  </div>
  <!-- /.invoice -->
</div><!-- /.col -->
<div class="card-footer">
  <a href="{{ URL::previous() }}" class="btn btn-primary btn-sm"><i class="fas fa-angle-left mr-1"></i>Back</a>
</div>
</div>
</div>
</div><!-- /.row -->
@endsection