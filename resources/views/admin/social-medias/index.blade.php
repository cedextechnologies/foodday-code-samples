@extends('layouts.admin')
@section('title', 'Social Media')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Social Media") }}</li>
</ol>
@endsection

@section('content')


<div class="">
	<!-- Horizontal Form -->
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Social Media Settings</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" method="post" action="{{ route('admin.social-medias.update') }}">
            @csrf
            <input type="hidden" name="_method" value="PUT">

			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Facebook</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('facebook') ? ' is-invalid' : '' }}" id="inputEmail3" name="facebook" value="{{ old('facebook', $settings['facebook']) }}">

		                        @if ($errors->has('facebook'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('facebook') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Twitter</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('twitter') ? ' is-invalid' : '' }}" id="inputEmail3" name="twitter" value="{{ old('twitter', $settings['twitter']) }}">

		                        @if ($errors->has('twitter'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('twitter') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Youtube</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('youtube') ? ' is-invalid' : '' }}" id="inputEmail3" name="youtube" value="{{ old('youtube', $settings['youtube']) }}">

		                        @if ($errors->has('youtube'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('youtube') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Instagram</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('instagram') ? ' is-invalid' : '' }}" id="inputEmail3" name="instagram" value="{{ old('instagram', $settings['instagram']) }}">
		                        
		                        @if ($errors->has('instagram'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('instagram') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">Pinterest</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('pinterest') ? ' is-invalid' : '' }}" id="inputEmail3" name="pinterest" value="{{ old('pinterest', $settings['pinterest']) }}">

		                        @if ($errors->has('pinterest'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('pinterest') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">Update</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>

@endsection