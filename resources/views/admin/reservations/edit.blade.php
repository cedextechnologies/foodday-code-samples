@extends('layouts.admin')
@section('title', 'Edit Reservation')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.reservations') }}">{{ __("Reservations") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Edit Reservation") }}</li>
</ol>
@endsection

@section('content')

<div class="container-fluid">
	<div class="row">
		<div class="col-lg-12">
			<!-- Horizontal Form -->
			<div class="card card-default">
				<div class="card-header">
					<h3 class="card-title">Edit Reservation</h3>
				</div>
				<!-- /.card-header -->
				<!-- form start -->
			<form role="form" action="{{ route('admin.reservations.update',$reservation) }}" method="POST" enctype="multipart/form-data">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="PUT">
					<div class="card-body">
						<div class="row">
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">Name</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name', $reservation->name) }}">

										@if ($errors->has('name'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('name') }}</strong>
										</span>
										@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Email</label>

									<div class="col-sm-12">
										<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email', $reservation->email) }}">

										@if ($errors->has('email'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
										@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Phone</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('mobile') ? ' is-invalid' : '' }}" name="mobile" value="{{ old('mobile', $reservation->mobile) }}">

										@if ($errors->has('mobile'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('mobile') }}</strong>
										</span>
										@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Booking Date</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('booking_time') ? ' is-invalid' : '' }} datetimepicker" name="booking_time" value="{{ old('booking_time', $reservation->booking_date.' '.$reservation->booking_time) }}">

										@if ($errors->has('booking_time'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('booking_time') }}</strong>
										</span>
										@endif

									</div>
								</div>

								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Status</label>
									<div class="col-sm-12">
										<select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
											<option {{ $reservation->status==0?'selected':'' }} value="0">Pending</option>
											<option {{ $reservation->status==1?'selected':'' }} value="1">Approved</option>
											<option {{ $reservation->status==2?'selected':'' }} value="2">Rejected</option>
										</select>

										@if ($errors->has('status'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('status') }}</strong>
										</span>
										@endif

									</div>
								</div>

							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Party Size</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('party_size') ? ' is-invalid' : '' }}" name="party_size" value="{{ old('party_size', $reservation->party_size) }}">

										@if ($errors->has('party_size'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('party_size') }}</strong>
										</span>
										@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">Extra Notes</label>

									<div class="col-sm-12">
										<textarea class="form-control{{ $errors->has('extra_notes') ? ' is-invalid' : '' }}" name="extra_notes" rows="8">{{ old('extra_notes', $reservation->extra_notes) }}</textarea>

										@if ($errors->has('extra_notes'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('extra_notes') }}</strong>
										</span>
										@endif

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<button type="submit" class="btn btn-success">Update</button>
					</div>
					<!-- /.card-footer -->
				</form>
			</div>
		</div>
	</div>
</div>

@endsection