@extends('layouts.admin')
@section('title', 'Reservations')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Reservations") }}</li>
</ol>
@endsection

@section('content')
<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">Reservations</h3>
				<div class="card-tools row">
					<form method="get" action="{{route('admin.reservations')}}">
						<div class="input-group mb-3">
							<div class="input-group-prepend">
								<span class="input-group-text"><i class="fas fa-search"></i></span>
							</div>
							<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control">
							<input type="submit" style="display: none;">
						</div>
					</form>
				</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				<div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>{{ __("Name") }}</th>
								<th>{{ __("Email") }}</th>
								<th>{{ __("Phone") }}</th>
								<th>{{ __("Booking Date") }}</th>
								<th>{{ __("Booking Time") }}</th>
								<th width="5%">Status</th>
								<th>{{ __("Action") }}</th>
							</tr>
						</thead>
						<tbody>

							@forelse($reservations as $key => $reservation)

							<tr role="row" class="odd">
								<td width="20%" class="sorting_1">{{ $reservation->name }}</td>
								<td>{{ $reservation->email }}</td>
								<td>{{ $reservation->mobile }}</td>
								<td>{{ \Carbon\Carbon::parse($reservation->booking_date)->format('M d, Y') }}</td>
								<td>{{ \Carbon\Carbon::parse($reservation->booking_time)->format('h:i A') }}</td>
								<td>
									<label class="btn btn-{{ $reservation->getStatusClass() }} btn-sm">{{ $reservation->getStatus() }}</label>
								</td>
								<td>
									<a href="{{route('admin.reservations.edit',[$reservation])}}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit mr-2"></i></a>

									<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="js-delete-user"><i class="fas fa-trash"></i></a>
									<form method="post" action="{{route('admin.reservations.destroy', $reservation)}}" class="js-form-delete-user">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
									</form>
								</td>
							</tr>

							@empty
							<p>Sorry No reservations found!</p>
							@endforelse

						</tbody>
					</table>
				</div>
			</div>
		</div>
		{{ $reservations->links() }}
	</div>
</div>	

@endsection