@extends('layouts.admin')
@section('title', 'SMS Settings')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("SMS Settings") }}</li>
</ol>
@endsection

@section('content')

<div class="">
	<!-- Horizontal Form -->
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">SMS Settings</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" method="post" action="{{ route('admin.sms-settings.update') }}">
			@csrf
			<input type="hidden" name="_method" value="PUT">

			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<h5>Twilio</h5>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">From Email</label>
							<div class="col-sm-12">
								<input type="email" class="form-control{{ $errors->has('twilio_from_email') ? ' is-invalid' : '' }}" id="inputEmail3" name="twilio_from_email" value="{{ old('twilio_from_email', $settings['twilio_from_email']) }}">

		                        @if ($errors->has('twilio_from_email'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('twilio_from_email') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">From Name</label>
							<div class="col-sm-12">
								<input type="email" class="form-control{{ $errors->has('twilio_from_name') ? ' is-invalid' : '' }}" id="inputEmail3" name="twilio_from_name" value="{{ old('twilio_from_name', $settings['twilio_from_name']) }}">

		                        @if ($errors->has('twilio_from_name'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('twilio_from_name') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">SMTP Host</label>
							<div class="col-sm-12">
								<input type="email" class="form-control{{ $errors->has('twilio_smtp_host') ? ' is-invalid' : '' }}" id="inputEmail3" name="twilio_smtp_host" value="{{ old('twilio_smtp_host', $settings['twilio_smtp_host']) }}">

		                        @if ($errors->has('twilio_smtp_host'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('twilio_smtp_host') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>
					</div>
				</div>
				<hr>
				<div class="row mt-3">
					<div class="col-md-7">

						<h5>SMS Templates</h5>
						<div class="spacer-10"></div>

						<div class="form-group ">
							<label for="" class="col-md-3 control-label">Order Placed <br> (To Administrator)</label>
							<div class="col-md-9">
								<textarea cols="5" rows="5" class="form-control{{ $errors->has('order_placed_admin_sms') ? ' is-invalid' : '' }}" id="inputEmail3" name="order_placed_admin_sms">{{ old('order_placed_admin_sms', $settings['order_placed_admin_sms']) }}</textarea>

		                        @if ($errors->has('order_placed_admin_sms'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('order_placed_admin_sms') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-md-3 control-label">Order Placed <br> (To Customer)</label>
							<div class="col-md-9">
								<textarea cols="5" rows="5" class="form-control{{ $errors->has('order_placed_customer_sms') ? ' is-invalid' : '' }}" id="inputEmail3" name="order_placed_customer_sms">{{ old('order_placed_customer_sms', $settings['order_placed_customer_sms']) }}</textarea>

		                        @if ($errors->has('order_placed_customer_sms'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('order_placed_customer_sms') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-md-3 control-label">Order Confirmed <br> (To Customer)</label>
							<div class="col-md-9">
								<textarea cols="5" rows="5" class="form-control{{ $errors->has('order_confirmed_customer_sms') ? ' is-invalid' : '' }}" id="inputEmail3" name="order_confirmed_customer_sms">{{ old('order_confirmed_customer_sms', $settings['order_confirmed_customer_sms']) }}</textarea>

		                        @if ($errors->has('order_confirmed_customer_sms'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('order_confirmed_customer_sms') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-md-3 control-label">Order Cancelled <br> (To Customer)</label>
							<div class="col-md-9">
								<textarea cols="5" rows="5" class="form-control{{ $errors->has('order_cancelled_customer_sms') ? ' is-invalid' : '' }}" id="inputEmail3" name="order_cancelled_customer_sms">{{ old('order_cancelled_customer_sms', $settings['order_cancelled_customer_sms']) }}</textarea>

		                        @if ($errors->has('order_cancelled_customer_sms'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('order_cancelled_customer_sms') }}</strong>
		                        </span>
		                        @endif

							</div>
						</div>

						<div class="form-group ">
							<label for="" class="col-md-3 control-label">Order Delivered <br> (To Customer)</label>
							<div class="col-md-9">
								<textarea cols="5" rows="5" class="form-control{{ $errors->has('order_delivered_customer_sms') ? ' is-invalid' : '' }}" id="inputEmail3" name="order_delivered_customer_sms">{{ old('order_delivered_customer_sms', $settings['order_delivered_customer_sms']) }}</textarea>

		                        @if ($errors->has('order_delivered_customer_sms'))
		                        <span class="invalid-feedback" style="display: block !important;" role="alert">
		                          <strong>{{ $errors->first('order_delivered_customer_sms') }}</strong>
		                        </span>
		                        @endif
								
							</div>
						</div>

					</div>

					<div class="col-md-5">
						<h5>Tags</h5>
						<div class="spacer-10"></div>
						<table class="table table-bordered">
							<tbody>
								<tr>
									<td width="50%"><code>order_reference</code></td>
									<td width="50%"><code>Order ID</code></td>
								</tr>
								<tr>
									<td><code>tax</code></td>
									<td><code>Tax %</code></td>
								</tr>
								<tr>
									<td><code>delivery_charge</code></td>
									<td><code>Delivery charge (If applicable)</code></td>
								</tr>
								<tr>
									<td><code>subtotal</code></td>
									<td><code>Subtotal of cart</code></td>
								</tr>
								<tr>
									<td><code>payed_amount</code></td>
									<td><code>Grand total</code></td>
								</tr>
								<tr>
									<td><code>payment_mode</code></td>
									<td><code>Payment mode (cash or card)</code></td>
								</tr>
								<tr>
									<td><code>checkout_type</code></td>
									<td><code>Checkout type (Delivery, Carry-out, Dine-in)</code></td>
								</tr>
								<tr>
									<td><code>is_preorder</code></td>
									<td><code>Is pre-order ? (Yes or No)</code></td>
								</tr>

								<tr>
									<td><code>first_name</code></td>
									<td><code>Customer first name</code></td>
								</tr>
								<tr>
									<td><code>last_name</code></td>
									<td><code>Customer last name</code></td>
								</tr>
								<tr>
									<td><code>email</code></td>
									<td><code>Customer email</code></td>
								</tr>
								<tr>
									<td><code>phone</code></td>
									<td><code>Customer phone</code></td>
								</tr>
								<tr>
									<td><code>city</code></td>
									<td><code>Customer city</code></td>
								</tr>
								<tr>
									<td><code>zipcode</code></td>
									<td><code>Customer zipcode</code></td>
								</tr>
								<tr>
									<td><code>address_line_1</code></td>
									<td><code>Customer address line 1</code></td>
								</tr>
								<tr>
									<td><code>address_line_2</code></td>
									<td><code>Customer address line 2</code></td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
			</div>

			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">Update</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>

@endsection