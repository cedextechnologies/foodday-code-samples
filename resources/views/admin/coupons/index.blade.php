@extends('layouts.admin')
@section('title', 'Coupons')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Coupons") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Coupons") }}</h3>
				<div class="card-tools">
					<a href="{{ route('admin.coupons.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus mr-1"></i>{{ __('Add New Coupon') }}</a>
				</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">
				
				@if(!$coupons->isEmpty())
				<div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped datatable">
						<thead>
							<tr>
								<th>{{ __("Code") }}</th>
								<th>{{ __("Discount type") }}</th>
								<th>{{ __("Discount") }}</th>
								<th>{{ __("Usage Limit") }}</th>
								<th>{{ __("Used Coupons") }}</th>
								<th>{{ __("Start Date") }}</th>
								<th>{{ __("End Date") }}</th>
								<th width="5%">{{ __("Enable/Disable") }}</th>
								<th>{{ __("Action") }}</th>
							</tr>
						</thead>
						<tbody>

							@foreach($coupons as $key => $coupon)

							<tr>
								<td>{{$coupon->code}}</td>
								<td>{{$coupon->discount_type==1?'Percentage':'Fixed Amount'}}</td>
								<td>{{$coupon->discount}}</td>
								<td>{{$coupon->usage_limit}}</td>
								<td>{{$coupon->used_count}}</td>
								<td>{{$coupon->start_date?$coupon->start_date->format('d M, Y'):'' }}</td>
								<td>{{$coupon->start_date?$coupon->end_date->format('d M, Y'):'' }}</td>
								<td>
									<div class="form-group">
										<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											<input type="checkbox" class="custom-control-input js-coupon-status status-toggle" status-data="{{ $coupon->id }}" status-url="admin.coupons.updateStatus" id="customSwitch-{{$key}}" {{$coupon->isActive()?'checked':''}}>
											<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
										</div>
									</div>
								</td>
								<td>
									<a href="{{route('admin.coupons.edit',[$coupon])}}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>

									<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="js-delete-coupon"><i class="fas fa-trash"></i></a>
									<form method="post" action="{{route('admin.coupons.destroy', $coupon)}}" class="js-form-delete-coupon">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
									</form>		
								</td>
							</tr>

							@endforeach

							<tfoot>
							</tfoot>
						</table>
					</div>
					@endIf		

				</div>
			</div>
		</div>
	</div>

	@endsection