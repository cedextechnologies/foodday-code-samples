@extends('layouts.admin')
@section('title', 'Coupons')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.coupons.index') }}">{{ __("Coupons") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Edit Coupon") }}</li>
</ol>
@endsection

@section('content')


<div class="">
	<!-- Horizontal Form -->
	<form class="form-horizontal" method="post" action="{{ route('admin.coupons.update', $coupon) }}">
		
		@csrf
		@method('PUT')

		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">Edit Coupon</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->

			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Code') }}</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('code') ? ' is-invalid' : '' }}" name="code" value="{{ old('code', $coupon->code) }}">

								@if ($errors->has('code'))
								<span class="invalid-feedback" style="display: block !important;" role="alert">
									<strong>{{ $errors->first('code') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Discount') }}</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('discount') ? ' is-invalid' : '' }}" name="discount" value="{{ old('discount', $coupon->discount) }}">

								@if ($errors->has('discount'))
								<span class="invalid-feedback" style="display: block !important;" role="alert">
									<strong>{{ $errors->first('discount') }}</strong>
								</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="inputEmail3" class="col-sm-5 control-label">{{ __('Minimum Order Amount') }}</label>
							<div class="col-sm-12">
								<div class="input-group"><div class="input-group-prepend"><span class="input-group-text"><i>₹</i></span></div>
								<input type="text" class="form-control{{ $errors->has('min_amount') ? ' is-invalid' : '' }}" name="min_amount" value="{{ old('min_amount', $coupon->min_amount) }}">
							</div>
							@if ($errors->has('min_amount'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('min_amount') }}</strong>
							</span>
							@endif
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Start Date') }}</label>
						<div class="col-sm-12">
							<input type="text" class="form-control{{ $errors->has('start_date') ? ' is-invalid' : '' }} datetimepicker" name="start_date" id="inputEmail3" autocomplete="off" name="start_date" value="{{ old('start_date', $coupon->start_date) }}">
							@if ($errors->has('start_date'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('start_date') }}</strong>
							</span>
							@endif									
						</div>
					</div>						
					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">{{ __('End Date') }}</label>
						<div class="col-sm-12">
							<input type="text" class="form-control{{ $errors->has('end_date') ? ' is-invalid' : '' }} datetimepicker" name="end_date" id="inputEmail3" autocomplete="off" value="{{ old('end_date', $coupon->end_date) }}">
							@if ($errors->has('end_date'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('end_date') }}</strong>
							</span>
							@endif									
						</div>
					</div>
				</div>
				<div class="col-md-6">

					<div class="form-group">
						<label for="inputDiscount_type3" class="col-sm-4 control-label">{{ __('Discount Type') }}</label>
						<div class="col-sm-12">
							<select name="discount_type" class="form-control{{ $errors->has('discount_type') ? ' is-invalid' : '' }}">
								<option  {{ $coupon->discount_type=='1'?'selected':'' }} value="1">Percentage</option>
								<option {{ $coupon->discount_type=='0'?'selected':'' }} value="0">Fixed Amount</option>
							</select>
							@if ($errors->has('discount_type'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('discount_type') }}</strong>
							</span>
							@endif
						</div>
					</div>	

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Usage Limit') }}</label>
						<div class="col-sm-12">
							<input type="text" name="usage_limit" class="form-control{{ $errors->has('	usage_limit') ? ' is-invalid' : '' }}" id="inputEmail3" value="{{ old('usage_limit', $coupon->usage_limit) }}">
							@if ($errors->has('usage_limit'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('usage_limit') }}</strong>
							</span>
							@endif									
						</div>
					</div>

					<div class="form-group">
						<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Status') }}</label>
						<div class="col-sm-12">
							<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
								<option {{$coupon->status ==1?'selected':''}} value="1">Active</option>
								<option {{$coupon->status ==0?'selected':''}} value="0">Inactive</option>
							</select>
							@if ($errors->has('status'))
							<span class="invalid-feedback" style="display: block !important;" role="alert">
								<strong>{{ $errors->first('status') }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- /.card-body -->
		<div class="card-footer">
			<button type="submit" class="btn btn-success">{{ __('Update') }}</button>
		</div>
		<!-- /.card-footer -->
	</form>
</div>
</div>
@endsection            