@extends('layouts.admin')
@section('title', 'Admin Users')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Admin Users") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Admin Users") }}</h3>

				<div class="card-tools row">	

					<div class="col-md-8">
						<form method="get" action="{{route('admin.users.index')}}">
							<div class="input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><i class="fas fa-search"></i></span>
								</div>
								<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control">
								<input type="submit" style="display: none;">
							</div>
						</form>
					</div>

					<div class="col-md-4">
						<a href="{{ route('admin.users.create') }}" class="btn btn-primary btn-sm"><i class="fa fa-plus mr-1"></i>Add New User</a>
					</div>

				</div>
				
			</div>
			<!-- /.card-header -->
			<div class="card-body">

				@if(!$users->isEmpty())
				<div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>{{ __("Name") }}</th>
								<th>{{ __("Email") }}</th>
								<th width="5%">{{ __("Enable/Disable") }}</th>
								<th>{{ __("Action") }}</th>
							</tr>
						</thead>
						<tbody>

							@foreach($users as $key => $user)

							<tr>
								<td>{{$user->name}}</td>
								<td>{{$user->email}}</td>
								<td>
									<div class="form-group">
										<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											<input type="checkbox" class="custom-control-input js-user-status status-toggle" status-data="{{  $user->id  }}" status-url="admin.users.updateStatus" id="customSwitch-{{$key}}" {{$user->isActive()?'checked':''}}>
											<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
										</div>
									</div>
								</td>
								<td>
									<a href="{{route('admin.users.edit',[$user])}}" data-toggle="tooltip" title="Edit"><i class="fas fa-edit"></i></a>
									@if($user->id != '1')
									<a href="javascript:void(0);" data-toggle="tooltip" title="Delete" class="js-delete-user"><i class="fas fa-trash"></i></a>
									<form method="post" action="{{route('admin.users.destroy', $user)}}" class="js-form-delete-user">
										@csrf
										<input type="hidden" name="_method" value="DELETE">
									</form>	
									@endif	
								</td>
							</tr>

							@endforeach

							<tfoot>
							</tfoot>
						</table>
					</div>
					@endIf		

				</div>
			</div>
		</div>
	</div>

	@endsection