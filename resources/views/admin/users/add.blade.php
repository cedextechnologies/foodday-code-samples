@extends('layouts.admin')
@section('title', 'Admin Users')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item"><a href="{{ route('admin.users.index') }}">{{ __("Admin Users") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Add User") }}</li>
</ol>
@endsection

@section('content')


<div class="">
	<!-- Horizontal Form -->
	<div class="card card-default">
		<div class="card-header">
			<h3 class="card-title">Add Admin User</h3>
		</div>
		<!-- /.card-header -->
		<!-- form start -->
		<form class="form-horizontal" method="post" action="{{ route('admin.users.store') }}">
			@csrf
			<div class="card-body">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Name') }}</label>
							<div class="col-sm-12">
								<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}">

								@if ($errors->has('name'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('name') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<div class="form-group">
							<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Password') }}</label>
							<div class="col-sm-12">
								<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" value="">

								@if ($errors->has('password'))
									<span class="invalid-feedback" style="display: block !important;" role="alert">
										<strong>{{ $errors->first('password') }}</strong>
									</span>
								@endif
							</div>
						</div>

						<label for="inputPassword3" class="col-sm-4 control-label">{{ __('Status') }}</label>
						<div class="col-sm-12">
							<select name="status" class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}">
								<option value="1">Active</option>
								<option value="0">Inactive</option>
							</select>
							@if ($errors->has('status'))
								<span class="invalid-feedback" style="display: block !important;" role="alert">
									<strong>{{ $errors->first('status') }}</strong>
								</span>
							@endif
						</div>
					</div>
					<div class="col-md-6">

						<div class="form-group">

							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Email') }}</label>
								<div class="col-sm-12">
									<input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="inputEmail3" value="">
									@if ($errors->has('email'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('email') }}</strong>
										</span>
									@endif									
								</div>
							</div>

							<div class="form-group">
								<label for="inputEmail3" class="col-sm-4 control-label">{{ __('Confirm Password') }}</label>
								<div class="col-sm-12">
									<input type="password" class="form-control{{ $errors->has('confirm_password') ? ' is-invalid' : '' }}" name="confirm_password" id="inputEmail3">
									@if ($errors->has('confirm_password'))
										<span class="invalid-feedback" style="display: block !important;" role="alert">
											<strong>{{ $errors->first('confirm_password') }}</strong>
										</span>
									@endif									
								</div>
							</div>

						</div>
					</div>
				</div>
			</div>
			<!-- /.card-body -->
			<div class="card-footer">
				<button type="submit" class="btn btn-success">{{ __('Add') }}</button>
			</div>
			<!-- /.card-footer -->
		</form>
	</div>
</div>
@endsection            