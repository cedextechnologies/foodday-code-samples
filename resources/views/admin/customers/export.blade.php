<table id="example1" class="table table-bordered table-striped">
	<thead>
		<tr>
			<th>{{ __("Name") }}</th>
			<th>{{ __("Email") }}</th>
			<th>{{ __("Phone") }}</th>
			<th>{{ __("ZIP Code") }}</th>
		</tr>
	</thead>
	<tbody>

		@foreach($customers as $key => $customer)

		<tr>
			<td>{{$customer->first_name}} {{$customer->last_name}}</td>
			<td>{{$customer->email}}</td>
			<td>{{$customer->phone}}</td>
			<td>{{$customer->zipcode}}</td>
		</tr>

		@endforeach

		<tfoot>
		</tfoot>
	</table>