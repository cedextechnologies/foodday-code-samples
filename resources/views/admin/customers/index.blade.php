@extends('layouts.admin')
@section('title', 'Customers')

@section('breadcrumb')
<ol class="breadcrumb float-sm-right">
	<li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
	<li class="breadcrumb-item active">{{ __("Customers") }}</li>
</ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-header">
				<h3 class="card-title">{{ __("Registered Customers") }}</h3>
				<div class="card-tools">
					<div class="row">
						<div class="col-md-7">
							<form method="get" action="{{route('admin.customers')}}">
								<div class="input-group mb-3">
									<div class="input-group-prepend search-icon-submit">
										<span class="input-group-text"><i class="fas fa-search"></i></span>
									</div>
									<input type="text" placeholder="{{ __('Search') }}" name="search" autocomplete="off" value="{{ old('search') }}" class="form-control" id="search-customer">
									<input type="submit" style="display: none;">

								</div>
							</form>
						</div>
						<div class="col-md-5">
							<form action="{{route('admin.customers.export')}}" method="post">
								@csrf
								<input type="hidden" name="search" class="form-control" id="search-customer-cpy" value="{{ old('search') }}">
								<button type="submit" class="btn btn-primary"><i class="far fa-file-excel mr-2"></i>Export to Excel</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			<!-- /.card-header -->
			<div class="card-body">

				@if(!$customers->isEmpty())
				<div class="table-responsive">
					<table id="example1" class="table table-bordered table-striped">
						<thead>
							<tr>
								<th>{{ __("Name") }}</th>
								<th>{{ __("Email") }}</th>
								<th>{{ __("Phone") }}</th>
								<th>{{ __("ZIP Code") }}</th>
								<th width="5%">{{ __("Enable/Disable") }}</th>
								<th>{{ __("Action") }}</th>
							</tr>
						</thead>
						<tbody>

							@foreach($customers as $key => $customer)

							<tr>
								<td>{{$customer->first_name}} {{$customer->last_name}}</td>
								<td>{{$customer->email}}</td>
								<td>{{$customer->phone}}</td>
								<td>{{$customer->zipcode}}</td>
								<td>
									<div class="form-group">
										<div class="custom-control custom-switch custom-switch-off-danger custom-switch-on-success">
											<input type="checkbox" class="custom-control-input status-toggle" status-data="{{ $customer->id }}" status-url="admin.customers.updateStatus" id="customSwitch-{{$key}}" {{$customer->isActive()?'checked':''}}>
											<label class="custom-control-label" for="customSwitch-{{$key}}"></label>
										</div>
									</div>
								</td>
								<td><a href="{{route('admin.customers.edit',[$customer])}}" data-toggle="tooltip" title="Edit"><i
									class="fas fa-edit"></i></a>
								</td>
							</tr>

							@endforeach

							<tfoot>
							</tfoot>
						</table>
					</div>
					@endIf		

				</div>
			</div>
			{{ $customers->links() }}
		</div>
	</div>

	@endsection