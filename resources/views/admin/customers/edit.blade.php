@extends('layouts.admin')
@section('title', 'Edit Customer')

@section('breadcrumb')
    <ol class="breadcrumb float-sm-right">
        <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a></li>
        <li class="breadcrumb-item"><a href="{{ route('admin.customers') }}">{{ __('Customers') }}</a></li>
        <li class="breadcrumb-item active">{{ __("Edit") }}</li>
    </ol>
@endsection

@section('content')

<div class="row">
	<div class="col-lg-12">
		<!-- Horizontal Form -->
		<div class="card card-default">
			<div class="card-header">
				<h3 class="card-title">{{ __("Edit Customer") }}</h3>
			</div>
			<!-- /.card-header -->
			<!-- form start -->
			<form role="form" action="{{ route('admin.customers.update',$customer) }}" method="POST" enctype="multipart/form-data">
				<div class="card-body">
					<div class="row">
							{{ csrf_field() }}
							<input type="hidden" name="_method" value="PUT">

							<div class="col-md-6">
								<div class="form-group">
									<label for="inputEmail3" class="col-sm-4 control-label">{{ __("First Name") }}</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" name="first_name" value="{{ $customer->first_name }}">

										@if ($errors->has('first_name'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('first_name') }}</strong>
								            </span>
	            						@endif

									</div>

								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Last Name") }}</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $customer->last_name }}">

										@if ($errors->has('last_name'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('last_name') }}</strong>
								            </span>
	            						@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Phone") }}</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ $customer->phone }}">

										@if ($errors->has('phone'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('phone') }}</strong>
								            </span>
	            						@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Email") }}</label>

									<div class="col-sm-12">
										<input type="email" class="form-control" name="email" value="{{ $customer->email }}">
									</div>
								</div>
							</div>
							<div class="col-md-6">
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Password") }}</label>

									<div class="col-sm-12">
										<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">

										@if ($errors->has('password'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('password') }}</strong>
								            </span>
	            						@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Zip code") }}</label>

									<div class="col-sm-12">
										<input type="text" class="form-control{{ $errors->has('zipcode') ? ' is-invalid' : '' }}" name="zipcode" value="{{ $customer->zipcode }}">

										@if ($errors->has('zipcode'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('zipcode') }}</strong>
								            </span>
	            						@endif

									</div>
								</div>
								<div class="form-group">
									<label for="inputPassword3" class="col-sm-4 control-label">{{ __("Status") }}</label>
									<div class="col-sm-12">
										<select class="form-control{{ $errors->has('status') ? ' is-invalid' : '' }}" name="status">
											<option value="1" {{$customer->isActive()?'selected':''}}>Active</option>
											<option value="0" {{$customer->isActive() != true ?'selected':''}}>Inactive</option>
										</select>

										@if ($errors->has('status'))
								            <span class="invalid-feedback" style="display: block !important;" role="alert">
								              <strong>{{ $errors->first('status') }}</strong>
								            </span>
	            						@endif

									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /.card-body -->
					<div class="card-footer">
						<button type="submit" class="btn btn-success">{{ __("Update") }}</button>
					</div>
				<!-- /.card-footer -->
			</form>
		</div>
	</div>
</div>

@endsection