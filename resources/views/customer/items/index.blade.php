@extends('layouts.customer')
@section('title','Menu')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-content text-center w-75 m-auto">
                    <h1>Our Menu</h1>
                    <p class="lead">The mission is simple: serve delicious, affordable food that guests will want to return to week after week.</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')

@include('shared.components.customer.delivery')

<section class="menu-section">
    <div class="container">
        <div class="menu-nav">
            <ul class="nav nav-tabs" id="myTab" role="tablist">
                <li class="nav-item">
                    <a class="nav-link @if(isset($itemCategory))@else{{ 'active' }}@endif" href="{{ route('items.index') }}" >All</a>
                </li>
                @forelse($categories as $category)
                <li class="nav-item">
                    <a class="nav-link @if(isset($itemCategory) && $itemCategory->id==$category->id){{ 'active' }}@endif" href="{{ route('category.items',$category) }}" >{{ $category->name }}</a>
                </li>
                @empty
                @endforelse
            </ul>
        </div>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="category1" role="tabpanel" aria-labelledby="category1-tab">
                <div class="row">
                    @forelse($items as $item)
                    <div class="col-md-6">
                        <div class="menu-item">
                            <div class="menu-item-img imgLiquid">
                                <img src="{{ asset('storage/'.$item->image) }}" alt="Image">
                            </div>
                            <div class="menu-item-content">
                                <div class="item-text">
                                    <h5>{{ $item->name }} {{ $item->getItemType() }}</h5>
                                    <p>{{ $item->description }}</p>
                                </div>
                                <div class="item-price">
                                    <span>{{ $item->sizes->count()>0?'Custom':format_currency($item->price) }}</span>
                                    <a href="{{ route('items.show',$item) }}" class="secondary-btn btn-xsml btn-modal" data-toggle="modal" data-target="#menuItemModal2">add to cart</a>
                                </div>
                            </div>
                        </div>
                    </div>

                    @empty
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</section>
@endsection