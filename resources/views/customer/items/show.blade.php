<form action="{{ route('customer.cart.update',$item) }}" class="cart-form" method="post" id="cart-form">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select Product Choices </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row align-items-center">
            <div class="col-md-6">
                <h6>{{ $item->name }} {{ $item->getItemType() }}</h6>
                <input type="hidden" name="id" value="{{ $item->id }}" id="item_id">
            </div>
            <div class="col-md-6">
                <div class="item-price justify-content-end flex-row">
                    <input type="number" min="1" value="1" name="quantity" id="quantity">
                </div>
            </div>
        </div>
        @if($item->sizes->isNotEmpty())
        <div class="row mb-3">
            <div class="col-md-12">
                <h6>Size</h6>
                @foreach($item->sizes as $size)
                <div class="single-row">
                    <div class="single-col"><input type="radio" name="size" id="item_{{ $size->id }}" value="{{ $size->id }}" class="w-auto h-auto m-0 size_id"><label for="item_{{ $size->id }}">{{ $size->size->size }}</label></div>
                    <div class="single-col"><span>{{ format_currency($size->price) }}</span></div>
                </div>
                @endforeach
            </div>
        </div>
        @endif
        <div class="row mb-3">
            @if($item->modifiers->isNotEmpty())
            @foreach($item->modifiers as $modifier)
            <div class="col-md-12">
                <h6>{{ $modifier->title }}</h6>
                <input type="hidden" name="modifier_id[]" value="{{ $modifier->id }}">
                @foreach($modifier->ItemModifierDetails as $modifierItem)
                <div class="single-row">
                    <div class="single-col"><input type="checkbox" name="modifier_item_id[{{ $modifier->id }}][]" id="modifier_{{ $modifierItem->id }}" value="{{ $modifierItem->id }}" class="w-auto h-auto m-0 modifiers"><label for="modifier_{{ $modifierItem->id }}">{{ $modifierItem->name }}</label></div>
                    <div class="single-col"><span>{{ format_currency($modifierItem->price) }}</span></div>
                </div>
                @endforeach
            </div>
            @endforeach
            @endif
        </div>
        <textarea placeholder="Enter any additional information about your order" class="m-0" name="instruction" id="instruction"></textarea>
    </div>
    <div class="modal-footer">
        <button type="button" onclick="add_to_cart()" class="secondary-btn btn-sml btn-block">Continue</button>
    </div>
</form>
<script src="{{asset('customer/assets/js/app.js')}}"></script>