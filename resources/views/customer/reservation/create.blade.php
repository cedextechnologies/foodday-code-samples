@extends('layouts.customer')
@section('title','Reservation')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-content text-center w-75 m-auto">
                    <h1>Reservation</h1>
                    <p class="lead">Visiting our restaurant will fill you with warmth and put you in food heaven.</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection
@section('content')
@include('customer.components.tablebooking')
@endsection