@extends('layouts.customer')
@section('title','Cart')

@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header-content text-center w-75 m-auto">
					<h1>Cart</h1>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection

@section('content')
<!-- Section : Cart -->

@include('shared.components.customer.delivery')


<section class="cart-section">
	<div class="container">
		@include('flash::message')
		@if(isset($cart) && !empty($cart->items->count()>0))
		<div class="row">
			<div class="col-md-12">
				<table class="table table-borderless table-responsive-sm cart-table mb-4">
					<thead>
						<tr>
							<th scope="col"></th>
							<th scope="col">Product</th>
							<th scope="col">Price</th>
							<th scope="col">Quantity</th>
							<th scope="col">Total</th>
						</tr>
					</thead>
					<tbody>

						@forelse($cart->items as $cartItem)
						<tr>
							<td>
								<a href="#" class="cart-item-img imgLiquid"><img src="{{ asset('storage/'.$cartItem->item->image) }}" alt="Image"></a>
							</td>
							<td class="product-name"><a href="#">{{ $cartItem->item->name }} {{ $cartItem->getSizeText() }} {{ format_currency($cartItem->getItemPrice()) }}</a>
								<ul>
									@forelse($cartItem->modifiers as $modifier)
									<li>{{ $modifier->itemModifierDetail->name }}&nbsp;({{ format_currency($modifier->itemModifierDetail->price) }})</li>
									@empty
									@endforelse
								</ul>
							</td>
							<td>{{ format_currency($cartItem->getPrice()) }}</td>
							<td><input type="number" min="1" value="{{ $cartItem->quantity }}" class="item-quantity" url="{{ route('customer.cart.item.update-quantity',$cart->token) }}" row="{{ $cartItem->row_id }}"></td>
							<td>{{ format_currency($cartItem->getItemTotals($cartItem)) }}</td>
							<td>
								<form action="{{ route('customer.cart-item.delete',$cart) }}" method="post" id="delete-cart-item" class="delete-cart-item">
									@csrf
									<input name="row_id" value="{{ $cartItem->row_id }}" style="display:none">
									<button type="submit"><i class="fa fa-trash" aria-hidden="true" title="Delete"></i></button>
								</form>
							</td>
						</tr>
						@empty
						<h3><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Your cart is empty</h3>
						@endforelse
					</tbody>
				</table>
			</div>
		</div>
		<div class="row justify-content-start mb-4 coupen-field">
			<div class="col-md-6 d-lg-flex">
				<input type="text" placeholder="Coupon" name="coupon-code" value="{{ $cart->code }}" id="coupon-code" required>
				@if($cart->coupon_applied==0)
				<button type="submit" class="secondary-btn btn-block" id="apply-coupon" url="{{ route('cart.apply-coupon',$cart->token) }}">Apply Coupon</button>
				@else
				<button type="submit" class="secondary-btn btn-block" id="remove-coupon" url="{{ route('cart.remove-coupon',$cart->token) }}">Remove Coupon</button>
				@endif
			</div>
		</div>
		<div class="row justify-content-end">
			<div class="col-md-6">
				<h4 class="pb-4">Cart totals</h4>
				<table class="table-borderless table-responsive-sm subtotal-table w-100">
					<tbody>
						<tr>
							<th>Sub Total</th>
							<td>{{ format_currency($cartTotals['subtotal']) }}</td>
						</tr>
						<tr>
							<th>Tax({{ $cartTotals['tax'] }}%)</th>
							<td>{{ format_currency($cartTotals['tax_amount']) }}</td>
						</tr>
						<tr>
							<th>Delivery Charge </th>
							<td>{{ format_currency($cartTotals['delivery_charge']) }}</td>
						</tr>
						@if($cart->coupon_applied==1)
						<tr>
							<th>Coupon Discount</th>
							<td>-{{ format_currency($discount) }}</td>
						</tr>
						<tr>
							<th>Grand Total </th>
							<td>{{ format_currency($cartTotals['order_total']-$discount) }}</td>
						</tr>
						@else
						<tr>
							<th>Grand Total </th>
							<td>{{ format_currency($cartTotals['order_total']) }}</td>
						</tr>
						@endif
						
					</tbody>
				</table>
				<a href="{{ route('customer.cart.proceed') }}" class="secondary-btn btn-block">proceed to checkout</a>
			</div>
		</div>
		@else
		<h3 align="center"><i class="fa fa-shopping-cart"></i>&nbsp;&nbsp;Your cart is empty</h3>
		@endif
	</div>
</section>
@endsection