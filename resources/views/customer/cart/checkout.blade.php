@extends('layouts.customer')
@section('title','Checkout')

@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="header-content text-center w-75 m-auto">
					<h1>Checkout</h1>
				</div>
			</div>
		</div>
	</div>
</header>
@endsection

@section('content')
<!-- Section : checkout -->

@include('shared.components.customer.delivery')

<section class="checkout-section">

    <div class="container">
        <form action="{{ route('customer.cart.checkout') }}" method="post" class="ajax-form" novalidate id="checkout-frm" autocomplete=off>
            <div class="row flex-column-reverse flex-md-row">
                <div class="col-md-6 mb-4">
                    <div class="row">
                        <div class="col-md-6">
                            <select name="checkout_type">
                                @if(getStoreSettings('checkout_delivery')==1)
                                <option value="1">Delivery</option>
                                @endif
                                @if(getStoreSettings('checkout_carry_out')==1)
                                <option value="2">Carryout</option>
                                @endif
                            </select>
                        </div>
                        <div class="col-md-6 d-flex align-items-center mb-4">
                            @if(getStoreSettings('allow_pre_order')==1)
                            <input type="radio" name="is_preorder" value="0" id="radio3" class="w-auto h-auto m-0 is_preorder" checked="checked">
                            <label for="radio3" class="mx-2 mb-0">Now</label>
                            <input type="radio" name="is_preorder" value="1" id="radio4" class="w-auto h-auto m-0 is_preorder">
                            <label for="radio4" class="mx-2 mb-0">Choose Time</label>
                            @else
                            <input type="radio" name="is_preorder" value="0" id="radio3" class="w-auto h-auto m-0" checked="checked" style="display: none;">
                            @endif
                        </div>
                        <div class="col-md-6 pre-order-div" style="display: none">
                            <input type="text" class="datepicker" placeholder="Pre Order Date" name="preorder_date" autocomplete="preorder_date">
                            <span id="preorder_date_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6 pre-order-div" style="display:none">
                            <input type="text" class="clockpicker" data-autoclose="true" placeholder="Pre Order Time" name="preorder_time" autocomplete="preorder_time">
                            <span id="preorder_time_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="First Name" name="first_name" value="{{ isset($user)? $user->first_name:'' }}" required>
                            <span id="first_name_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Last Name" name="last_name" value="{{ isset($user)? $user->last_name:'' }}" required>
                            <span id="last_name_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="email" placeholder="Email" name="email" value="{{ isset($user)? $user->email:'' }}" required>
                            <span id="email_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Phone" name="phone" value="{{ isset($user)? $user->phone:'' }}" required>
                            <span id="phone_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Zip Code" name="zipcode" id="zipcode" value="{{ isset($user)? $user->zipcode:'' }}" required>
                            <span id="zipcode_error" class="error-label"></span>
                            <input type="hidden" name="latitude" id="latitude" value="{{ isset($location['latitude'])? $location['latitude']:'' }}">
                            <input type="hidden" name="longitude" id="longitude" value="{{ isset($location['longitude'])? $location['longitude']:'' }}">
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Door Code" name="door" value="{{ isset($user)? $user->door:'' }}" required>
                            <span id="door_error" class="error-label"></span>
                        </div>
                        <div class="col-md-6">
                            <input type="text" placeholder="Company" name="company" value="{{ isset($user)? $user->company:'' }}" required>
                            <span id="company_error" class="error-label"></span>
                        </div>
                    </div>
                    <textarea placeholder="Address1" name="address_line_1" rows="3" required></textarea>
                    <span id="address_line_1_error" class="error-label"></span>
                    <textarea placeholder="Address2" name="address_line_2" rows="3"></textarea>
                    <span id="address_line_2_error" class="error-label"></span>
                    @if(getStoreSettings('accept_cash')==1)
                    <div class="custom-radio">
                        <input type="radio" id="radio1" name="payment_mode" value="1" checked="checked">
                        <label for="radio1">Pay via cash</label>
                        <img src="{{ asset('images/cash.png') }}" alt="Icon">
                    </div>
                    @endif
                    @if(getStoreSettings('stripe_payment')==1)
                    <div class="custom-radio">
                        <input type="radio" id="radio2" name="payment_mode" value="2">
                        <label for="radio2">Pay via strip</label>
                        <img src="{{ asset('images/stripe.png') }}" alt="Icon">
                    </div>
                    @endif
                    @if(getStoreSettings('paypal_payment')==1)
                    <div class="custom-radio">
                        <input type="radio" id="radio5" name="payment_mode" value="3">
                        <label for="radio5">Pay via paypal</label>
                        <img src="{{ asset('images/paypal.png') }}" alt="Icon">
                    </div>
                    @endif
                    <button type="submit" class="secondary-btn">Order now</button>
                </div>
                <div class="col-md-6 d-flex d-md-block flex-column-reverse flex-md-column">
                    <div class="checkout-cart mb-4">
                        <h5>Cart</h5>
                        <ul>
                            @forelse($cart->items as $cartItem)
                            <li class="cart-item">
                                <div class="cart-text">
                                    <h6>{{ $cartItem->item->name }} {{ $cartItem->getSizeText() }} {{ format_currency($cartItem->getItemPrice()) }}</h6>
                                    <p>{{ $cartItem->instruction }}</p>
                                    <ul>
                                        @forelse($cartItem->modifiers as $modifier)
                                        <li>
                                            {{ $modifier->itemModifierDetail->name }}&nbsp;({{ format_currency($modifier->itemModifierDetail->price) }})
                                        </li>
                                        @empty
                                        @endforelse
                                    </ul>
                                </div>
                                <div class="cart-price">
                                    {{ $cartItem->quantity }} x {{ format_currency($cartItem->getPrice()) }}
                                </div>
                            </li>
                            @empty
                            @endforelse
                        </ul>
                        <table class="table-borderless table-responsive-sm subtotal-table w-100 m-0">
                            <tbody>
                                <tr>
                                    <th>Sub Total</th>
                                    <td>{{ format_currency($cartTotals['subtotal']) }}</td>

                                </tr>
                                <tr>
                                    <th>Tax(5.0%)</th>
                                    <td>{{ format_currency($cartTotals['tax_amount']) }}</td>
                                </tr>
                                <tr>
                                    <th>Delivery Charge </th>
                                    <td>{{ format_currency($cartTotals['delivery_charge']) }}</td>
                                </tr>
                                @if($cart->coupon_applied==1)
                                <tr>
                                    <th>Coupon Discount</th>
                                    <td>-{{ format_currency($discount) }}</td>
                                </tr>
                                <tr>
                                    <th>Grand Total </th>
                                    <td>{{ format_currency($cartTotals['order_total']-$discount) }}</td>
                                </tr>
                                @else
                                <tr>
                                    <th>Grand Total </th>
                                    <td>{{ format_currency($cartTotals['order_total']) }}</td>
                                </tr>
                                @endif
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
<form action="" method="POST" id="stripe-form" class="ajax-form">
    @csrf
    <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="{{ env('STRIPE_PUB_KEY') }}"
    data-name="Foodday"
    data-description="Online food ordering application"
    data-image="{{ asset('images/logo.png') }}"
    data-locale="auto"
    data-currency="usd">
</script>
</form>
@endsection