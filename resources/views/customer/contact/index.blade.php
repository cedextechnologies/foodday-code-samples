@extends('layouts.customer')
@section('title','Contact')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-content text-center w-75 m-auto">
                    <h1>Contact</h1>
                    <p class="lead">Let us know all your thoughts about us</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<section class="contact-section">

    <div class="container">
        <div class="row">
            <div class="col-md-6 mb-4">
                <h3 class="pb-4">Contact Us</h3>
                <p>As always we are interested in your feedback and suggestions, so please feel free to contact us directly.</p>
                <ul>
                    <li><span>Phone</span>{{ getStoreSettings('contact_number') }}</li>
                    <li><span>Email</span>{{ getStoreSettings('contact_email') }} </li>
                    <li><span>Address</span>{{ getStoreSettings('contact_address') }}</li>
                    <li><span>Follow</span>
                        <ul class="social-nav">
                            <li><a href="{{ getStoreSettings('youtube') }}" target="blank"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('pinterest') }}" target="blank"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('instagram') }}" target="blank"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('twitter') }}" target="blank"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
                            <li><a href="{{ getStoreSettings('facebook') }}" target="blank"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
                        </ul>
                    </li>
                </ul>
            </div>
            <div class="col-md-6">
                <div class="contact-form">
                    <form action="{{ route('customer.contact.store') }}" method="post" class="ajax-form">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Name" name="name" required>
                        <span id="name_error" class="error-label"></span>
                        <input type="email" placeholder="Email" name="email" required>
                        <span id="email_error" class="error-label"></span>
                        <textarea placeholder="Message" name="message" required></textarea>
                        <span id="message_error" class="error-label"></span><br>
                        <button type="submit" class="secondary-btn">Contact us</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

<!-- Section : contact -->
<section class="map-section p-0">
    <div class="container-fluid p-0">
        <div class="row">
            <div class="col-md-12">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2483.5778761531797!2d-0.3322891!3d51.5026134!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760d77bb8376eb%3A0x5385c548f895c2b1!2sBoston+Rd%2C+London%2C+UK!5e0!3m2!1sen!2sin!4v1475214694903" width="100%"
                height="500px" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
        </div>
    </div>
</section>
@endsection