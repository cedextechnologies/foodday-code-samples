<!-- Section : Booking -->

<section class="booking-section">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Book a table</h2>
        </div>
        <div class="row ">
            <div class="col-md-10 offset-md-1">
                <div class="booking-form text-center">
                    <form action="{{ route('customer.reserve-table') }}" class="ajax-form" method="post" autocomplete="off" novalidate>
                        {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-4">
                                <input type="text" placeholder="Name" name="name" class="input-text" required>
                                <span id="name_error" class="error-label"></span>
                            </div>
                            <div class="col-md-4">
                                <input type="text" placeholder="Phone" name="mobile" class="input-text" required>
                                <span id="mobile_error" class="error-label"></span>
                            </div>
                            <div class="col-md-4">
                                <input type="email" placeholder="Email" name="email" class="input-text" required>
                                <span id="email_error" class="error-label"></span>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="datepicker input-text" placeholder="   Booking Date" name="booking_date" required>
                                <span id="booking_date_error" class="error-label"></span>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="clockpicker input-text" data-autoclose="true" placeholder="Booking Time" class="m-0" name="booking_time" required>
                                <span id="booking_time_error" class="error-label"></span>
                            </div>
                            <div class="col-md-4">
                                <input type="text" class="input-text" data-autoclose="true" placeholder="Party Size" name="party_size" required>
                                <span id="party_size_error" class="error-label"></span>
                            </div>
                            <div class="col-md-12"><textarea class="input-text" placeholder="Message" name="extra_notes" required></textarea>
                                <span id="extra_notes_error" class="error-label"></span>
                            </div>

                        </div>
                        <button type="submit" class="secondary-btn">book a table</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>