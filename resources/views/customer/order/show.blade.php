
<h5 class="m-0">ORDER #{{ $order->order_reference }} </h5>
<time class="d-block mb-4">{{ getDateString($order->created_at) }}</time>
<table class="table table-borderless table-responsive-sm mb-4 profile-table table-hover">
    <tbody class="w-100 d-table">
        <tr>
            <td>Status</td>
            <td><span class="badge badge-dark">{{ $order->getStatus() }}</span></td>
        </tr>
        <tr>
            <td>Order Total</td>
            <td>{{ format_currency($order->subtotal) }}</td>
        </tr>
        <tr>
            <td>Tax({{ $order->tax }}%)</td>
            <td>{{ format_currency($order->tax_amount) }}</td>
        </tr>
        <tr>
            <td>Delivery Charge</td>
            <td>{{ format_currency($order->delivery_charge) }}</td>
        </tr>
        @if($order->coupon_code !='')
        <tr>
            <td>Coupon Discount</td>
            <td>{{ format_currency($order->coupon_discount) }}</td>
        </tr>
        @endif
        <tr>
            <td>Payed Amount</td>
            <td>{{ format_currency($order->payed_amount) }}</td>
        </tr>
    </tbody>
</table>
<table class="table table-borderless table-responsive-sm m-0 profile-table table-hover">
    <thead class="w-100 d-table">
        <tr>
            <th>Items</th>
        </tr>
    </thead>
    <tbody class="w-100 d-table">
        @foreach($order->orderItems as $orderItem)
        <tr>
            <td>
                <h6>{{ $orderItem->item->name }} {{ $orderItem->getSizeText() }} {{ format_currency($orderItem->price) }}</h6>
                <ul>
                    @forelse($orderItem->modifiers as $modifier)
                    <li>{{ $modifier->item_name }}&nbsp;({{ format_currency($modifier->item_price) }})</li>
                    @empty
                    @endforelse
                </ul>
            </td>
            <td>{{ $orderItem->quantity }} x {{ format_currency($orderItem->item_total) }}</td>
            <td>{{ format_currency($orderItem->subtotal) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>