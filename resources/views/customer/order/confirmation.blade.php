@extends('layouts.customer')
@section('title','Order Confirmation')

@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
</header>
@endsection
@section('content')
<!-- Section : Order Confirmation -->
<section class="order-confirmation">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card shadow-sm text-center rounded-0">
                    <div class="card-header">
                        <i class="fa fa-check-circle" aria-hidden="true"></i>
                        <h4 class="py-3">Thank you for your purchase!</h4>
                        <p class="lead mb-0">We have recieved your order.</p>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive-md">
                            <table class="table table-borderless confirm-table">
                                <tbody>
                                    @foreach($order->orderItems as $orderItem)
                                    <tr>
                                        <td class="d-none d-sm-block">
                                            <div class="imgLiquid">
                                                <img src="{{ asset('storage/'.$orderItem->item->image) }}" alt="Image">
                                            </div>
                                        </td>
                                        <td>
                                            <h6>{{ $orderItem->item->name }} {{ $orderItem->getSizeText() }} {{ format_currency($orderItem->price) }}</h6>
                                            <ul>
                                                @forelse($orderItem->modifiers as $modifier)
                                                <li>{{ $modifier->item_name }}&nbsp;({{ format_currency($modifier->item_price) }})</li>
                                                @empty
                                                @endforelse
                                            </ul>
                                        </td>
                                        <td>{{ $orderItem->quantity }} x {{ format_currency($orderItem->item_total) }} : {{ format_currency($orderItem->subtotal) }}</td>
                                    </tr>
                                    @endforeach
                                    <tr class="border-0">
                                        <td class="d-none d-sm-block"></td>
                                        <td></td>
                                        <td> Order ID : #{{ $order->order_reference }}</td>
                                    </tr>
                                    <tr class="border-0">
                                        <td class="d-none d-sm-block"></td>
                                        <td></td>
                                        <td> Tax ({{ $order->tax }}%)  : {{ format_currency($order->tax_amount) }}</td>
                                    </tr>
                                    <tr class="border-0">
                                        <td class="d-none d-sm-block"></td>
                                        <td></td>
                                        <td> Delivery Charge : {{ format_currency($order->delivery_charge) }}</td>
                                    </tr>
                                    @if($order->coupon_code!='')
                                    <tr class="border-0">
                                        <td class="d-none d-sm-block"></td>
                                        <td></td>
                                        <td> Coupon Discount : {{ format_currency($order->coupon_discount) }}</td>
                                    </tr>
                                    @endif
                                    <tr class="border-0">
                                        <td class="d-none d-sm-block"></td>
                                        <td></td>
                                        <td> Total : {{ format_currency($order->payed_amount) }}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <a href="{{ route('items.index') }}" class="secondary-btn">Continue shopping</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection