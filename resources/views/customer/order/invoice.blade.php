<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>AdminLTE 3 | Invoice</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<div class="wrapper">
  <section class="invoice">
    <div class="row">
      <div class="col-12">
        <div class="invoice p-3 mb-3">
          <div class="row">
            <div class="col-12">
              <h4>
                <i class="fas fa-globe"></i> Foodday.
                <small class="float-right">Date: {{ getDateString($order->created_at) }}</small>
              </h4>
            </div>
          </div>
        <div class="row invoice-info" align="left">
            <div class="col-sm-4 invoice-col">
              From
              <address>
                <strong>Admin, Foodday.</strong><br>
                795 Folsom Ave, Suite 600<br>
                San Francisco, CA 94107<br>
                Phone: {{ getStoreSettings('contact_number') }}<br>
                Email: {{ getStoreSettings('contact_email') }}
              </address>
            </div>
            <br><br>
            <div class="col-sm-4 invoice-col">
              To
              <address>
                <strong>{{ $order->customer->getName() }}</strong><br>
                {{ $order->customer->address_line_1 }}
              </address>
            </div>
            <div class="col-sm-4 invoice-col">
              <br>
              <b>Order ID:</b> #{{ $order->order_reference }}<br><br>
            </div>
          </div>

          <div class="row">
            <div class="col-12 table-responsive">
              <table class="table" cellpadding="3px" width="100%" border="1px solid" style="border-collapse: collapse;">
                <thead>
                  <tr align="left">
                    <th>Item</th>
                    <th>Quantity</th>
                    <th>Price</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($order->orderItems as $orderItem)
                  <tr>
                    <td>{{ $orderItem->item->name }}{{ $orderItem->getSizeText() }} {{ format_currency($orderItem->price) }}

                      @if($orderItem->modifiers->count() > 0)
                      <table class="table" width="100%">
                        <tbody>
                          @foreach($orderItem->modifiers as $modifierItem)

                          <tr>
                            <td><strong>.</strong>&nbsp;{{ $modifierItem->item_name }} ({{ format_currency($modifierItem->item_price) }})</td>
                          </tr>
                          @endforeach 
                        </tbody>
                      </table>
                      @endif  
                    </td>
                    <td>{{ $orderItem->quantity }}</td>
                    <td>{{ format_currency($orderItem->item_total) }}</td>
                    <td>{{ format_currency($orderItem->subtotal) }}</td>
                  </tr>
                  @endforeach
                  <tr>
                    <th colspan="3" align="right">Subtotal</th>
                    <td>{{ format_currency($order->subtotal) }}</td>
                  </tr>
                  <tr>
                    <th colspan="3" align="right">Tax ({{ $order->tax }}%)</th>
                    <td>{{ format_currency($order->tax_amount) }}</td>
                  </tr>

                  <tr>
                    <th colspan="3" align="right">Delivery Charge</th>
                    <td>{{ format_currency($order->delivery_charge) }}</td>
                  </tr>
                  @if($order->coupon_code!='')
                  <tr>
                    <th colspan="3" align="right">Coupon Discount ({{ getCouponString($order->coupon_code) }})</th>
                    <td>-{{ format_currency($order->coupon_discount) }}</td>
                  </tr>
                  @endif
                  <tr>
                    <th colspan="3" align="right">Total</th>
                    <td>{{ format_currency($order->payed_amount) }}</td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
</body>
</html>
