@extends('layouts.customer')
@section('title','Account')

@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-content text-center w-75 m-auto">
                    <h1>Account</h1>
                    <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sedolorm reminusto doeiusmod tempor incidition ulla mco laboris nisi ut aliquip ex ea commo condorico.</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<!-- Section : Account -->
<section class="account-section">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <ul class="nav nav-tabs d-block mb-4" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" data-toggle="tab" href="#edit-profile">Profile</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#order-history" id="order-history-div">Order History</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#reset-password">Reset Password</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form-sidebar').submit();">Logout</a>
                        <form id="logout-form-sidebar" action="{{ route('logout') }}" method="POST" style="display: none;">
                          {{ csrf_field() }}
                        </form>
                  </li>
              </ul>
          </div>
          <div class="col-md-8">
            <div class="tab-content" id="myTabContent">
                <!-- Edit profile -->
                <div class="tab-pane fade show active" id="edit-profile">
                    <form action="{{ route('customer.profile.update') }}" class="ajax-form" method="put">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <h5>Edit Profile</h5>
                        <input type="text" value="{{ $customer->first_name }}" placeholder="First Name" name="first_name">
                        <span id="first_name_error" class="error-label"></span>
                        <input type="text" value="{{ $customer->last_name }}" placeholder="Last Name" name="last_name">
                        <span id="last_name_error" class="error-label"></span>
                        <input type="email" value="{{ $customer->email }}" name="]email" disabled>
                        <span id="email_error" class="error-label"></span>
                        <input type="text" value="{{ $customer->zipcode }}" name="zipcode" placeholder="Zip Code">
                        <span id="zipcode_error" class="error-label"></span>
                        <input type="text" value="{{ $customer->phone }}" placeholder="Phone" name="phone">
                        <span id="phone_error" class="error-label"></span><br>
                        <button type="submit" class="secondary-btn">Save changes</button>
                    </form>
                </div>
                <!-- Order history -->
                <div class="tab-pane fade" id="order-history">
                    <div id="orders-div">
                    <h5>Order history</h5>
                    <table class="table table-borderless table-responsive-sm m-0 profile-table table-hover">
                        <thead>
                            <tr>
                                <th scope="col">#ID</th>
                                <th scope="col">Payed Amount</th>
                                <th scope="col">Status</th>
                                <th scope="col">Date</th>
                                <th scope="col">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @forelse($orders as $order)
                            <tr>
                                <td>{{ $order->order_reference }}</td>
                                <td>{{ format_currency($order->payed_amount) }}</td>
                                <td><span class="badge badge-pill badge-dark">{{ $order->getStatus() }}</span></td>
                                <td>{{ getDateString($order->created_at) }}</td>
                                <td><a href="{{ route('customer.order.show',$order) }}" class="primary-btn btn-xsml btn-view-order">View</a></td>
                            </tr>
                            @empty
                            <tr>
                                <td>No Orders Found</td>
                            </tr>
                            @endforelse
                        </tbody>
                    </table>
                    </div>
                    <div id="order-details-div" style="display: none">
                    </div>
                </div>
                <!-- Reset password -->
                <div class="tab-pane fade" id="reset-password">
                    <form action="{{ route('customer.profile.change-password') }}" class="ajax-form" method="post" id="reset-password">
                        {{ csrf_field() }}
                        <h5>Change Password</h5>
                        <input type="password" placeholder="Current Password" name="current_password">
                        <span id="current_password_error" class="error-label"></span>
                        <input type="password" placeholder="New Password" name="password">
                        <span id="password_error" class="error-label"></span>
                        <input type="password" placeholder="Confirm Password" name="password_confirmation">
                        <span id="password_confirmation_name_error" class="error-label"></span>
                        <button type="submit" class="secondary-btn">Save changes</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
</section>
@endsection