@extends('layouts.customer')
@section('title','Home')
@section('header')
<header class="header position-relative d-flex align-items-center imgLiquid">
            <img src="{{ asset('storage/'.getStoreSettings('store_image')) }}" alt="Banner Image">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-md-1">
                        <div class="header-content text-center m-auto">
                            <h1>Down Town Restaurant</h1>
                            <p class="lead">We are committed to serve high quality yummy and delicious food to make your every visit enjoyable and relaxed ambience.</p>
                            <a href="{{ route('items.index') }}" class="primary-btn">view our menu</a>
                        </div>
                    </div>
                </div>
            </div>
        </header>
@endsection
@section('content')
<!-- Section : About -->
<section class="about-section pb-0">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="lead text-md-right text-sm-left"> Hello. Our hotel has been present for over 20 years. We make the best for all our customers.</p>
            </div>
            <div class="col-md-6">
                <p>Our Resturant, a relaxed atmosphere and casual dining with indoor and outdoor seating — perfect for every meal.</p>
                <p>The restaurant is Chef Keller’s second three-Michelin-starred property featuring his daily nine-course tasting menu and a nine-course vegetable tasting menu using classic French technique and the finest quality ingredients available. Multi-cuisine restaurant offering Thai,Chinese, Indian, South-Indian, Tandoori & Traditional cuisines. It is an ideal place for having a lavish dine-in with family and friends.</p>
            </div>
        </div>
    </div>
</section>

<!-- Section : Cuisines -->

<section class="cuisines-section pb-0">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Our cuisines</h2>
        </div>
        <div class="row-5">
            @forelse($categories as $category)
            <div class="col-md-20p">
                <div class="cuisine-item imgLiquid">
                    <a href="{{ route('category.items',$category) }}">
                        <img src="{{ asset('storage/'.$category->image) }}" alt="Image">
                        <span>{{ $category->name }}</span>
                    </a>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</section>

<!-- Section : Products -->

<section class="products-section pb-0">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Featured products</h2>
        </div>
        <div class="row justify-content-center">
            @forelse($featuredItems as $item)
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="product-item">
                    <div class="product-img imgLiquid">
                        <img src="{{ asset('storage/'.$item->image) }}" alt="Image">
                    </div>
                    <h5>{{ $item->name }} {{ $item->getItemType() }}</h5>
                    <p>{{ $item->description }}</p>
                    <div class="product-price">
                        <span>{{ $item->price!=''?format_currency($item->price):'Custom' }}</span>
                        <a href="{{ route('items.show',$item) }}" data-toggle="modal" data-target="#menuItemModal2" class="secondary-btn btn-sml btn-modal">add to cart</a>
                    </div>
                </div>
            </div>
            @empty
            @endforelse
        </div>
    </div>
</section>
@include('customer.components.tablebooking')
@endsection