<form action="{{ route('customer.zone.update') }}" class="cart-form" method="post" id="cart-form">
    <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Select your delivery location </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="modal-body">
        <div class="row align-items-center">

            <div class="col-md-12">

                    <div class="adrst-type" id="google_location">
                        <input id="id_address" type="text" class="form-control" placeholder="Grand Mall, Kochi" />
                        <input type="hidden" id="js-choosen-location" name="choosen_location" value="">
                        <input type="hidden" id="js-delivery-available" name="delivery_available" value="">
                        <input type="hidden" id="js-delivery-zone" name="zone_choosen" value="">
                        <input type="hidden" id="js-longitude" name="zone_longitude" value="">
                        <input type="hidden" id="js-latitude" name="zone_latitude" value="">
                    </div>

            </div>
        </div>       
    </div>
    <div class="modal-footer">
        <button type="button" onclick="update_delivery_zone()" class="secondary-btn btn-sml btn-block">Update</button>
    </div>
</form>