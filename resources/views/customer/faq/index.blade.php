@extends('layouts.customer')
@section('title','Faq')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="header-content text-center w-75 m-auto">
                    <h1>Faq</h1>
                    <p class="lead">Frequently Asked Questions</p>
                </div>
            </div>
        </div>
    </div>
</header>
@endsection

@section('content')
<section class="faq-section">
    <div class="container">
        @include('flash::message')
        <div class="row">
            <div class="col-md-8">
                <h4>ORDERING FROM {{ strtoupper(config('app.name')) }}</h4>
                @forelse($faqs as $faq)
                <h5>{{ strtoupper($faq->title) }}</h5>
                <p>{{ $faq->description }}</p>
                @empty
                @endforelse
            </div>
            <div class="col-md-4">
                <div class="basic-form text-center p-4">
                    <h4>GET IN TOUCH</h4>
                    <p>Do you have any more questions? Please be free to reach us.</p>
                    <p>Email us at {{ getStoreSettings('contact_email') }} <br>Or call us on {{ getStoreSettings('contact_number') }}</p>
                    <form action="{{ route('customer.contact.store') }}" method="post" class="ajax-form">
                        {{ csrf_field() }}
                        <input type="text" placeholder="Name" name="name" required>
                        <span id="name_error" class="error-label"></span>
                        <input type="email" placeholder="Email" name="email" required>
                        <span id="email_error" class="error-label"></span>
                        <textarea placeholder="Message" name="message" required></textarea>
                        <span id="message_error" class="error-label"></span>
                        <button type="submit" class=" secondary-btn">Send now</button>
                    </form>
                </div>

            </div>
        </div>
    </div>
</section>
@endsection