@extends('emails.template')

@section('title')
Order Confirmed
@endsection

@section('content')
<p><strong>Your order #{{ $order->order_reference }} has been confirmed.</strong></p>
@endsection
