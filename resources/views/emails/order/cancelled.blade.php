@extends('emails.template')

@section('title')
Order Cancelled
@endsection

@section('content')
<p><strong>Sorry!, Your order #{{ $order->order_reference }} has been cancelled. </strong></p>
@endsection
