@extends('emails.template')

@section('title')
Order Placed
@endsection

@section('content')
<p><strong>Please find below, the summary of your order #{{ $order->order_reference }} on {{ getDateString($order->created_at) }}.</strong></p>
<p><strong>Address</strong></p>
<table width="100%">
	<tr>
		<td>
			<address>
				<strong>{{ $order->customer->getName() }}</strong><br>
				{{ $order->customer->company }}<br>
                {{ $order->customer->door }}<br>
				{{ $order->customer->address_line_1 }}<br>
				{{ $order->customer->address_line_2 }}<br>
				Pin : {{ $order->customer->zipcode }}<br>
				Phone : {{ $order->customer->phone }}<br>
				Email : {{ $order->customer->email }}<br>
			</address>
		</td>
		<td>
			<table cellpadding="5px">
				@if($order->is_preorder==1)
				<tr>
					<td>Preorder Date:</td><td>{{ getDateString($order->preorder_date) }}</td>
				</tr>
				<tr>
					<td>Preorder Time:</td><td>{{ getTimeString($order->preorder_date) }}</td>
				</tr>
				@endif
				<tr>
					<td>Payment Via:</td><td>{{ $order->paymentMethod() }}</td>
				</tr>
				<tr>
					<td>Payment Status:</td><td>{{ $order->payment_status==1? 'Paid':'Pending' }}</td>
				</tr>
				<tr>
					<td>Checkout Type:</td><td>{{$order->checkoutType()}}</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<p><strong>Order Details</strong></p>
<table class="table" cellpadding="2px" width="100%" border="1px">
	<thead>
		<tr align="left">
			<th>Item</th>
			<th>Quantity</th>
			<th>Price</th>
			<th>Subtotal</th>
		</tr>
	</thead>
	<tbody>
		@foreach($order->orderItems as $orderItem)
		<tr>
			<td>{{ $orderItem->item->name }}{{ $orderItem->getSizeText() }} {{ format_currency($orderItem->price) }}

				@if($orderItem->modifiers->count() > 0)
				<table class="table" width="100%">
					<tbody>
						@foreach($orderItem->modifiers as $modifierItem)

						<tr>
							<td><strong>.</strong>&nbsp;{{ $modifierItem->item_name }}</td>
							<td>({{ format_currency($modifierItem->item_price) }})</td>
						</tr>
						@endforeach 
					</tbody>
				</table>
				@endif  
			</td>
			<td>{{ $orderItem->quantity }}</td>
			<td>{{ format_currency($orderItem->item_total) }}</td>
			<td>{{ format_currency($orderItem->subtotal) }}</td>
		</tr>
		@endforeach
		<tr>
			<th colspan="3" align="right">Subtotal</th>
			<td>{{ format_currency($order->subtotal) }}</td>
		</tr>
		<tr>
			<th colspan="3" align="right">Tax ({{ $order->tax }}%)</th>
			<td>{{ format_currency($order->tax_amount) }}</td>
		</tr>

		<tr>
			<th colspan="3" align="right">Delivery Charge</th>
			<td>{{ format_currency($order->delivery_charge) }}</td>
		</tr>
		@if($order->coupon_code!='')
		<tr>
			<th colspan="3" align="right">Coupon Discount ({{ getCouponString($order->coupon_code) }})</th>
			<td>-{{ format_currency($order->coupon_discount) }}</td>
		</tr>
		@endif
		<tr>
			<th colspan="3" align="right">Total</th>
			<td>{{ format_currency($order->payed_amount) }}</td>
		</tr>
	</tbody>
</table>
@endsection
