@extends('emails.template')

@section('title')
    New Contact Received
@endsection

@section('content')
    <table border="0" cellpadding="8px">
        <tr>
            <td>Name</td>
            <td>{{ $contact['name'] }}</td>
        </tr>

        <tr>
            <td>Email</td>
            <td>{{ $contact['email'] }}</td>
        </tr>
        <tr>
            <td>Message</td>
            <td>{{ $contact['message'] }}</td>
        </tr>
    </table>
@endsection
