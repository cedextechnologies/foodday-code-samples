@extends('emails.template')

@section('title')
    Welcome
@endsection

@section('content')
	{{ 'Hi '.$user->first_name }},<br>
    {{ 'You are successfully created an account on foodday.' }}
@endsection
