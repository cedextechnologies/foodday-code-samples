@extends('emails.template')

@section('title')
Table Reservation
@endsection

@section('content')
<h2 style="color:#444343;font-family:'Roboto', Tahoma, Verdana, Segoe, sans-serif;margin-left: 3px">
    @if($reservation->status==1)
    Your table reservation is confirmed.
    @else
    Sorry! Your table reservation is cancelled.
    @endif
</h2>
<table border="0" cellpadding="8" cellspacing="0" width="100%">
    <tr>
        <td>Name</td><td>{{ $reservation['name'] }}</td>
    </tr>
    <tr>
        <td>Email </td><td> {{ $reservation['email'] }}</td>
    </tr>
    <tr>
        <td>Phone </td><td> {{ $reservation['mobile'] }}</td>
    </tr>
    <tr>
        <td>Booking Date </td><td> {{ getDateString($reservation['booking_date']) }}</td>
    </tr>
    <tr>
        <td>Booking Time </td><td> {{ getTimeString($reservation['booking_time']) }}</td>
    </tr>
    <tr>
        <td>Party Size </td><td> {{ $reservation['party_size'] }}</td>
    </tr>
    <tr>
        <td>Extra Notes </td><td>{{ $reservation['extra_notes'] }}</td>
    </tr>
</table>
@endsection
