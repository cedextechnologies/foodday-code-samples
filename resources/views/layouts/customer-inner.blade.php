<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="title" content="">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>Home | {{ config('app.name') }}</title>
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Other css -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('customer/assets/images/favicon.png') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('customer/assets/css/clockpicker.css')}}?v={{ getVersion() }}">
    <link rel="stylesheet" href="{{asset('customer/assets/css/styles.css')}}?v={{ getVersion() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>
</head>

<body>
    <div class="main-outercon overflow-hidden innerpage">

        @include('shared.components.customer.navigation')

        <!-- Header -->

        <header class="header position-relative d-flex align-items-center">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="header-content text-center w-75 m-auto">
                            <h1>Cart</h1>
                        </div>
                    </div>
                </div>
            </div>
        </header>

        <!-- Section : Cart -->

        <section class="cart-section">
            <div class="container">
                <form action="">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-borderless table-responsive-sm cart-table mb-4">
                                <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Product</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>
                                            <a href="#" class="cart-item-img imgLiquid"><img src="./assets/images/content/img1.jpg" alt="Image"></a>
                                        </td>
                                        <td class="product-name"><a href="#">Feta Salad</a></td>
                                        <td>£ 109.00</td>
                                        <td><input type="number" min="1" value="1"></td>
                                        <td>£ 109.00</td>
                                        <td>
                                            <a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" class="cart-item-img imgLiquid"><img src="./assets/images/content/img2.jpg" alt="Image"></a>
                                        </td>
                                        <td class="product-name"><a href="#">Feta Salad</a>
                                            <ul>
                                                <li>Tomato</li>
                                                <li>Ginger</li>
                                                <li>Cucumber</li>
                                            </ul>
                                        </td>
                                        <td>£ 15.00</td>
                                        <td><input type="number" min="1" value="1"></td>
                                        <td>£ 15.00</td>
                                        <td>
                                            <a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <a href="#" class="cart-item-img imgLiquid"><img src="./assets/images/content/img3.jpg" alt="Image"></a>
                                        </td>
                                        <td><a href="#">Starter Dish</a></td>
                                        <td>£ 21.00</td>
                                        <td><input type="number" min="1" value="1"></td>
                                        <td>£ 21.00</td>
                                        <td>
                                            <a href="#"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="row justify-content-start mb-4 coupen-field">
                        <div class="col-md-6 d-lg-flex">
                            <input type="text" placeholder="Coupon" required>
                            <button type="submit" class="secondary-btn btn-block">Apply Coupon</button>
                        </div>
                    </div>
                    <div class="row justify-content-end">
                        <div class="col-md-6">
                            <h4 class="pb-4">Cart totals</h4>
                            <table class="table-borderless table-responsive-sm subtotal-table w-100">
                                <tbody>
                                    <tr>
                                        <th>Sub Total</th>
                                        <td>£ 1200.00</td>
                                    </tr>
                                    <tr>
                                        <th>Tax(5.0%)</th>
                                        <td>£ 60.00</td>
                                    </tr>
                                    <tr>
                                        <th>Delivery Charge </th>
                                        <td>£ 0.00</td>
                                    </tr>
                                    <tr>
                                        <th>Order Total </th>
                                        <td>£ 1260</td>
                                    </tr>
                                </tbody>
                            </table>
                            <a href="checkout.html" class="secondary-btn btn-block">proceed to checkout</a>
                        </div>
                    </div>
                </form>
            </div>
        </section>

        <!-- Footer -->

        <footer class="footer">
            <div class="footer-top">
                <div class="container">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="footer-block">
                                <h6>About us</h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to
                                    make
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-block hours">
                                <h6>Opening hours</h6>
                                <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                                <ul>
                                    <li>Monday<span>Closed</span></li>
                                    <li>Tuesday<span>10:00AM - 11:00 PM</span></li>
                                    <li>Wednesday<span>10:00AM - 11:00 PM</span></li>
                                    <li>Thursday<span>10:00AM - 11:00 PM</span></li>
                                    <li>Friday<span>10:00AM - 11:00 PM</span></li>
                                    <li>Saturday<span>10:00AM - 11:00 PM</span></li>
                                    <li>Sunday<span>10:00AM - 11:00 PM</span></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="footer-block">
                                <h6>Contact us</h6>
                                <a href="mailto:foodcart2019@gmail.com">Email: foodcart2017@gmail.com</a><br>
                                <a href="tel:09544123123">Phone: 09544-123-123</a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="footer-bottom">
                <div class="container">
                    <p class="copyright">&copy; Copyright 2019 Foodday</p>
                </div>
            </div>
        </footer>

    </div>
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="assets/js/app.js?v={{ getVersion() }}"></script>
</body>

</html>