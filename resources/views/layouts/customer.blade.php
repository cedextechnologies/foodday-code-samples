<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="title" content="">
    <meta name="description" content="">
    <meta name="keyword" content="">
    <meta name="author" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title') | {{ config('app.name') }}</title>
    <!-- Bootstrap css -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <!-- Other css -->
    <link rel="shortcut icon" type="image/png" href="{{ asset('customer/assets/images/favicon.png') }}" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="{{asset('customer/assets/css/clockpicker.css')}}?v={{ getVersion() }}">
    <link rel="stylesheet" href="{{asset('customer/assets/css/styles.css')}}?v={{ getVersion() }}">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/css/toastr.css" rel="stylesheet"/>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.css" rel="stylesheet"/>
</head>

<body>
    <div class="@if(Request::segment(2)=='login' || Request::segment(2)=='register' | Request::segment(1)=='forgot-password' || Request::segment(3)=='confirmation')
    {{ 'main-outercon overflow-hidden innerpage page-sml' }}
    @elseif(Request::segment(1)=='')
    {{ 'main-outercon overflow-hidden' }}
    @else
    {{ 'main-outercon overflow-hidden innerpage' }}@endif">

        @include('shared.components.customer.navigation')

        @yield('header')
        
        @yield('content')

        @include('shared.components.customer.footer')

    </div>
    <div class="modal fade" id="menuItemModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="modal-content">
                
            </div>
        </div>
    </div>


    <div class="modal fade" id="zoneModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content" id="modal-content">
                @include('customer.zone.index')
            </div>
        </div>
    </div>




    @routes
    
    <!-- Scripts -->
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBLeMPncsAM5MEU92wPtziaC8tDoVG_PlQ&amp;libraries=places" type="text/javascript"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <script src="{{asset('customer/assets/js/datepicker.min.js')}}?v={{ getVersion() }}"></script>
    <script src="{{asset('customer/assets/js/clockpicker.js')}}?v={{ getVersion() }}"></script>
    <script src="{{asset('customer/assets/js/app.js')}}?v={{ getVersion() }}"></script>
    <script src="{{asset('customer/assets/js/alertify.min.js')}}?v={{ getVersion() }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/2.0.1/js/toastr.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.js"></script>

</body>

</html>