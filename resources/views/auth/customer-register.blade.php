@extends('layouts.customer')
@section('title','Signup')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
</header>
@endsection
@section('content')
<section class="login-section">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Sign up</h2>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="basic-form text-center p-4 shadow-sm">
                    <form action="{{ route('customer.register') }}" method="post" class="ajax-form" id="signup-frm" novalidate>
                        {{ csrf_field() }}
                        <input type="text" placeholder="First Name" name="first_name" class="{{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{ old('first_name') }}" required>
                        <span id="first_name_error" class="error-label"></span>

                        <input type="text" placeholder="Last Name" name="last_name" class="{{ $errors->has('last_name') ? ' is-invalid' : '' }}" value="{{ old('last_name') }}" required>
                        <span id="last_name_error" class="error-label"></span>

                        <input type="email" placeholder="Email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" required>
                        <span id="email_error" class="error-label"></span>

                        <input type="text" placeholder="Phone" name="phone" class="{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" required>
                        <span id="phone_error" class="error-label"></span>

                        <input type="password" placeholder="Password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        <span id="password_error" class="error-label"></span>

                        <input type="password" placeholder="Confirm Password" class="{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}" name="password_confirmation" required>
                        <span id="password_confirmation_error" class="error-label"></span>
                        
                        <button type="submit" class="secondary-btn btn-block">Submit</button>
                        <a href="{{ route('customer.profile.forgot-password') }}">Forgot Password?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection