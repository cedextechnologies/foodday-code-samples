@extends('layouts.customer')
@section('title','Reset Password')

@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
</header>
@endsection
@section('content')
<section class="login-section">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Reset Password</h2>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="basic-form text-center p-4 shadow-sm" id="reset-link-div">
                    <form action="{{ route('customer.password.email') }}" method="post" class="ajax-form" id="reset-pass-email-form">
                        {{ csrf_field() }}
                        <input type="email" placeholder="Email" name="email" required>
                        <span id="email_error" class="error-label"></span>
                        <button type="submit" class="secondary-btn btn-block">Submit</button>
                    </form>
                </div>

                <div class="basic-form text-center p-4 shadow-sm" id="reset-pass-div" style="display: none">
                    <form action="{{ route('customer.password.reset') }}" method="post" class="ajax-form" id="reset-pass-form">
                        {{ csrf_field() }}
                        <input type="text" placeholder="OTP" name="token" required>
                        <span id="token_error" class="error-label"></span>
                        <input type="password" placeholder="New Password" name="password" required>
                        <span id="password_error" class="error-label"></span>
                        <input type="password" placeholder="Confirm Password" name="password_confirmation" required>
                        <span id="password_confirmation_error" class="error-label"></span>
                        <button type="submit" class="secondary-btn btn-block">Submit</button>
                        <small>Haven't received it yet? <a href="javascript:void(0)" onclick="javascript:$('#reset-pass-form').submit();">Resend OTP</a></small>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection