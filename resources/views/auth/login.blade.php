@extends('layouts.customer')
@section('title','Login')
@section('header')
<header class="header position-relative d-flex align-items-center" style="background: url('{{ asset('images/banner-img1.jpg') }}');">
</header>
@endsection
@section('content')
<section class="login-section">
    <div class="container">
        <div class="common-title text-center w-50 m-auto">
            <h2>Login</h2>
        </div>
        <div class="row">
            <div class="col-md-6 offset-md-3">
                <div class="basic-form text-center p-4 shadow-sm">
                    <form action="{{ route('customer.login') }}" method="post" novalidate="">
                        {{ csrf_field() }}
                        <input type="email" placeholder="Email" name="email" class="{{ $errors->has('email') ? ' is-invalid' : '' }}" required>
                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                        <input type="password" placeholder="Password" class="{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                        @if ($errors->has('password'))
                        <span class="invalid-feedback" role="alert" style="display: block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                        <button type="submit" class="secondary-btn btn-block">Submit</button>
                        <a href="{{ route('customer.profile.forgot-password') }}">Forgot Password?</a>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection