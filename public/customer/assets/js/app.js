$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
// Ajax wrapper
window.makeApiCall = function(type, url, data, callbackSuccess) {
  $.ajax({
    url: url,
    type: type,
    data: data,
    dataType: "json",
    success: function(response) {
      callbackSuccess(response);
    }
  });
}

// Ajax wrapper
window.makeJsonApiCall = function(type, url, data, callbackSuccess) {
  $.ajax({
    url: url,
    type: type,
    data: data,
    dataType: "json",
    beforeSend: function(x) {
      if (x && x.overrideMimeType) {
        x.overrideMimeType("application/j-son;charset=UTF-8");
      }
    },
    success: function(response) {
      callbackSuccess(response);
    }
  });
}

$(function() {

  $('#menuItemModal2').on('hidden.bs.modal', function (e) {
    $('.btn-modal').trigger('blur');
  })

  function toaster(message,type)
  {
    toastr.options.timeOut = 1500; // 1.5s
    toastr.options.positionClass = 'toast-bottom-right'; // 1.5s
    switch(type){
      case 'success': toastr.success(message, 'Success');
      break;
      case 'info': toastr.info(message , 'Info');
      break;
      case 'warning' : toastr.warning(message, 'Warning');
    }
  }

  $(".ajax-form").submit(function(e){
    e.preventDefault();
    url=$(this).attr('action');
    method=$(this).attr('method');
    form_id=$(this).attr('id');
    makeApiCall(method, url, $(this).serialize(), function(response) {
      $(".error-label").text('');
      if(response.status == 'error') {
        $.each(response.data, function (key, val) {
          $("#" + key + "_error").text(val[0]);
        });
        toaster(response.data.message,'warning');
      }
      if(response.status=='success'){
        if($(".ajax-form").attr('id')=='checkout-frm'){
          if($("input[name=payment_mode]:checked").val()==1){
            window.location.href=response.data.redirect_url;
          }else if($("input[name=payment_mode]:checked").val()==2)
          {
            $("#stripe-form").attr('action', route('cart.stripe.pay',response.data.payment_token));
            $(".stripe-button-el").click();
            
          }else if($("input[name=payment_mode]:checked").val()==3)
          {
            var url=route('cart.paypal.get-url',response.data.payment_token);
            
            makeApiCall('POST', url, '', function(response) {
              if(response.status == 'error') {
                $.each(response.data, function (key, val) {
                  $("#" + key + "_error").text(val[0]);
                });
                toaster(response.data.message,'warning');
              }
              if(response.status=='success'){
                window.location.href=response.data.redirect_url;
              }
            });
          }
        }
        if($(".ajax-form").attr('id')=='signup-frm'){
          window.location.href=route('customer.login');
        }
        toaster(response.data.message,'success');
        if(method=='post' && form_id!='reset-pass-email-form' && form_id!='reset-pass-form' && form_id!='checkout-frm' && form_id!='reset-password'){
          $(".ajax-form").trigger('reset');
        }
      }
    });
  });

  $("#reset-pass-email-form").submit(function(e){
   $("#reset-link-div").hide();
   $("#reset-pass-div").show();
 });

  $(".btn-modal").click(function(e){
    e.preventDefault();
    url=$(this).attr('href');
    makeApiCall('GET', url, '', function(response) {
      if(response.status=='success'){
        $("#modal-content").html(response.html);
      }
    });
  });

  $(".is_preorder").click(function(){
    if($(this).val()==1){
      $(".pre-order-div").show();
    }else{
      $(".pre-order-div").hide();
    }
  });

  $(".btn-view-order").click(function(e){
    e.preventDefault();
    url=$(this).attr('href');
    makeApiCall('GET', url, '', function(response) {
      if(response.status=='success'){
        $("#order-details-div").html(response.data.html);
        $("#order-details-div").show();
        $("#orders-div").hide();
      }
    });
  });

  $("#order-history-div").click(function(e){
    e.preventDefault();
    $("#orders-div").show();
    $("#order-details-div").hide();
  });

  $(".item-quantity").change(function(e){
    e.preventDefault();
    url=$(this).attr('url');
    makeApiCall('POST', url, {row_id:$(this).attr('row'),quantity:$(this).val()}, function(response) {
      if(response.status=='success'){
        window.location.reload();
        //toaster(response.data.message,'success');
      }
    });
  })

  $("#apply-coupon").click(function(e){
    e.preventDefault();
    url=$(this).attr('url');
    makeApiCall('POST', url, {code:$("#coupon-code").val()}, function(response) {
      if(response.status == 'error') {
        toaster(response.data.message,'warning');
      }
      if(response.status=='success'){
        toaster(response.data.message,'success');
        window.location.reload();
      }
    });
  });

  $("#remove-coupon").click(function(e){
    e.preventDefault();
    url=$(this).attr('url');
    makeApiCall('POST', url, {code:$("#coupon-code").val()}, function(response) {
      if(response.status == 'error') {
        toaster(response.data.message,'warning');
      }
      if(response.status=='success'){
        toaster(response.data.message,'success');
        window.location.reload();
      }
    });
  });

  $("#zipcode").change(function(){
    makeApiCall('POST', route('cart.set-location'), {zipcode:$(this).val()}, function(response) {
      if(response.status == 'error') {
        toaster(response.data.message,'Invalid Zipcode');
      }
      if(response.status=='success'){
        $("#latitude").val(response.data.latitude);
        $("#longitude").val(response.data.longitude);
        toaster(response.data.message,'success');
      }
    });
  });

  $(".delete-cart-item").submit(function(e){
    e.preventDefault();
    if(confirm('Do you want to delete?')){
      url=$(this).attr('action');
      makeApiCall('POST', url, $(this).serialize(), function(response) {
        if(response.status == 'error') {
          toaster(response.data.message,'warning');
        }
        if(response.status=='success'){
          toaster(response.data.message,'success');
          window.location.reload();
        }
      });
    }
  });

});

function cartToaster(message,type)
{
    toastr.options.timeOut = 1500; // 1.5s
    toastr.options.positionClass = 'toast-bottom-right';
    switch(type){
      case 'success': toastr.success(message, 'Success');
      break;
      case 'info': toastr.info(message , 'Info');
      break;
      case 'warning' : toastr.warning(message, 'Warning');
    }
  }

  function add_to_cart() {
    url=$(".cart-form").attr('action');
    method=$(".cart-form").attr('method');
    var modifiers = [];
    $.each($("input[name='modifier_id[]']"), function(){
      var modifier_id = $(this).val();
      var modifier_items = [];
      $.each($("input[name='modifier_item_id["+modifier_id+"][]']:checked"), function(){
        modifier_items.push($(this).val());
      });
      if(modifier_items.length>0){
        modifiers.push({modifier_items:modifier_items,id:modifier_id});
      }
    });

    var data = {
      "items":[
      {
        "id": $("#item_id").val(),
        "size": $(".size_id:checked").val(),
        "quantity":$("#quantity").val(),
        "instruction":$("#instruction").val(),
        "modifiers": (modifiers.length > 0) ? modifiers: [],
      } 
      ]
    };

    makeJsonApiCall(method, url, data, function(response) {
      $(".error-label").text('');
      if(response.status == 'error') {
        $.each(response.data, function (key, val) {
          $("#" + key + "_error").text(val[0]);
        });
        cartToaster(response.data.message,'warning');
      }
      if(response.status=='success'){
        $('#menuItemModal2').modal('hide');
        cartToaster(response.data.message,'success');
        $(".cart-number").html(parseInt($(".cart-number").text())+1);
        if(method=='post'){
          $(".cart-form").trigger('reset');
        }
      }
    });
  }

  $('div.alert').not('.alert-important').delay(3000).fadeOut(350);

// sticky navbar
jQuery(window).scroll(function() {
  if (jQuery(document).scrollTop() > 1) {
    jQuery('.main-outercon').addClass('sticky');
  } else {
    jQuery('.main-outercon').removeClass('sticky');
  }
});

// Background image
jQuery('.imgLiquid').each(function() {
  jQuery(this).find('img').hide();
  var imgURL = jQuery(this).find('img').attr('src');
  jQuery(this).css('background-image', 'url(' + imgURL + ')');
});

//Clock picker
jQuery('.clockpicker').clockpicker({});

$('.datepicker').datepicker({
  format: 'yyyy-mm-dd',
  autoclose:'true',
});

//set featured item menu height
var menuHeight = $(".product-item").map(function ()
{
  return $(this).height();
}).get();
maxMenuHeight = Math.max.apply(null, menuHeight);
$(".product-item").attr('style','min-height: '+maxMenuHeight+'px');



    var geocoder;
    var map;

    function initialize() {
        var input = document.getElementById('id_address');
        var options = {
            types: ['geocode'],
        };

        autocomplete = new google.maps.places.Autocomplete(input, options);
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            var location_latitude  = Math.round(place.geometry.location.lat() * 10000) / 10000;
            var location_longitude = Math.round(place.geometry.location.lng() * 10000) / 10000;

            var formatted_address = place.formatted_address;
            document.getElementById('js-choosen-location').value = formatted_address;
            document.getElementById('js-longitude').value = location_longitude;
            document.getElementById('js-latitude').value = location_latitude;
            
            if (place.address_components[0].types[0] == "postal_code") {
                var zipcode = place.address_components[0].long_name.replace(/\s/g,'');
                var url = APP.base_url + 'location/get-city-id/' + zipcode;
                makeApiCall("POST", url, {}, function(response) {
                    document.getElementById('js-delivery-zone').value = response.data.zone;
                    document.getElementById('js-delivery-available').value = response.data.available;
                });
                $('.btn-location-submit').prop("disabled", false);
            }
            if(place.address_components[0].types[0] != "postal_code") {
                var url = route('customer.zones.get-by-geo',[location_longitude,location_latitude]);
                var city_name = place.address_components[0].long_name;
                makeApiCall("POST", url, {}, function(response) {

                    alertCustomerDelivery(response.data.available);

                    document.getElementById('js-delivery-zone').value = response.data.zone;
                    document.getElementById('js-delivery-available').value = response.data.available;
                });
                $('.btn-location-submit').prop("disabled", false);
            }
        });
    }

    if ($('#google_location').length > 0) {    
        google.maps.event.addDomListener(window, "load", initialize);
    }


    function alertCustomerDelivery(available)
    {

      if(available != '1')
      {
        swal({
          title: "Delivery not available to this location",
          text: "You can choose pick up option",
          icon: "warning",
        });

      } 
    }


    function update_delivery_zone()
    {
      var location = $('#js-choosen-location').val();
      var zone = $('#js-delivery-zone').val();
      var available = $('#js-delivery-available').val();
      var longitude = $('#js-longitude').val();
      var latitude = $('#js-latitude').val();

      var inputData = {
        location:location,
        zone:zone,
        available:available,
        longitude:longitude,
        latitude:latitude
      };


      var url = route('customer.zone.update');
      makeApiCall('POST', url, inputData, function(response) {
        if(response.status == 'error') {          
          $('#zoneModal').modal('toggle');
        }
        if(response.status=='success'){
          $('#zoneModal').modal('toggle');
        }
        window.location.reload();
      });


    }