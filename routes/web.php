<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('customer.home');

Route::get('/admin/login', 'Auth\LoginController@showLoginForm')->name('admin.login');

Route::post('/admin/login', 'Auth\LoginController@login')->name('admin.login');

Route::get('/customer/login', 'Customer\LoginController@showLoginForm')->name('customer.login');

Route::post('/customer/login', 'Customer\LoginController@login')->name('customer.login');

Route::get('/customer/register', 'Auth\RegisterController@showCustomerRegisterForm')->name('customer.register');

Route::post('/customer/register', 'Auth\RegisterController@createCustomer')->name('customer.register');

Route::get('/home', 'HomeController@index')->name('home');

// Admin
Route::group([
    'middleware' => ['auth:admin', 'web'],
    'namespace' => 'Admin',
    'prefix' => 'admin',
], function () {
    //Route::get('/home', 'HomeController@index')->name('admin.home');
    Route::get('/dashboard', 'DashboardController@index')->name('admin.dashboard');
    Route::get('/dashboard/sales-graph-data', 'DashboardController@graphData')->name('admin.sales-graph-data');

    Route::get('/profile', 'ProfileController@index')->name('admin.profile');
    Route::put('/update-profile', 'ProfileController@update')->name('admin.update_profile');

    Route::get('/change-password', 'ProfileController@changePassword')->name('admin.change_password');
    Route::put('/update-password', 'ProfileController@updatePassword')->name('admin.update_password');

    Route::get('/customers', 'CustomerController@index')->name('admin.customers');
    Route::get('/customers/{customer}', 'CustomerController@edit')->name('admin.customers.edit');
    Route::put('/customers/{customer}', 'CustomerController@update')->name('admin.customers.update');
    Route::put('/customers/{customer}/update-status', 'CustomerController@updateStatus')->name('admin.customers.updateStatus');
    Route::post('/customers/export', 'CustomerController@export')->name('admin.customers.export');

    Route::put('item-categories/{itemCategory}/update-status', 'ItemCategoryController@updateStatus')->name('admin.item-categories.updateStatus');
    Route::resource('item-categories', 'ItemCategoryController', ['as' => 'admin']);

    Route::put('/users/{user}/update-status', 'UserController@updateStatus')->name('admin.users.updateStatus');
    Route::resource('users', 'UserController', ['as' => 'admin']);

    Route::put('items/{item}/update-status', 'ItemController@updateStatus')->name('admin.items.updateStatus');
    Route::resource('items', 'ItemController', ['as' => 'admin']);

    Route::get('item/{item}/modifiers/create', 'ModifierController@create')->name('admin.modifiers.create');
    Route::post('item/{item}/modifiers/store', 'ModifierController@store')->name('admin.modifiers.store');
    Route::delete('item/{item}/modifiers/{modifier}/destroy', 'ModifierController@destroy')->name('admin.modifiers.destroy');
    Route::get('item/{item}/modifiers/{modifier}/edit', 'ModifierController@edit')->name('admin.modifiers.edit');
    Route::put('item/{item}/modifiers/{modifier}/update', 'ModifierController@update')->name('admin.modifiers.update');


    Route::get('item/{item}/common-modifiers/create', 'CommonModifierController@createToItem')->name('admin.common-modifiers-to-item.create');
    Route::post('item/{item}/common-modifiers/store', 'CommonModifierController@storeToItem')->name('admin.common-modifiers-to-item.store');
    Route::delete('item/{item}/common-modifiers/{itemModifier}/destroy', 'CommonModifierController@destroyToItem')->name('admin.common-modifiers-to-item.destroy');
    Route::get('item/{item}/common-modifiers/{itemModifier}/edit', 'CommonModifierController@editToItem')->name('admin.common-modifiers-to-item.edit');
    Route::put('item/{item}/common-modifiers/{itemModifier}/update', 'CommonModifierController@updateToItem')->name('admin.common-modifiers-to-item.update');


    Route::get('item/{item}/item-size/create', 'ItemSizeController@create')->name('admin.item-size.create');
    Route::post('item/{item}/item-size/store', 'ItemSizeController@store')->name('admin.item-size.store');
    Route::delete('item/{item}/item-size/{size}/destroy', 'ItemSizeController@destroy')->name('admin.item-size.destroy');
    Route::get('item/{item}/item-size/{size}/edit', 'ItemSizeController@edit')->name('admin.item-size.edit');
    Route::put('item/{item}/item-size/{size}/update', 'ItemSizeController@update')->name('admin.item-size.update');

    Route::put('coupons/{coupon}/update-status', 'CouponController@updateStatus')->name('admin.coupons.updateStatus');
    Route::resource('coupons', 'CouponController', ['as' => 'admin']);

    Route::put('cities/{city}/update-status', 'CityController@updateStatus')->name('admin.cities.updateStatus');
    Route::resource('cities', 'CityController', ['as' => 'admin']);

    Route::get('/store-settings', 'StoreSettingsController@index')->name('admin.store-settings');
    Route::put('/store-settings/basic-info', 'StoreSettingsController@updateBasicInfo')->name('admin.store-settings.update-basic-info');
    Route::put('/store-settings/contact-details', 'StoreSettingsController@updateContactDetails')->name('admin.store-settings.update-contact-details');
    Route::put('/store-settings/other-details', 'StoreSettingsController@updateOtherDetails')->name('admin.store-settings.update-other-details');
    Route::put('/store-settings/other-settings', 'StoreSettingsController@updateOtherSettings')->name('admin.store-settings.update-other-settings');

    Route::get('/order-settings', 'OrderSettingsController@index')->name('admin.order-settings');
    Route::put('/order-settings', 'OrderSettingsController@update')->name('admin.order-settings.update');

    Route::get('/working-hours', 'workingHoursController@index')->name('admin.working-hours');
    Route::put('/working-hours', 'workingHoursController@update')->name('admin.working-hours.update');

    Route::get('/social-media', 'SocialMediaController@index')->name('admin.social-medias');
    Route::put('/social-media', 'SocialMediaController@update')->name('admin.social-medias.update');

    Route::get('/email-settings', 'EmailSettingsController@index')->name('admin.email-settings');
    Route::put('/email-settings', 'EmailSettingsController@update')->name('admin.email-settings.update');

    Route::get('/sms-settings', 'SmsSettingsController@index')->name('admin.sms-settings');
    Route::put('/sms-settings', 'SmsSettingsController@update')->name('admin.sms-settings.update');

    Route::get('/payment-settings', 'PaymentSettingsController@index')->name('admin.payment-settings');
    Route::put('/payment-settings/update-payment-settings', 'PaymentSettingsController@updatePaymentSettings')->name('admin.payment-settings.update-payment-settings');
    Route::put('/payment-settings/update-stripe-settings', 'PaymentSettingsController@updateStripeSettings')->name('admin.payment-settings.update-stripe-settings');
    Route::put('/payment-settings/update-braintree-settings', 'PaymentSettingsController@updateBraintreeSettings')->name('admin.payment-settings.update-braintree-settings');
    Route::put('/payment-settings/update-paypal-settings', 'PaymentSettingsController@updatePaypalSettings')->name('admin.payment-settings.update-paypal-settings');

    Route::put('faqs/{faq}/update-status', 'FaqController@updateStatus')->name('admin.faqs.updateStatus');
    Route::resource('faqs', 'FaqController', ['as' => 'admin']);

    Route::put('reviews/{review}/update-status', 'ReviewController@updateStatus')->name('admin.reviews.updateStatus');
    Route::get('/reviews', 'ReviewController@index')->name('admin.reviews');
    Route::get('/reviews/{review}', 'ReviewController@edit')->name('admin.reviews.edit');
    Route::put('/reviews/{review}', 'ReviewController@update')->name('admin.reviews.update');
    Route::delete('/reviews/{review}', 'ReviewController@destroy')->name('admin.reviews.destroy');

    Route::put('reservations/{reservation}/update-status', 'ReservationController@updateStatus')->name('admin.reservations.updateStatus');
    Route::get('/reservations', 'ReservationController@index')->name('admin.reservations');
    Route::get('/reservations/{reservation}', 'ReservationController@edit')->name('admin.reservations.edit');
    Route::put('/reservations/{reservation}', 'ReservationController@update')->name('admin.reservations.update');
    Route::delete('/reservation/{reservation}', 'ReservationController@destroy')->name('admin.reservations.destroy');

    Route::resource('zones', 'DeliveryZoneController', ['as' => 'admin']);

    Route::get('zones/{zone}/custome-delivey-fee', 'DeliveryZoneController@getCustomeDeliveryFee')->name('admin.zones.custome-delivey-fee');
    Route::post('zones/{zone}/custome-delivey-fee/create', 'DeliveryZoneController@storeCustomeDeliveryFee')->name('admin.zones.custome-delivey-fee.create');

    Route::put('orders/{order}/update-status', 'OrderController@updateStatus')->name('admin.orders.updateStatus');
    Route::get('/orders', 'OrderController@index')->name('admin.orders');
    Route::get('/order/{order}', 'OrderController@edit')->name('admin.orders.edit');
    Route::put('/order/{order}', 'OrderController@update')->name('admin.orders.update');
    Route::delete('/order/{order}', 'OrderController@destroy')->name('admin.orders.destroy');
    Route::get('/orders/{order}/invoice', 'OrderController@getReceipt')->name('admin.orders.invoice');

    Route::resource('common-modifiers', 'CommonModifierController', ['as' => 'admin']);
});

// Customer
Route::group([
    'namespace' => 'Customer',
], function () {
    Route::get('items', 'ItemController@index')->name('items.index');

    Route::get('items/{item}', 'ItemController@show')->name('items.show');

    Route::get('cart/items', 'CartController@index')->name('customer.cart.index');

    Route::post('cart/items/{item}/update', 'CartController@update')->name('customer.cart.update');

    Route::post('cart/{cart}/item/delete', 'CartController@delete')->name('customer.cart-item.delete');

    Route::post('cart/{token}/update-quantity', 'CartController@updateQuantity')->name('customer.cart.item.update-quantity');

    Route::post('cart/{token}/apply-coupon', 'CartController@applyCoupon')->name('cart.apply-coupon');
    Route::post('cart/{token}/remove-coupon', 'CartController@removeCoupon')->name('cart.remove-coupon');

    Route::get('order/{order}/show', 'OrderController@show')->name('customer.order.show');

    Route::get('cart/proceed-checkout', 'CartController@proceed')->name('customer.cart.proceed');

    Route::post('cart/checkout', 'CartController@checkout')->name('customer.cart.checkout');

    Route::post('cart/set-location', 'CartController@setLocation')->name('cart.set-location');

    Route::post('cart/stripe/{token}/pay', 'PaymentController@stripePayment')->name('cart.stripe.pay');

    Route::post('cart/paypal/{token}/pay', 'PaymentController@paypalPayment')->name('cart.paypal.get-url');

    Route::get('cart/paypal/{token}/success', 'PaymentController@makePaypalPayment')->name('cart.paypal.success');

    Route::get('cart/paypal/{token}/cancel', 'PaymentController@cancelPaypalPayment')->name('cart.paypal.cancel');

    Route::get('order/{order}/confirmation', 'OrderController@confirmation')->name('customer.order.confirmation');

    Route::get('categories/{category}/items', 'ItemController@items')->name('category.items');

    Route::get('reservation/create', 'ReservationController@create')->name('customer.reservation.create');

    Route::post('reserve-table', 'ReservationController@reserveTable')->name('customer.reserve-table');

    Route::get('faq', 'FaqController@index')->name('customer.faq.index');

    Route::post('contact/store', 'ContactController@contact')->name('customer.contact.store');
    Route::get('contact', 'ContactController@index')->name('customer.contact.index');

    Route::get('account', 'AccountController@index')->name('customer.account.index');

    Route::put('account', 'AccountController@update')->name('customer.profile.update');

    Route::post('account', 'AccountController@changePassword')->name('customer.profile.change-password');
    Route::get('forgot-password', 'ForgotPasswordController@forgot')->name('customer.profile.forgot-password');

    Route::post('send-reset-link', 'ForgotPasswordController@sendOtp')->name('customer.password.email');
    Route::post('reset-password', 'ForgotPasswordController@reset')->name('customer.password.reset');

    Route::post('zone/get-by-geo/{longitude}/{latitude}', 'DeliveryZoneController@getByGeo')->name('customer.zones.get-by-geo');
    Route::post('zone', 'DeliveryZoneController@update')->name('customer.zone.update');



});
