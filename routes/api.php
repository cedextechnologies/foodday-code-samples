<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::group([
    'middleware' => ['throttle', 'cors'],
    'namespace' => 'Api\v1',
    'prefix' => 'v1',
], function ($router) {
    Route::post('login', 'AuthController@login');
    Route::post('logout', 'AuthController@logout');
    Route::post('signup', 'CustomerController@store');
    Route::get('profile', 'CustomerController@profile');
    Route::post('update-profile', 'CustomerController@update');
    Route::post('change-password', 'CustomerController@changePassword');

    Route::post('forgot-password', 'CustomerController@forgot');
    Route::post('reset-password', 'CustomerController@reset');

    Route::get('city', 'CityController@index');
    Route::get('categories', 'ItemCategoryController@index');
    Route::get('categories/{category}/items', 'ItemCategoryController@show');
    Route::get('items', 'ItemController@index');
    Route::get('items/featured', 'ItemController@featured');
    Route::get('items/{item}', 'ItemController@show');

    Route::post('is-delivery-available-area', 'DeliveryZoneController@isDeliveryAvailable');

    Route::get('generatetoken', 'CartController@getCartToken');
    Route::post('cart/add/{token}', 'CartController@store');
    Route::post('cart/{token}/quantity', 'CartController@updateQuantity');
    Route::get('cart/{token}', 'CartController@show');
    Route::post('cart/destroy/{token}', 'CartController@destroy');

    Route::get('coupons', 'CartController@showCoupons');

    Route::get('store-settings', 'StoreSettingsController@getStoreSettings');

    Route::get('working-hours', 'StoreSettingsController@workingHours');

    Route::post('cart/{token}/apply-coupon', 'CartController@applyCoupon');
    Route::post('cart/{token}/remove-coupon', 'CartController@removeCoupon');
    Route::post('cart/{token}/checkout', 'CartController@checkout');

    //payments

    Route::post('stripe-make-payment/{token}', 'PaymentController@stripePayment');

    Route::post('paypal-make-payment/{token}', 'PaymentController@paypalPayment');

    Route::get('paypal-callback-success/{token}', 'PaymentController@makePaypalPayment');

    Route::get('paypal-callback-cancel/{token}', 'PaymentController@cancelPaypalPayment');

    Route::post('reservation', 'ReservationController@store');

    Route::post('subscribe', 'CustomerController@subscribe');

    Route::get('orders', 'OrderController@index');

    Route::get('orders/{order}', 'OrderController@show');

    Route::get('order-confirmation/{order}', 'OrderController@orderConfirmation');

    Route::get('faqs', 'FaqController@index');

    Route::get('faqs', 'FaqController@index');

    Route::post('contact-us', 'ContactController@contact');
});
