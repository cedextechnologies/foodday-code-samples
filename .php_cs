<?php

// See for more options https://github.com/GrahamCampbell/PHP-CS-Fixer

$finder = PhpCsFixer\Finder::create()
    ->notPath('bootstrap/cache')
    ->notPath('storage')
    ->notPath('vendor')
    ->in(__DIR__)
    ->name('*.php')
    ->notName('*.blade.php')
    ->ignoreDotFiles(true)
    ->ignoreVCS(true)
;

return PhpCsFixer\Config::create()
    ->setRules(array(
        '@Symfony' => true,
        'binary_operator_spaces' => ['align_double_arrow' => false],
        'array_syntax' => ['syntax' => 'short'],
        'linebreak_after_opening_tag' => true,
        'not_operator_with_successor_space' => true,
        'ordered_imports' => ['sortAlgorithm' => 'length'],
        'phpdoc_order' => true,
        'blank_line_after_opening_tag' => true,
        'blank_line_before_return' => true,
        'no_unused_imports' => true,
        'whitespace_after_comma_in_array' => true,
        'ternary_to_null_coalescing' => true,
        'no_whitespace_in_blank_line' => true
    ))
    ->setFinder($finder)
;