let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

 mix.js([

 	'resources/assets/js/app.js',
 	'resources/assets/admin/app.js',
 	'resources/assets/admin/plugins/jquery/jquery.min.js',
 	'resources/assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js',
 	'resources/assets/admin/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js',
 	'resources/assets/admin/dist/js/adminlte.js',
 	'resources/assets/admin/dist/js/demo.js',

 	'resources/assets/admin/plugins/raphael/raphael.min.js',
 	'resources/assets/admin/plugins/chart.js/Chart.min.js',
 	'resources/assets/admin/dist/js/pages/dashboard2.js',
 	'resources/assets/admin/plugins/datatables/jquery.dataTables.js',
 	'resources/assets/admin/plugins/datatables/dataTables.bootstrap4.js',
 	'resources/assets/admin/plugins/sweetalert2/sweetalert2.cdn.js',
 	'resources/assets/admin/plugins/toastr/toastr.min.js',
 	'resources/assets/admin/plugins/select2/js/select2.full.min.js',
 	'resources/assets/admin/plugins/bootstrap-timepicker/bootstrap-datetimepicker.js',

 	], 'public/admin/app.js')

 .styles([
 	'resources/assets/admin/plugins/overlayScrollbars/css/OverlayScrollbars.min.css',
 	'resources/assets/admin/dist/css/adminlte.min.css',
 	'resources/assets/admin/plugins/icheck-bootstrap/icheck-bootstrap.min.css',
 	'resources/assets/admin/plugins/datatables/dataTables.bootstrap4.css',
 	'resources/assets/admin/font.css',
 	'resources/assets/admin/app.css',
 	'resources/assets/admin/plugins/sweetalert2/sweetalert2.min.css',
 	'resources/assets/admin/plugins/select2/css/select2.min.css',
 	'resources/assets/admin/plugins/bootstrap-timepicker/bootstrap-datetimepicker.css',
 	
 	], 'public/admin/app.css');