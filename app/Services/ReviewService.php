<?php

namespace App\Services;

use App\Models\Review;
use Illuminate\Http\Request;

class ReviewService
{
    public function filter(Request $request)
    {
        $users = Review::when($request->input('search'),
            function ($query) use ($request) {
                $query->where('comment', 'like', '%'.$request->search.'%');
            })->orderBy('created_at', 'desc');

        return $users;
    }

    public function update(Request $request, Review $review)
    {
        $review->fill($request->all())->save();
        flash('Review updated successfully.', 'success');

        return redirect(route('admin.reviews'));
    }
}
