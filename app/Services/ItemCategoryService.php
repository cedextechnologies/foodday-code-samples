<?php

namespace App\Services;

use App\Models\ItemCategory;
use Illuminate\Http\Request;

class ItemCategoryService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request)
    {
        $category = new ItemCategory();

        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->status = $request->input('status');
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('public/item-categories');
            $category->image = substr($path, 7);
        }
        $category->save();

        flash('Well Done! Item category added successfully', 'success');

        return redirect()->route('admin.item-categories.index');
    }

    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, ItemCategory $category)
    {
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $category->status = $request->input('status');
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('public/item-categories');
            $category->image = substr($path, 7);
        }
        $category->update();

        flash('Well Done! Item category updated successfully', 'success');

        return redirect()->route('admin.item-categories.index');
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }
}
