<?php

namespace App\Services;

use App\Models\City;
use Illuminate\Http\Request;

class CityService
{
    public function filter(Request $request)
    {
        $cities = City::when($request->input('search'),
            function ($query) use ($request) {
                $query->whereRaw('(`name` like "%'.$request->input('search').'%")');
            })->orderBy('created_at', 'desc');

        return $cities;
    }

    public function create(Request $request)
    {
        $city = new City();

        $city->fill($request->all());

        $city->save();

        flash('City created successfully.', 'success');

        return redirect()->route('admin.cities.index');
    }

    public function update(Request $request, $city)
    {
        $city->fill($request->all());

        $city->save();

        flash('City updated successfully.', 'success');

        return redirect(route('admin.cities.index'));
    }
}
