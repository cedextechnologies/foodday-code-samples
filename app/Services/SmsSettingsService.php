<?php

namespace App\Services;

use Illuminate\Http\Request;

class SmsSettingsService
{
    public function update(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        $validatedData = [];

        if ($request->twilio_from_email) {
            $validatedData['twilio_from_email'] = $request->twilio_from_email;
        }

        if ($request->twilio_from_name) {
            $validatedData['twilio_from_name'] = $request->twilio_from_name;
        }

        if ($request->twilio_smtp_host) {
            $validatedData['twilio_smtp_host'] = $request->twilio_smtp_host;
        }

        if ($request->order_placed_admin_sms) {
            $validatedData['order_placed_admin_sms'] = $request->order_placed_admin_sms;
        }

        if ($request->order_placed_customer_sms) {
            $validatedData['order_placed_customer_sms'] = $request->order_placed_customer_sms;
        }

        if ($request->order_confirmed_customer_sms) {
            $validatedData['order_confirmed_customer_sms'] = $request->order_confirmed_customer_sms;
        }

        if ($request->order_cancelled_customer_sms) {
            $validatedData['order_cancelled_customer_sms'] = $request->order_cancelled_customer_sms;
        }

        if ($request->order_delivered_customer_sms) {
            $validatedData['order_delivered_customer_sms'] = $request->order_delivered_customer_sms;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('SMS settings updated successfully', 'success');

        return redirect()->route('admin.sms-settings');
    }
}
