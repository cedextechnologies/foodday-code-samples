<?php

namespace App\Services;

use Validator;
use App\Models\StoreHour;
use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Mail\TableReservation;
use App\Mail\TableReservationStatus;
use Illuminate\Support\Facades\Mail;

class ReservationService
{
    public function filter(Request $request)
    {
        $users = Reservation::when($request->input('search'),
            function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere(' email', 'like', '%'.$request->search.'%')
                ->orWhere(' mobile', 'like', '%'.$request->search.'%');
            })->orderBy('created_at', 'desc');

        return $users;
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'mobile' => 'required|string|max:15',
            'booking_date' => 'required|date_format:Y-m-d|after:yesterday',
            'booking_time' => 'required|date_format:H:i',
            'party_size' => 'required|numeric',
            'extra_notes' => 'string|nullable|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        $reservations = Reservation::where('email', request('email'))
        ->where('booking_date', request('booking_date'))
        ->where('booking_time', request('booking_time'))
        ->get();

        if ($reservations->isNotEmpty()) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Already booked.',
                    'status' => 0,
                ],
            ]);
        }

        $day = \Carbon\Carbon::parse(request('booking_date'))->format('N');
        $time = \Carbon\Carbon::parse(request('booking_time'))->format('H:i');

        if (0 == $this->isOpen($day, $time)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'status' => 0,
                    'message' => 'Restaurant not opened at this time',
                ],
            ]);
        }

        Reservation::create([
            'name' => request('name'),
            'email' => request('email'),
            'mobile' => request('mobile'),
            'booking_date' => request('booking_date'),
            'booking_time' => request('booking_time'),
            'party_size' => request('party_size'),
            'extra_notes' => request('extra_notes'),
            'status' => '0',
        ]);

        Mail::to(env('CONTACT_TO_MAIL'))->send(new TableReservation($request->all()));

        return response()->json([
            'status' => 'success',
            'data' => [
                'status' => 1,
                'message' => 'Reservation created successfully.',
            ],
        ]);
    }

    public function update(Request $request, Reservation $reservation)
    {
        $validatedData = $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|email',
            'mobile' => 'required|string|max:15',
            'booking_time' => 'required',
            'party_size' => 'required|numeric',
            'extra_notes' => 'string|max:255',
            'status' => 'numeric|in:1,0,2',
        ]);

        $reservation->update([
            'name' => request('name'),
            'email' => request('email'),
            'mobile' => request('mobile'),
            'booking_date' => \Carbon\Carbon::parse(request('booking_time'))->format('Y-m-d'),
            'booking_time' => \Carbon\Carbon::parse(request('booking_time'))->format('H:i'),
            'party_size' => request('party_size'),
            'extra_notes' => request('extra_notes'),
            'status' => request('status'),
        ]);

        flash('Reservation updated successfully.', 'success');

        return redirect(route('admin.reservations'));
    }

    public function updateStatus($request, $reservation)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0,2',
        ]);

        $reservation->update($validatedData);

        Mail::to($reservation->email)->send(new TableReservationStatus($reservation));

        return response()->json(['status' => 'success']);
    }

    public function isOpen($day, $time)
    {
        $storeHour = StoreHour::where('day', $day)
        ->where('status', 1)
        ->where('open_hour', '<=', $time)
        ->where('close_hour', '>=', $time)
        ->first();

        if (empty($storeHour)) {
            return 0;
        }

        return 1;
    }
}
