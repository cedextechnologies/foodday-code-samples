<?php

namespace App\Services;

use App\Models\StoreHour;
use Illuminate\Http\Request;

class WorkingHoursService
{
    public function getWorkingHours()
    {
        return [
            'open_hour_monday' => $this->getOpenHour(1),
            'close_hour_monday' => $this->getCloseHour(1),
            'status_monday' => $this->getStatus(1),

            'open_hour_tuesday' => $this->getOpenHour(2),
            'close_hour_tuesday' => $this->getCloseHour(2),
            'status_tuesday' => $this->getStatus(2),

            'open_hour_wednesday' => $this->getOpenHour(3),
            'close_hour_wednesday' => $this->getCloseHour(3),
            'status_wednesday' => $this->getStatus(3),

            'open_hour_thursday' => $this->getOpenHour(4),
            'close_hour_thursday' => $this->getCloseHour(4),
            'status_thursday' => $this->getStatus(4),

            'open_hour_friday' => $this->getOpenHour(5),
            'close_hour_friday' => $this->getCloseHour(5),
            'status_friday' => $this->getStatus(5),

            'open_hour_saturday' => $this->getOpenHour(6),
            'close_hour_saturday' => $this->getCloseHour(6),
            'status_saturday' => $this->getStatus(6),

            'open_hour_sunday' => $this->getOpenHour(7),
            'close_hour_sunday' => $this->getCloseHour(7),
            'status_sunday' => $this->getStatus(7),
        ];
    }

    public function update(Request $request)
    {
        if ($request->open_hour_monday) {
            $this->updateStoreHour(1, 'open_hour', $request->open_hour_monday);
        }

        if ($request->close_hour_monday) {
            $this->updateStoreHour(1, 'close_hour', $request->close_hour_monday);
        }

        if ($request->status_monday) {
            $this->updateStoreHour(1, 'status', 0);
        } else {
            $this->updateStoreHour(1, 'status', 1);
        }

        if ($request->open_hour_tuesday) {
            $this->updateStoreHour(2, 'open_hour', $request->open_hour_tuesday);
        }

        if ($request->close_hour_tuesday) {
            $this->updateStoreHour(2, 'close_hour', $request->close_hour_tuesday);
        }

        if ($request->status_tuesday) {
            $this->updateStoreHour(2, 'status', 0);
        } else {
            $this->updateStoreHour(2, 'status', 1);
        }

        if ($request->open_hour_wednesday) {
            $this->updateStoreHour(3, 'open_hour', $request->open_hour_wednesday);
        }

        if ($request->close_hour_wednesday) {
            $this->updateStoreHour(3, 'close_hour', $request->close_hour_wednesday);
        }

        if ($request->status_wednesday) {
            $this->updateStoreHour(3, 'status', 0);
        } else {
            $this->updateStoreHour(3, 'status', 1);
        }

        if ($request->open_hour_thursday) {
            $this->updateStoreHour(4, 'open_hour', $request->open_hour_thursday);
        }

        if ($request->close_hour_thursday) {
            $this->updateStoreHour(4, 'close_hour', $request->close_hour_thursday);
        }

        if ($request->status_thursday) {
            $this->updateStoreHour(4, 'status', 0);
        } else {
            $this->updateStoreHour(4, 'status', 1);
        }

        if ($request->open_hour_friday) {
            $this->updateStoreHour(5, 'open_hour', $request->open_hour_friday);
        }

        if ($request->close_hour_friday) {
            $this->updateStoreHour(5, 'close_hour', $request->close_hour_friday);
        }

        if ($request->status_friday) {
            $this->updateStoreHour(5, 'status', 0);
        } else {
            $this->updateStoreHour(5, 'status', 1);
        }

        if ($request->open_hour_saturday) {
            $this->updateStoreHour(6, 'open_hour', $request->open_hour_saturday);
        }

        if ($request->close_hour_saturday) {
            $this->updateStoreHour(6, 'close_hour', $request->close_hour_saturday);
        }

        if ($request->status_saturday) {
            $this->updateStoreHour(6, 'status', 0);
        } else {
            $this->updateStoreHour(6, 'status', 1);
        }

        if ($request->open_hour_sunday) {
            $this->updateStoreHour(7, 'open_hour', $request->open_hour_sunday);
        }

        if ($request->close_hour_sunday) {
            $this->updateStoreHour(7, 'close_hour', $request->close_hour_sunday);
        }

        if ($request->status_sunday) {
            $this->updateStoreStatus(7, 'status', 0);
        } else {
            $this->updateStoreStatus(7, 'status', 1);
        }

        flash('Working Hours updated successfully.', 'success');

        return redirect()->route('admin.working-hours');
    }

    public function updateStoreHour($day, $field, $value)
    {
        return StoreHour::where('day', $day)->update([
            $field => $value,
        ]);
    }

    public function updateStoreStatus($day, $field, $value)
    {
        return StoreHour::where('day', $day)->update([
            $field => $value,
        ]);
    }

    public function getOpenHour($day)
    {
        return StoreHour::where('day', $day)->first()->open_hour;
    }

    public function getCloseHour($day)
    {
        return StoreHour::where('day', $day)->first()->close_hour;
    }

    public function getStatus($day)
    {
        return StoreHour::where('day', $day)->first()->status;
    }
}
