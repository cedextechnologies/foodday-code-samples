<?php

namespace App\Services;

use Illuminate\Http\Request;

class PaymentSettingsService
{
    public function updatePaymentSettings(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->accept_cash) {
            $validatedData['accept_cash'] = 1;
        } else {
            $validatedData['accept_cash'] = 0;
        }

        if ($request->stripe_payment) {
            $validatedData['stripe_payment'] = 1;
        } else {
            $validatedData['stripe_payment'] = 0;
        }

        if ($request->braintree_payment) {
            $validatedData['braintree_payment'] = 1;
        } else {
            $validatedData['braintree_payment'] = 0;
        }

        if ($request->paypal_payment) {
            $validatedData['paypal_payment'] = 1;
        } else {
            $validatedData['paypal_payment'] = 0;
        }

        if ($request->checkout_delivery) {
            $validatedData['checkout_delivery'] = 1;
        } else {
            $validatedData['checkout_delivery'] = 0;
        }

        if ($request->checkout_carry_out) {
            $validatedData['checkout_carry_out'] = 1;
        } else {
            $validatedData['checkout_carry_out'] = 0;
        }

        if ($request->checkout_dine_in) {
            $validatedData['checkout_dine_in'] = 1;
        } else {
            $validatedData['checkout_dine_in'] = 0;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Payment settings updated successfully', 'success');

        return redirect()->route('admin.payment-settings');
    }

    public function updateStripeSettings(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->stripe_secret_key) {
            $validatedData['stripe_secret_key'] = $request->stripe_secret_key;
        }

        if ($request->stripe_publishable_key) {
            $validatedData['stripe_publishable_key'] = $request->stripe_publishable_key;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Stripe settings updated successfully', 'success');

        return redirect()->route('admin.payment-settings')->with('active_settings', 'stripe');
    }

    public function updateBraintreeSettings(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->braintree_environment) {
            $validatedData['braintree_environment'] = $request->braintree_environment;
        }

        if ($request->marchant_id) {
            $validatedData['marchant_id'] = $request->marchant_id;
        }

        if ($request->braintree_public_key) {
            $validatedData['braintree_public_key'] = $request->braintree_public_key;
        }

        if ($request->private_key) {
            $validatedData['private_key'] = $request->private_key;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Braintree settings updated successfully', 'success');

        return redirect()->route('admin.payment-settings')->with('active_settings', 'braintree');
    }

    public function updatePaypalSettings(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->paypal_environment) {
            $validatedData['paypal_environment'] = $request->paypal_environment;
        }

        if ($request->paypal_cient_id) {
            $validatedData['paypal_cient_id'] = $request->paypal_cient_id;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Paypal settings updated successfully', 'success');

        return redirect()->route('admin.payment-settings')->with('active_settings', 'paypal');
    }
}
