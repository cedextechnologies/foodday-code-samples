<?php

namespace App\Services;

use App\Models\Item;
use Illuminate\Support\Arr;
use Illuminate\Http\Request;
use App\Http\Resources\Item as ItemResource;

class ItemService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request)
    {
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('public/items');
            $image = substr($path, 7);
        }

        $item = Item::create($request->except('image') + ['image' => $image]);

        flash('Well Done! Item added successfully', 'success');

        return redirect()->route('admin.items.index');
    }

    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, Item $item)
    {
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store('public/items');
            $image = substr($path, 7);
            $item->update(['image' => $image]);
        }

        if (1 == $request->input('has_different_size')) {
            $price = 0;
        } else {
            $price = $request->input('price');
        }

        $item->update(Arr::add($request->except('image', 'price'), 'price', $price));

        flash('Well Done! Item updated successfully', 'success');

        return redirect()->route('admin.items.index');
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }

    public function show($item)
    {
        $item = Item::where('id', $item)->first();

        if (empty($item)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Item not found',
                ],
            ]);
        }

        return new ItemResource($item);
    }
}
