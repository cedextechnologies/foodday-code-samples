<?php

namespace App\Services;

use Auth;
use Validator;
use Newsletter;
use App\Models\Customer;
use App\Mail\NewCustomer;
use Illuminate\Http\Request;
use App\Models\PasswordReset;
use App\Rules\ValidResetToken;
use Illuminate\Validation\Rule;
use App\Notifications\ResetPassword;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use App\Rules\CurrentPasswordValidator;
use App\Http\Resources\Customer as CustomerResource;

class CustomerService
{
    public function filter(Request $request)
    {
        $customers = Customer::when($request->input('search'),
            function ($query) use ($request) {
                $query->whereRaw('(`first_name` like "%'.$request->input('search').'%" OR `last_name` like "%'.$request->input('search').'%")')
                ->orWhere('zipcode', $request->input('search'))
                ->orWhere('email', $request->input('search'))
                ->orWhere('phone', $request->input('phone'));
            })->orderBy('created_at', 'desc');

        return $customers;
    }

    public function update(Request $request, $customer)
    {
        $customer->fill($request->except(['password']));

        if ($request->filled('password')) {
            $customer->password = Hash::make($validatedData['password']);
        }

        $customer->save();

        flash('Customer updated successfully.', 'success');

        return redirect(route('admin.customers'));
    }

    public function updateProfile(Request $request, $customer)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            // 'email' => [
            //     'required',
            //     'email',
            //     Rule::unique('customers')->ignore($customer->id),
            // ],
            'phone' => 'required|string|max:15',
            'zipcode' => 'required|string|min:6|max:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        $customer->update($request->all());

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Profile updated successfully',
            ],
        ]);
    }

    public function store($request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required|alpha|max:255',
            'last_name' => 'required|alpha|max:255',
            'email' => 'required|email|unique:customers,email',
            'phone' => 'required|string|max:15',
            'zipcode' => 'required|string|min:6|max:6',
            'password' => 'required|min:6',
            'confirm_password' => 'required|same:password',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 422);
        }

        $customer = Customer::create(array_merge($request->except('password'), [
           'password' => Hash::make($request->input('password')),
           'customer_type' => 1,
           'status' => 1,
        ]));

        Mail::to($customer->email)->send(new NewCustomer($customer));

        return response()->json([
            'status' => 'success',
            'data' => [
                'first_name' => $customer->first_name,
                'last_name' => $customer->last_name,
                'email' => $customer->email,
            ],
        ]);
    }

    public function getProfile()
    {
        $customer = Auth::guard('customer')->user();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'account not found',
                ],
            ]);
        }

        return new CustomerResource($customer);
    }

    public function changePassword($request, $customer)
    {
        $validator = Validator::make($request->all(), [
            'current_password' => [
                'required',
                new CurrentPasswordValidator(),
            ],
            'password' => 'required|string|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        $customer->password = bcrypt($request->input('password'));

        $customer->update();

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Password changed successfully',
            ],
        ]);
    }

    public function forgot($request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 422);
        }

        $customer = Customer::where('email', $request->input('email'))->first();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Account not found',
                ],
            ]);
        }

        $token = mt_rand(100000, 999999);

        $passwordReset = PasswordReset::insert([
            'email' => $customer->email,
            'token' => $token,
            'created_at' => now(),
        ]);

        $customer->notify(new ResetPassword($customer, $token));

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Please check your email for OTP',
            ],
        ]);
    }

    public function reset($request)
    {
        $validator = Validator::make($request->all(), [
            'token' => ['bail', 'required', new ValidResetToken()],
            'password' => 'required|min:6|confirmed',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        $token = PasswordReset::where('token', $request->input('token'))->first();
        $customer = $token->customer;

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Token is invalid',
                ],
            ]);
        }

        $customer->password = Hash::make($request->password);
        $customer->save();

        PasswordReset::where('token', $request->input('token'))->delete();

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'password updated successfully',
            ],
        ]);
    }

    public function subscribe(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 422);
        }

        //$subscribe = Newsletter::subscribe(request('email'));

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Subscribed successfully',
            ],
        ]);
    }
}
