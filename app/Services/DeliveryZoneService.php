<?php

namespace App\Services;

use App\Models\DeliveryZone;
use Illuminate\Http\Request;

class DeliveryZoneService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request)
    {
        $coordinates = formatDeliveryZone($request->input('polygondata'));

        DeliveryZone::create([
            'name' => $request->input('name'),
            'delivery_available' => $request->input('delivery_available'),
            'delivery_charge' => 0,
            'default_delivery_charge' => $request->input('default_delivery_charge'),
            'geometry' => \DB::raw("ST_GeomFromText('POLYGON(($coordinates))')"),
        ]);

        flash('Well Done! delivery zone added successfully', 'success');

        return redirect()->route('admin.zones.index');
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }

    /**
     * Check delivery zone by zipcode.
     *
     * @param string $zipcode
     *
     * @return JSON
     */
    public function isDeliveryAvailable(Request $request)
    {
        $latitude = request('latitude');
        $longitude = request('longitude');

        if ('' != $latitude && '' != $longitude) {
            $zone = DeliveryZone::whereRaw('ST_Contains(geometry, GeomFromText(?))', ["POINT($latitude $longitude)"])->where('delivery_available', 1)->first();

            if (! empty($zone)) {
                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'message' => 'Delivery available',
                    ],
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Delivery not available to this location',
                    ],
                ]);
            }
        }

        return response()->json([
            'status' => 'error',
            'data' => [
                'message' => 'Invalid location',
            ],
        ]);
    }


    public function storeCustomeDeliveryFee($request, $zone)
    {
        $orderAmounts = $request->orderAmount;
        $orderDeliveryCharges = $request->orderDeliveryCharge;

        $zone->customDeliveryFees()->delete();

        foreach ($orderAmounts as $key => $orderAmount) {
            if($orderAmount && $orderDeliveryCharges[$key])
            {

                $zone->customDeliveryFees()->insert([
                    'delivery_zone_id' => $zone->id,
                    'order_amount' => $orderAmount,
                    'delivery_fee_amount' => $orderDeliveryCharges[$key]
                ]);
            }
        }
    }

    public function updateDeliveryLocation($request)
    {

        $latitude = request('latitude');
        $longitude = request('longitude');

        if ('' != $latitude && '' != $longitude) {
            $zone = DeliveryZone::whereRaw('ST_Contains(geometry, GeomFromText(?))', ["POINT($latitude $longitude)"])->where('delivery_available', 1)->first();


            session([
                'deliveryZone' => ($zone) ? $zone->id : null,
                'isDeliveryAvailable' => ($zone) ? 1 : 0,
                'deliveryLocation' => $request->location,
            ]);



            if (! empty($zone)) {

                return response()->json([
                    'status' => 'success',
                    'data' => [
                        'message' => 'Delivery available',
                    ],
                ]);
            } else {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Delivery not available to this location',
                    ],
                ]);
            }
        }


    }
}
