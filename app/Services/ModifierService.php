<?php

namespace App\Services;

use App\Models\Item;
use App\Models\ItemModifier;
use Illuminate\Http\Request;
use App\Models\ItemModifierDetail;

class ModifierService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request, Item $item)
    {
        $modifier = ItemModifier::create([
            'item_id' => $item->id,
            'title' => $request->input('title'),
            'minimum' => $request->input('minimum'),
            'maximum' => $request->input('maximum'),
        ]);

        $names = collect(request('item_name'));
        $prices = collect(request('item_price'));

        $items = $names->combine($prices);

        foreach ($items as $name => $price) {
            ItemModifierDetail::create([
                'modifier_id' => $modifier->id,
                'name' => $name,
                'price' => $price,
            ]);
        }

        flash('Well Done! Modifier added successfully', 'success');

        return response()->json(['status' => 1]);
    }

    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, Item $item, ItemModifier $modifier)
    {
        $modifier->update($request->all());

        ItemModifierDetail::where('modifier_id', $modifier->id)->delete();

        $names = collect(request('item_name'));
        $prices = collect(request('item_price'));

        $items = $names->combine($prices);

        foreach ($items as $name => $price) {
            ItemModifierDetail::create([
                'modifier_id' => $modifier->id,
                'name' => $name,
                'price' => $price,
            ]);
        }

        flash('Well Done! Modifier updated successfully', 'success');

        return response()->json(['status' => 1]);
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }
}
