<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderService
{
    public function filter(Request $request)
    {
        $orders = Order::when($request->input('search'),
            function ($query) use ($request) {
                $query->where('order_reference', 'like', '%'.$request->search.'%');
            })->orderBy('created_at', 'desc')->paginate(10);

        return $orders;
    }

    public function update(Request $request, Order $order)
    {
        $order->fill($request->except('booking_time'));

        if ($request->has('booking_time') && ! is_null($request->booking_time)) {
            $order->booking_time = Carbon::parse($request->booking_time);
        }

        $order->save();

        flash('Order updated successfully.', 'success');

        return redirect(route('admin.orders'));
    }
}
