<?php

namespace App\Services;

use Auth;
use Session;
use Validator;
use Carbon\Carbon;
use App\Models\Cart;
use App\Models\Item;
use App\Models\Order;
use App\Models\Coupon;
use App\Models\CartItem;
use App\Models\Customer;
use App\Models\ItemSize;
use App\Models\OrderItem;
use App\Models\StoreHour;
use App\Traits\CartTrait;
use App\Events\NewOrderReceived as NewOrderNotification;
use Illuminate\Support\Str;
use App\Models\DeliveryZone;
use App\Models\ItemModifier;
use Illuminate\Http\Request;
use App\Models\OrderItemSize;
use App\Mail\NewOrderReceived;
use App\Models\CartItemModifier;
use App\Models\OrderItemModifier;
use App\Models\ItemModifierDetail;
use Illuminate\Support\Facades\Mail;
use App\Models\CartItemModifierDetail;
use App\Http\Resources\Cart as CartResource;

class CartService
{
    use CartTrait;

    public function getCartToken()
    {
        $token = $this->generateToken();

        Cart::create([
            'token' => $token,
            'coupon_applied' => 0,
        ]);

        return response()->json([
            'status' => 'success',
            'data' => [
                'cart_token' => $token,
            ],
        ]);
    }

    public function getWebCartToken()
    {
        if ('' != Session::get('web_cart_token')) {
            return Session::get('web_cart_token');
        }

        $token = $this->generateToken();

        Cart::create([
            'token' => $token,
            'coupon_applied' => 0,
        ]);
        Session::put('web_cart_token', $token);

        return $token;
    }

    /**
     * Generate cart token.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return token
     */
    public function generateToken()
    {
        $token = md5(Str::random(10));

        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return $token;
        }

        $this->generateToken();
    }

    /**
     * Generate rowId.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return token
     */
    public function generateRowId()
    {
        $rowId = Str::random(16);

        $cartItem = CartItem::where('row_id', $rowId)->first();

        if (empty($cartItem)) {
            return $rowId;
        }

        $this->generateToken();
    }

    /**
     * Generate order reference id.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return token
     */
    public function generateOrderReference()
    {
        $reference = mt_rand(100000, 999999);

        $order = Order::where('order_reference', $reference)->first();

        if (empty($order)) {
            return $reference;
        }

        $this->generateOrderReference();
    }

    /**
     * Generate payment token.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return token
     */
    public function generatePaymentToken()
    {
        $token = Str::random(16);

        $order = Order::where('payment_token', $token)->first();

        if (empty($order)) {
            return $token;
        }

        $this->generatePaymentToken();
    }

    public function getCart($token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        $today = date('Y-m-d');

        if (1 == $cart->coupon_applied) {
            $coupon = Coupon::where('code', $cart->code)->first();

            if (! $coupon->isActive()) {
                $this->deleteCoupon($cart, $coupon);
            }

            if ($today < $coupon->start_date) {
                $this->deleteCoupon($cart, $coupon);
            }

            if ($today > $coupon->end_date) {
                $this->deleteCoupon($cart, $coupon);
            }
        }

        return new CartResource($cart);
    }

    public function deleteCoupon($cart, $coupon)
    {
        $cart->coupon_applied = 0;
        $cart->code = '';
        $cart->update();

        $coupon->decrement('used_count');
    }

    public function updateCart($request, $token)
    {
        $total = 0.0;

        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        if (! empty($request->items)) {
            foreach ($request->items as $item) {
                $dishItem = Item::where('id', $item['id'])->first();

                if (empty($dishItem)) {
                    return response()->json([
                        'status' => 'error',
                        'data' => [
                            'message' => 'Item not found',
                        ],
                    ]);
                }

                foreach ($dishItem->modifiers as $modifier) {
                    if ($modifier->minimum > 0) {
                        if (! isset($item['modifiers']) || empty($item['modifiers'])) {
                            return response()->json([
                                'status' => 'error',
                                'data' => [
                                    'message' => 'You must select atleast '.$modifier->minimum.' '.$modifier->title,
                                ],
                            ]);
                        }
                    }
                }

                if (isset($item['modifiers']) && ! empty($item['modifiers'])) {
                    foreach ($item['modifiers'] as $modifier) {
                        $dishModifier = ItemModifier::where('id', $modifier['id'])->first();

                        if (empty($dishModifier)) {
                            return response()->json([
                                'status' => 'error',
                                'data' => [
                                    'message' => 'Modifier not found',
                                ],
                            ]);
                        }

                        $count = count($modifier['modifier_items']);

                        if ($dishModifier->minimum > $count) {
                            return response()->json([
                                'status' => 'error',
                                'data' => [
                                    'message' => 'You must select atleast '.$dishModifier->minimum.' '.$dishModifier->title,
                                ],
                            ]);
                        } elseif ($dishModifier->maximum < $count) {
                            return response()->json([
                                'status' => 'error',
                                'data' => [
                                    'message' => 'You can\'t select more than '.$dishModifier->maximum.' '.$dishModifier->title,
                                ],
                            ]);
                        }

                        if (! empty($modifier['modifier_items'])) {
                            foreach ($modifier['modifier_items'] as $modifierItem) {
                                $dishModifierItem = ItemModifierDetail::where('id', $modifierItem)->first();

                                if (empty($dishModifierItem)) {
                                    return response()->json([
                                        'status' => 'error',
                                        'data' => [
                                            'message' => 'Modifier item not found',
                                        ],
                                    ]);
                                }
                            }
                        }
                    }
                }

                if (isset($item['size']) && ! empty($item['size'])) {
                    $itemSize = ItemSize::where('id', $item['size'])->first();

                    if (empty($itemSize)) {
                        return response()->json([
                            'status' => 'error',
                            'data' => [
                                'message' => 'Item size not found',
                            ],
                        ]);
                    }
                } else {
                    if (1 == $dishItem->has_different_size) {
                        return response()->json([
                            'status' => 'error',
                            'data' => [
                                'message' => 'Item must have size',
                            ],
                        ]);
                    }
                }
            }
        } else {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'No items found',
                ],
            ]);
        }

        foreach ($request->items as $item) {
            $cartItem = CartItem::where('cart_id', $cart->id)
            ->where('item_id', $item['id'])
            ->when(isset($item['size']), function ($query) use ($item) {
                return $query->where('size_id', $item['size']);
            })
            ->first();

            if ($cartItem) {
                if (isset($item['modifiers']) && ! empty($item['modifiers']) && $cartItem->modifiers->count() > 0) {
                    if (1 == $this->isMatchModifiers($cartItem, $item['modifiers'])) {
                        $cartItem->increment('quantity', $item['quantity']);
                    } else {
                        $this->createCartItem($cart, $item);
                    }
                } else {
                    $cartItem->increment('quantity', $item['quantity']);
                }
            } else {
                $this->createCartItem($cart, $item);
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Cart updated successfully',
            ],
        ]);
    }

    public function createCartItem($cart, $item)
    {
        $rowId = $this->generateRowId();

        $dishItem = Item::where('id', $item['id'])->first();

        $cartItem = CartItem::create([
            'row_id' => $rowId,
            'cart_id' => $cart->id,
            'item_id' => $item['id'],
            'quantity' => $item['quantity'],
            'size_id' => isset($item['size']) && '' != $item['size'] ? $item['size'] : null,
            'instruction' => $item['instruction'],
        ]);

        if (! empty($item['modifiers'])) {
            foreach ($item['modifiers'] as $modifier) {
                $cartItemModifier = CartItemModifier::create([
                    'cart_item_id' => $cartItem->id,
                    'item_modifier_id' => $modifier['id'],
                ]);

                if (! empty($modifier['modifier_items'])) {
                    foreach ($modifier['modifier_items'] as $modifierItem) {
                        CartItemModifierDetail::create([
                            'cart_item_modifier_id' => $cartItemModifier->id,
                            'item_modifier_detail_id' => $modifierItem,
                        ]);
                    }
                }
            }
        }

        return true;
    }

    public function updateQuantity($request, $token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        $cartItem = CartItem::where('row_id', $request->input('row_id'))->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Item not found',
                ],
            ]);
        }

        if ($request->input('quantity') > 0) {
            $cartItem->update(['quantity' => $request->input('quantity')]);
        } else {
            $cartItem->delete();
        }

        flash('Quantity updated successfully.', 'success');

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Cart updated successfully',
            ],
        ]);
    }

    public function getModifierPrice($modifiersDetails)
    {
        return ItemModifierDetail::whereIn('id', $modifiersDetails)->sum('price');
    }

    public function removeCartItem($request, $cart)
    {
        $cartItem = CartItem::where('row_id', $request->input('row_id'))->first();

        if (empty($cartItem)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Item not found',
                ],
            ]);
        }

        $cartItem->delete();

        if (1 == $cart->coupon_applied) {
            $coupon = Coupon::where('code', $cart->code)->first();

            $cartTotals = $this->getCartTotals($cart->items);

            if ($cartTotals['subtotal'] <= $coupon->min_amount) {
                $cart->coupon_applied = 0;
                $cart->code = '';
                $cart->update();
                $coupon->decrement('used_count');
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Item Deleted successfully',
            ],
        ]);
    }

    public function applyCoupon($request, $token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        if ($cart->items->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Cart is empty',
                ],
            ]);
        }

        $coupon = Coupon::where('code', request('code'))->first();

        if (empty($coupon)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid coupon',
                ],
            ]);
        }

        $today = date('Y-m-d H:i:s');

        if (! $coupon->isActive()) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid coupon',
                ],
            ]);
        }

        if (Carbon::now()->lessThan(Carbon::parse($coupon->start_date))) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Coupon discount does not started yet',
                ],
            ]);
        }

        if (Carbon::now()->greaterThan(Carbon::parse($coupon->end_date))) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Coupon expired',
                ],
            ]);
        }

        if ($coupon->used_count >= $coupon->usage_limit) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Coupon usage limit exceeded',
                ],
            ]);
        }

        $cartTotals = $this->getCartTotals($cart->items, $cart);

        if ($cartTotals['subtotal'] <= $coupon->min_amount) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'You must have minimum order amount of '.format_currency($coupon->min_amount).'.',
                ],
            ]);
        }

        if (0 == $cart->coupon_applied) {
            $coupon->increment('used_count');
        }

        $cart->coupon_applied = 1;
        $cart->code = $coupon->code;
        $cart->update();

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Coupon added successfully',
            ],
        ]);
    }

    public function removeCoupon($request, $token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        if ($cart->items->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Cart is empty',
                ],
            ]);
        }

        $coupon = Coupon::where('code', request('code'))->first();

        if (empty($coupon)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid coupon',
                ],
            ]);
        }

        if (0 == $cart->coupon_applied) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Coupon does not exists',
                ],
            ]);
        }

        $cart->coupon_applied = 0;
        $cart->code = '';
        $cart->update();

        $coupon->decrement('used_count');

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Coupon removed successfully',
            ],
        ]);
    }

    public function checkout($request, $token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }


        if($request->checkout_type == '1' && $request->session()->get('isDeliveryAvailable') == 0)
        {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Delivery is not available in choosen location, Please select carryout option.',
                ],
            ]);
        }

        $validator = Validator::make($request->all(), [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email',
            'phone' => 'required|string|max:20',
            'zipcode' => 'required|string|max:15',
            'door' => 'required|string',
            'company' => 'required|string',
            'address_line_1' => 'required|string|max:255',
            'address_line_2' => 'required|string|max:255',
            'payment_mode' => 'required|in:1,2,3',
            'checkout_type' => 'required|in:1,2',
            'is_preorder' => 'required|in:1,0',
            'preorder_date' => 'nullable|required_if:is_preorder,1|date_format:Y-m-d',
            'preorder_time' => 'nullable|required_if:is_preorder,1|date_format:H:i',
            'order_date' => date('Y-m-d'),
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        $cartTotals = $this->getCartTotals($cart->items, $request->checkout_type);

        if ($cartTotals['subtotal'] <= getStoreSettings('minimum_order')) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Minimum order amount not reached',
                ],
            ]);
        }

        if (1 == request('is_preorder')) {
            $day = \Carbon\Carbon::parse(request('preorder_date'))->format('N');
            $time = \Carbon\Carbon::parse(request('preorder_time'))->format('H:i');

            if (0 == getStoreSettings('allow_pre_order')) {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Preorders are not allowd',
                    ],
                ]);
            }

            if (0 == $this->isOpen($day, $time)) {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Restaurant not opened at this time',
                    ],
                ]);
            }
        } else {
            if (0 == $this->isOpen(date('N'), date('H:i'))) {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Restaurant not opened at this time',
                    ],
                ]);
            }
        }

        if (1 == request('checkout_type') && 0 == getStoreSettings('checkout_delivery')) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Delivery is not available',
                ],
            ]);
        }

        if (2 == request('checkout_type') && 0 == getStoreSettings('checkout_carry_out')) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Pickup is not available.',
                ],
            ]);
        }

        $latitude = request('latitude');
        $longitude = request('longitude');

        if (1 == request('checkout_type') && '' != $latitude && '' != $longitude) {
            $zone = DeliveryZone::whereRaw('ST_Contains(geometry, GeomFromText(?))', ["POINT($latitude $longitude)"])->where('delivery_available', 1)->first();
            if (empty($zone)) {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'Delivery is not available to this location',
                    ],
                ]);
            }
        }

        $customer = Auth::user();

        if (empty($customer)) {
            $customer = Auth::guard('customer')->user();
        }

        $customerData = [
            'customer_type' => 2,
            'first_name' => request('first_name'),
            'last_name' => request('last_name'),
            'zipcode' => request('zipcode'),
            'door' => request('door'),
            'company' => request('company'),
            'address_line_1' => request('address_line_1'),
            'address_line_2' => request('address_line_2'),
            'email' => request('email'),
            'phone' => request('phone'),
            'status' => 1,
        ];

        if (empty($customer)) {
            $customer = Customer::where('email', request('email'))->first();

            if (! empty($customer)) {
                $customer->update($customerData);
            } else {
                $customer = Customer::create($customerData);
            }
        } else {
            $customer->update($customerData);
        }

        $discount = $this->getCouponDiscount($cart, $cartTotals['order_total']);

        $order = Order::create([
            'order_reference' => $this->generateOrderReference(),
            'customer_id' => $customer->id,
            'tax' => $cartTotals['tax'],
            'tax_amount' => $cartTotals['tax_amount'],
            'delivery_charge' => $cartTotals['delivery_charge'],
            'subtotal' => $cartTotals['subtotal'],
            'coupon_code' => $cart->code,
            'coupon_discount' => $discount,
            'payed_amount' => $cartTotals['order_total'] - $discount,
            'payment_status' => 1 == request('payment_mode') ? 1 : 0,
            'payment_mode' => request('payment_mode'),
            'checkout_type' => request('checkout_type'),
            'is_preorder' => request('is_preorder'),
            'preorder_date' => 1 == request('is_preorder') ? request('preorder_date').' '.request('preorder_time').':00' : null,
            'payment_token' => 1 != request('payment_mode') ? $this->generatePaymentToken() : '',
            'status' => 0,
        ]);

        $cart->update(['order_reference' => $order->order_reference]);

        foreach ($cart->items as $cartItem) {
            $price = $this->getItemPrice($cartItem);

            $orderItem = OrderItem::create([
                'order_id' => $order->id,
                'item_id' => $cartItem->item_id,
                'quantity' => $cartItem->quantity,
                'price' => $price,
                'item_total' => $cartItem->getPrice(),
                'subtotal' => $cartItem->quantity * $price,
                'instruction' => $cartItem->instruction,
            ]);

            if (! empty($cartItem->size)) {
                OrderItemSize::create([
                    'order_item_id' => $orderItem->id,
                    'size_id' => $cartItem->size_id,
                ]);
            }

            $modifierTotal = 0;

            if ($cartItem->modifiers->isNotEmpty()) {
                foreach ($cartItem->modifiers as $modifierItem) {
                    $modifier = OrderItemModifier::create([
                        'order_item_id' => $orderItem->id,
                        'modifier_name' => $modifierItem->cartItemModifier->itemModifier->title,
                        'item_modifier_details_id' => $modifierItem->item_modifier_detail_id,
                        'item_name' => $modifierItem->itemModifierDetail->name,
                        'item_price' => $modifierItem->itemModifierDetail->price,
                    ]);
                    $modifierTotal += ($modifierItem->itemModifierDetail->price * $cartItem->quantity);
                }
            }

            $orderItem->subtotal += $modifierTotal;
            $orderItem->update();
        }

        Mail::to(env('CONTACT_TO_MAIL'))->send(new NewOrderReceived($order));

        Mail::to($order->customer->email)->send(new NewOrderReceived($order));

        event(new NewOrderNotification());

        if (1 == request('payment_mode')) {
            $cart->delete();
            $request->session()->forget('web_cart_token');

            return response()->json([
                'status' => 'success',
                'data' => [
                    'message' => 'Order placed successfully',
                    'order_reference' => $order->order_reference,
                    'redirect_url' => route('customer.order.confirmation', $order),
                ],
            ]);
        } else {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'payment_token' => $order->payment_token,
                    'order_reference' => $order->order_reference,
                    'order_total' => format_currency($order->payed_amount),
                ],
            ]);
        }
    }

    public function getItemPrice($cartItem)
    {
        return ! empty($cartItem->size) ? $cartItem->size->price : $cartItem->item->price;
    }

    public function isOpen($day, $time)
    {
        $storeHour = StoreHour::where('day', $day)
        ->where('status', 1)
        ->where('open_hour', '<=', $time)
        ->where('close_hour', '>=', $time)
        ->first();

        if (empty($storeHour)) {
            return 0;
        }

        return 1;
    }

    public function isMatchModifiers($cartItem, $modifiers)
    {
        foreach ($modifiers as $modifier) {
            if (! empty($modifier['modifier_items'])) {
                foreach ($modifier['modifier_items'] as $modifierItem) {
                    $modifierItems[] = $modifierItem;
                }
            }
        }

        $cartItemModifiers = $cartItem->modifiers->pluck('item_modifier_detail_id');

        $diff = $cartItemModifiers->diff($modifierItems);

        if (0 == $diff->count()) {
            return 1;
        }

        return 0;
    }
}
