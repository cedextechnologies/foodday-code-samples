<?php

namespace App\Services;

use App\Models\CommonModifier;
use Illuminate\Http\Request;
use App\Models\CommonModifierDetail;
use App\Models\ItemModifier;
use App\Models\ItemModifierDetail;
use App\Models\Item;

class CommonModifierService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request)
    {
        $modifier = CommonModifier::create([
            'title' => $request->input('title'),
            'minimum' => $request->input('minimum'),
            'maximum' => $request->input('maximum'),
        ]);

        $names = collect(request('item_name'));
        $prices = collect(request('item_price'));

        $items = $names->combine($prices);

        foreach ($items as $name => $price) {
            CommonModifierDetail::create([
                'common_modifier_id' => $modifier->id,
                'name' => $name,
                'price' => $price,
            ]);
        }

        $request->flash();

        flash('Well Done! Modifier added successfully', 'success');
        return redirect()->route('admin.common-modifiers.index');
    }

    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, CommonModifier $commonModifier)
    {
        $commonModifier->update($request->all());

        CommonModifierDetail::where('common_modifier_id', $commonModifier->id)->delete();

        $names = collect(request('item_name'));
        $prices = collect(request('item_price'));

        $items = $names->combine($prices);

        foreach ($items as $name => $price) {
            CommonModifierDetail::create([
                'common_modifier_id' => $commonModifier->id,
                'name' => $name,
                'price' => $price,
            ]);
        }

        flash('Well Done! Modifier updated successfully', 'success');
        return redirect()->route('admin.common-modifiers.index');
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }

    /**
     * Store to item.
     *
     * @param Request $request [modifier id]
     *
     * @return [status]
     */


    public function storeToItem($request, $item)
    {

        $commonModifier = CommonModifier::find($request->common_modifier);

        if (empty($commonModifier)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Common modifier not found',
                ],
            ]);
        }

        $itemCommonModifier = ItemModifier::Create([
            'item_id' => $item->id,
            'title' => $commonModifier->title,
            'minimum' => $commonModifier->minimum,
            'maximum' => $commonModifier->maximum,
            'common_modifier_id' => $commonModifier->id
        ]);

        $commonModifierItems = $commonModifier->CommonModifierDetails;

        foreach ($commonModifierItems as $key => $commonModifierItem) {
            $itemCommonModifier->ItemModifierDetails()->create([
                'modifier_id' => $itemCommonModifier->id,
                'name' => $commonModifierItem->name,
                'price' => $commonModifierItem->price,
            ]);
        }

        flash('Well Done! Modifier added successfully', 'success');
        return response()->json(['status' => 1]);

    }


    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function updateToItem(Request $request, Item $item, ItemModifier $itemModifier)
    {
        $itemModifier->update($request->all());

        $itemModifier->itemModifierDetails()->delete();
        $names = collect(request('item_name'));
        $prices = collect(request('item_price'));

        $items = $names->combine($prices);

        foreach ($items as $name => $price) {
            ItemModifierDetail::create([
                'modifier_id' => $itemModifier->id,
                'name' => $name,
                'price' => $price,
            ]);
        }

        flash('Well Done! Modifier updated successfully', 'success');
        return response()->json(['status' => 1]);
    }

}
