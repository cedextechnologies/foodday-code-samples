<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Order;
use App\Models\Reservation;
use Illuminate\Support\Facades\DB;

class DashboardService
{
    public function getCardData()
    {
        $data['todays-orders'] = Order::where('status', '!=', 0)
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->whereDate('created_at', Carbon::today())->count();

        $data['todays-reservations'] = Reservation::where('deleted', 0)->whereDate('created_at', Carbon::today())->count();

        $salesToday = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->whereDate('created_at', Carbon::today())->pluck('paidsum')->first();

        $data['todays-sales'] = format_currency($salesToday);

        $data['this-weeks-orders'] = Order::where('status', '!=', 0)
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->whereBetween('created_at', [Carbon::now()->subDays(7)->format('Y-m-d H:i:s'), Carbon::tomorrow()->format('Y-m-d H:i:s')])->count();

        $data['this-weeks-reservations'] = Reservation::where('deleted', 0)
        ->whereBetween('created_at', [Carbon::now()->subDays(7)->format('Y-m-d H:i:s'), Carbon::tomorrow()->format('Y-m-d H:i:s')])->count();

        $salesThisWeek = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->whereBetween('created_at', [Carbon::now()->subDays(7)->format('Y-m-d H:i:s'), Carbon::tomorrow()->format('Y-m-d H:i:s')])->pluck('paidsum')->first();

        $data['this-weeks-sales'] = format_currency($salesThisWeek);

        $salesThisMonth = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->whereBetween('created_at', [Carbon::now()->subDays(30)->format('Y-m-d H:i:s'), Carbon::tomorrow()->format('Y-m-d H:i:s')])->pluck('paidsum')->first();

        $data['this-month-sales'] = format_currency($salesThisMonth);

        $totalSales = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
        ->where('deleted', 0)
        ->where('payment_status', '!=', 0)
        ->pluck('paidsum')->first();

        $data['total-sales'] = format_currency($totalSales);

        return $data;
    }

    public function getGraphData($request)
    {
        if ('six-month' == request('type') || '' == request('type')) {
            foreach (range(5, 0) as $i) {
                $date = \Carbon\Carbon::now();

                $months[] = $date->subMonth($i)->format('F');
                $sale = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
                ->where('deleted', 0)
                ->where('payment_status', '!=', 0)
                ->whereMonth('created_at', $date->format('m'))
                ->whereYear('created_at', $date->format('Y'))
                ->pluck('paidsum')->first();

                $sales[] = '' == $sale ? 0 : $sale;
            }

            $graphData['months'] = $months;
            $graphData['sales'] = '' == $sales ? 0 : $sales;
            $graphData['date_range'] = getDateString($date->subMonth(6)->startOfMonth()).' - '.getToday();

            return $graphData;
        }

        if ('this-year' == request('type')) {
            foreach (range(\Carbon\Carbon::now()->format('m') - 1, 0) as $i) {
                $date = \Carbon\Carbon::now();

                $months[] = $date->subMonth($i)->format('F');

                $sale = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
                ->where('deleted', 0)
                ->where('payment_status', '!=', 0)
                ->whereMonth('created_at', $date->format('m'))
                ->whereYear('created_at', $date->format('Y'))
                ->pluck('paidsum')->first();

                $sales[] = '' == $sale ? 0 : $sale;
            }

            $graphData['months'] = $months;
            $graphData['sales'] = $sales;
            $graphData['date_range'] = getDateString($date->subMonth($date->format('m') - 1)->startOfMonth()).' - '.getToday();

            return $graphData;
        }

        if ('previous-year' == request('type')) {
            foreach (range(12, 1) as $i) {
                $date = \Carbon\Carbon::now();

                $months[] = date('F', mktime(0, 0, 0, $i, 1));

                $sale = Order::select(DB::raw('SUM(payed_amount) as paidsum'))->whereIn('status', [1, 2, 4])
                ->where('deleted', 0)
                ->where('payment_status', '!=', 0)
                ->whereMonth('created_at', sprintf('%02d', $i))
                ->whereYear('created_at', $date->subYear()->format('Y'))
                ->pluck('paidsum')->first();

                $sales[] = '' == $sale ? 0 : $sale;
            }

            $graphData['months'] = $months;
            $graphData['sales'] = '' == $sales ? 0 : $sales;
            $graphData['date_range'] = getDateString($date->startOfYear()).' - '.getDateString($date->endOfYear());

            return $graphData;
        }
    }
}
