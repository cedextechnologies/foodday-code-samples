<?php

namespace App\Services;

use Illuminate\Http\Request;

class OrderSettingsService
{
    public function update(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->minimum_order) {
            $validatedData['minimum_order'] = $request->minimum_order;
        }

        if ($request->tax) {
            $validatedData['tax'] = $request->tax;
        }

        if ($request->delivery_charges) {
            $validatedData['delivery_charges'] = $request->delivery_charges;
        }

        $validatedData['allow_pre_order'] = $request->allow_pre_order;

        $storeSettingsService->updateSettings($validatedData);

        flash('Order settings updated successfully', 'success');

        return redirect()->route('admin.order-settings');
    }
}
