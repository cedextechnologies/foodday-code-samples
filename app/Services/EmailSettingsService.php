<?php

namespace App\Services;

use Illuminate\Http\Request;

class EmailSettingsService
{
    public function update(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->from_email) {
            $validatedData['from_email'] = $request->from_email;
        }

        if ($request->from_email_name) {
            $validatedData['from_email_name'] = $request->from_email_name;
        }

        if ($request->smtp_port) {
            $validatedData['smtp_port'] = $request->smtp_port;
        }

        if ($request->smtp_username) {
            $validatedData['smtp_username'] = $request->smtp_username;
        }

        if ($request->smtp_host) {
            $validatedData['smtp_host'] = $request->smtp_host;
        }

        if ($request->smtp_password) {
            $validatedData['smtp_password'] = $request->smtp_password;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Email settings updated successfully', 'success');

        return redirect()->route('admin.email-settings');
    }
}
