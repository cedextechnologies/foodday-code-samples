<?php

namespace App\Services;

use Illuminate\Http\Request;

class SocialMediaService
{
    public function update(Request $request)
    {
        $storeSettingsService = new StoreSettingsService();

        if ($request->facebook) {
            $validatedData['facebook'] = $request->facebook;
        }

        if ($request->twitter) {
            $validatedData['twitter'] = $request->twitter;
        }

        if ($request->youtube) {
            $validatedData['youtube'] = $request->youtube;
        }

        if ($request->instagram) {
            $validatedData['instagram'] = $request->instagram;
        }

        if ($request->pinterest) {
            $validatedData['pinterest'] = $request->pinterest;
        }

        $storeSettingsService->updateSettings($validatedData);

        flash('Social medias updated successfully', 'success');

        return redirect()->route('admin.social-medias');
    }
}
