<?php

namespace App\Services;

use Stripe\Charge;
use Stripe\Stripe;
use App\Models\Cart;
use App\Models\Order;
use Stripe\Customer as StripeCustomer;

class StripePaymentService
{
    public function makePayment($request, $token)
    {
        $order = Order::where('payment_token', $token)->first();

        if (empty($order)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid payment token',
                ],
            ]);
        }

        try {
            Stripe::setApiKey(env('STRIPE_SECRET_KEY'));

            $customer = StripeCustomer::create([
                'email' => $request->stripeEmail,
                'source' => $request->stripeToken,
            ]);

            $charge = Charge::create([
                'customer' => $customer->id,
                'amount' => $order->payed_amount * 100,
                'currency' => 'usd',
            ]);

            $order->update(['payment_status' => 1]);

            $cart = Cart::where('order_reference', $order->order_reference)->first();

            if ($cart) {
                $cart->delete();
            }

            return response()->json([
                'status' => 'success',
                'data' => [
                    'message' => 'Payment Completed Successfully.',
                ],
            ]);
        } catch (\Exception $ex) {
            $order->delete();

            if (1 != $order->payment_status) {
                $order->update(['status' => 2]);
            }

            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Payment is not successfull.',
                ],
            ]);
        }
    }
}
