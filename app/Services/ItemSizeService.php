<?php

namespace App\Services;

use App\Models\Item;
use App\Models\ItemSize;
use Illuminate\Http\Request;

class ItemSizeService
{
    /**
     * Create.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function store(Request $request, Item $item)
    {
        ItemSize::create([
            'item_id' => $item->id,
            'size_id' => $request->input('size'),
            'price' => $request->input('price'),
        ]);

        flash('Well Done! size added successfully', 'success');

        return response()->json(['status' => 1]);
    }

    /**
     * Update.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function update(Request $request, Item $item, ItemSize $size)
    {
        $size->update([
            'size_id' => $request->input('size'),
            'price' => $request->input('price'),
        ]);

        flash('Well Done! size updated successfully', 'success');

        return response()->json(['status' => 1]);
    }

    /**
     * Delete.
     *
     * @param Request $request [description]
     *
     * @return [type] [description]
     */
    public function delete($information)
    {
        $information->delete();

        return true;
    }
}
