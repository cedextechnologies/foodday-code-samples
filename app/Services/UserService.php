<?php

namespace App\Services;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService
{
    public function filter(Request $request)
    {
        $users = User::whereNotIn('id',['1'])->when($request->input('search'),
            function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->search.'%')
                ->orWhere('email', $request->input('search'));
            })->orderBy('created_at', 'desc');

        return $users;
    }

    public function create(Request $request)
    {
        $user = new User();

        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->status = $request->input('status');

        if ($request->input('password')) {
            $user->password = Hash::make($request->input('password'));
        }

        $user->save();

        flash('User created successfully.', 'success');

        return redirect()->route('admin.users.index');
    }

    public function update(Request $request, User $user)
    {
        $user->fill($request->except(['password', 'confirm_password']));

        if ($request->filled('password')) {
            $user->password = Hash::make($validatedData['password']);
        }

        $user->save();

        flash('User updated successfully.', 'success');

        return redirect(route('admin.users.index'));
    }
}
