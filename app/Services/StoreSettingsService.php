<?php

namespace App\Services;

use App\Models\StoreSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StoreSettingsService
{
    public function getSettings()
    {
        $settings = StoreSetting::all();

        $settingsArray = [];

        foreach ($settings as $key => $settings) {
            $settingsArray[$settings['settings_key']] = $settings['settings_value'];
        }

        return $settingsArray;
    }

    public function updateSettings($settingsData)
    {
        //dd($settingsData);

        foreach ($settingsData as $key => $value) {
            StoreSetting::where('settings_key', $key)->update(['settings_value' => $value]);
        }
    }

    public function updateBasicInfo(Request $request)
    {
        if ($request->store_name) {
            $validatedData['store_name'] = $request->store_name;
        }

        if ($request->hasFile('store_image')) {
            $validatedData['store_image'] = Storage::disk('public')->putFile('store-image', $request->file('store_image'));
        }

        $this->updateSettings($validatedData);

        flash('Basic information updated successfully', 'success');

        return redirect()->route('admin.store-settings')->with('active_basic_details', 'basic');
    }

    public function updateContactDetails(Request $request)
    {
        if ($request->contact_person) {
            $validatedData['contact_person'] = $request->contact_person;
        }

        if ($request->contact_email) {
            $validatedData['contact_email'] = $request->contact_email;
        }

        if ($request->contact_number) {
            $validatedData['contact_number'] = $request->contact_number;
        }

        if ($request->contact_address) {
            $validatedData['contact_address'] = $request->contact_address;
        }

        $this->updateSettings($validatedData);

        flash('Contact details updated successfully', 'success');

        return redirect()->route('admin.store-settings')->with('active_settings', 'contact');
    }

    public function updateOtherSettings(Request $request)
    {
        if ($request->maintenance_mode) {
            $validatedData['maintenance_mode'] = $request->maintenance_mode;
        }

        if ($request->store_currency) {
            $validatedData['store_currency'] = $request->store_currency;
        }

        if ($request->timezone) {
            $validatedData['timezone'] = $request->timezone;
        }

        if ($request->currency_code_position) {
            $validatedData['currency_code_position'] = $request->currency_code_position;
        }

        if ($request->decimal_places) {
            $validatedData['decimal_places'] = $request->decimal_places;
        }

        if ($request->use_thousand_separators) {
            $validatedData['use_thousand_separators'] = $request->use_thousand_separators;
        }

        if ($request->thousand_separator) {
            $validatedData['thousand_separator'] = $request->thousand_separator;
        }

        if ($request->decimal_separator) {
            $validatedData['decimal_separator'] = $request->decimal_separator;
        }

        if ($request->footer_text) {
            $validatedData['footer_text'] = $request->footer_text;
        }

        if ($request->comments_plugin_app_id) {
            $validatedData['comments_plugin_app_id'] = $request->comments_plugin_app_id;
        }

        if ($request->google_analytics) {
            $validatedData['google_analytics'] = $request->google_analytics;
        }

        if ($request->zopim_chat) {
            $validatedData['zopim_chat'] = $request->zopim_chat;
        }

        $this->updateSettings($validatedData);

        flash('Settings updated successfully', 'success');

        return redirect()->route('admin.store-settings')->with('active_settings', 'other');
    }

    public function getStoreSettings()
    {
        $storeSettings = StoreSetting::all();
        $data = [];
        foreach ($storeSettings as $storeSetting) {
            if ('store_image' == $storeSetting->settings_key) {
                $data += [$storeSetting->settings_key => asset('storage/'.$storeSetting->settings_value)];
            } else {
                $data += [$storeSetting->settings_key => $storeSetting->settings_value];
            }
        }

        return [
            'status' => 'success',
            'data' => $data,
        ];
    }
}
