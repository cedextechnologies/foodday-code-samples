<?php

namespace App\Services;

use Validator;
use App\Models\Cart;
use App\Models\Order;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Payment;
use PayPal\Api\Transaction;
use PayPal\Rest\ApiContext;
use Illuminate\Http\Request;
use PayPal\Api\RedirectUrls;
use PayPal\Api\PaymentExecution;
use Illuminate\Support\Facades\URL;
use PayPal\Auth\OAuthTokenCredential;

class PaypalPaymentService
{
    protected $_api_context;

    public function __construct()
    {
        $paypal_conf = \Config::get('paypal');
        $this->_api_context = new ApiContext(new OAuthTokenCredential(
            $paypal_conf['client_id'],
            $paypal_conf['secret']));

        $this->_api_context->setConfig($paypal_conf['settings']);
    }

    public function getPaypalUrl($request, $token, $successUrl, $cancelUrl)
    {
        $order = Order::where('payment_token', $token)->first();

        if ($request->is('api/*')) {
            $validator = Validator::make($request->all(), [
                'redirect_url' => 'required|url',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => 'error',
                    'data' => $validator->errors()->toArray(),
                ], 422);
            }

            $order->update([
                'redirect_url' => request('redirect_url'),
            ]);
        } else {
            $order->update([
                'redirect_url' => route('customer.order.confirmation', $order),
            ]);
        }

        $order = Order::where('payment_token', $token)->first();

        if (empty($order)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid payment token',
                ],
            ]);
        }

        $payer = new Payer();
        $payer->setPaymentMethod('paypal');
        $amount = new Amount();
        $amount->setCurrency('USD')
        ->setTotal($order->payed_amount);
        $transaction = new Transaction();
        $transaction->setAmount($amount)
        ->setDescription('Grand Total : '.format_currency($order->payed_amount));
        $redirect_urls = new RedirectUrls();
        $redirect_urls->setReturnUrl(url($successUrl))
        ->setCancelUrl(url($cancelUrl));
        $payment = new Payment();
        $payment->setIntent('Sale')
        ->setPayer($payer)
        ->setRedirectUrls($redirect_urls)
        ->setTransactions([$transaction]);

        try {
            $payment->create($this->_api_context);
        } catch (\PayPal\Exception\PPConnectionException $ex) {
            $order->update(['status' => 2]);

            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Payment is not successfull.',
                ],
            ]);
        }

        foreach ($payment->getLinks() as $link) {
            if ('approval_url' == $link->getRel()) {
                $redirect_url = $link->getHref();
                break;
            }
        }

        if (isset($redirect_url)) {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'message' => 'Payment URL generated successfully',
                    'redirect_url' => $redirect_url,
                ],
            ]);
        }

        return response()->json([
            'status' => 'error',
            'data' => [
                'message' => 'Payment failed',
            ],
        ]);
    }

    public function makePayment($request, $token)
    {
        $order = Order::where('payment_token', $token)->first();

        if (empty($order)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid payment token',
                ],
            ]);
        }

        $payment_id = request('paymentId');

        if (empty(request('PayerID')) || empty(request('token'))) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Payment failed',
                ],
            ]);
        }

        $payment = Payment::get($payment_id, $this->_api_context);
        $execution = new PaymentExecution();
        $execution->setPayerId(request('PayerID'));
        /**Execute the payment **/
        $result = $payment->execute($execution, $this->_api_context);

        if ('approved' == $result->getState()) {
            $order->update(['payment_status' => 1]);

            $cart = Cart::where('order_reference', $order->order_reference)->first();

            if ($cart) {
                $request->session()->forget('web_cart_token');
                $cart->delete();
            }
        }

        if (1 != $order->payment_status) {
            $order->update(['status' => 2]);
        }

        return redirect()->away($order->redirect_url.'?order_reference='.$order->order_reference);
    }

    public function cancelPayment($request, $token)
    {
        $order = Order::where('payment_token', $token)->first();

        if (1 != $order->payment_status) {
            $order->update(['status' => 2]);
        }

        return redirect()->away($order->redirect_url.'?status=0');
    }
}
