<?php

namespace App\Services;

use Validator;
use Illuminate\Support\Facades\Auth;

class AuthService
{
    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login($request, $guard)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 422);
        }

        $credentials = request(['email', 'password']);

        if (! $token = Auth::guard($guard)->attempt($credentials)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Unauthorized',
                ],
            ], 401);
        }

        if ('customer' == $guard) {
            $customer = Auth::guard('customer')->user();
            if (! $customer->isActive()) {
                return response()->json([
                    'status' => 'error',
                    'data' => [
                        'message' => 'account blocked',
                    ],
                ], 401);
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'access_token' => $token,
            ],
        ]);
    }

    public function logout($guard)
    {
        Auth::guard($guard)->logout();

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Successfully logged out',
            ],
        ]);
    }
}
