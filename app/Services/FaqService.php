<?php

namespace App\Services;

use App\Models\Faq;
use Illuminate\Http\Request;

class FaqService
{
    public function filter(Request $request)
    {
        $users = Faq::when($request->input('search'),
            function ($query) use ($request) {
                $query->where('title', 'like', '%'.$request->search.'%')
                ->orWhere('description', $request->input('search'));
            })->orderBy('created_at', 'desc');

        return $users;
    }

    public function create(Request $request)
    {
        $faq = new Faq();

        $faq->title = $request->input('title');
        $faq->description = $request->input('description');
        $faq->status = $request->input('status');

        $faq->save();

        flash('Faq created successfully.', 'success');

        return redirect()->route('admin.faqs.index');
    }

    public function update(Request $request, Faq $faq)
    {
        $faq->fill($request->all())->save();
        flash('FAQ updated successfully.', 'success');

        return redirect(route('admin.faqs.index'));
    }
}
