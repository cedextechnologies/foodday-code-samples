<?php

namespace App\Services;

use Carbon\Carbon;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponService
{
    public function filter(Request $request)
    {
        $coupons = Coupon::when($request->input('search'),
            function ($query) use ($request) {
                $query->where('code', $request->input('search'))
                    ->orWhere('discount', $request->input('search'));
            })->orderBy('created_at', 'desc');

        return $coupons;
    }

    public function create(Request $request)
    {
        $coupon = new Coupon();

        $coupon->fill($request->except(['start_date', 'end_date']));

        if ($request->has('start_date') && ! is_null($request->start_date)) {
            $coupon->start_date = Carbon::parse($request->start_date);
        }

        if ($request->has('end_date') && ! is_null($request->end_date)) {
            $coupon->end_date = Carbon::parse($request->end_date);
        }
        $coupon->used_count = 0;

        $coupon->save();

        flash('Coupon created successfully.', 'success');

        return redirect()->route('admin.coupons.index');
    }

    public function update(Request $request, $coupon)
    {
        $coupon->fill($request->except(['start_date', 'end_date']));

        if ($request->has('start_date') && ! is_null($request->start_date)) {
            $coupon->start_date = Carbon::parse($request->start_date);
        }

        if ($request->has('end_date') && ! is_null($request->end_date)) {
            $coupon->end_date = Carbon::parse($request->end_date);
        }

        $coupon->save();

        flash('Coupon updated successfully.', 'success');

        return redirect(route('admin.coupons.index'));
    }
}
