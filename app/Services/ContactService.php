<?php

namespace App\Services;

use Validator;
use Illuminate\Http\Request;
use App\Mail\ContactReceived;
use Illuminate\Support\Facades\Mail;

class ContactService
{
    public function contact(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'email' => 'required|email',
            'message' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 'error',
                'data' => $validator->errors()->toArray(),
            ], 200);
        }

        Mail::to(env('CONTACT_TO_MAIL'))->send(new ContactReceived($request->all()));

        return response()->json([
            'status' => 'success',
            'data' => [
                'message' => 'Contact message sent successfully',
            ],
        ]);
    }
}
