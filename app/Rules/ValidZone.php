<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class ValidZone implements Rule
{
    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $zonePoints = explode(',', $value);
        if (count($zonePoints) > 3) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Please select a zone';
    }
}
