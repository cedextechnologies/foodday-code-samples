<?php

namespace App\Rules;

use Carbon\Carbon;
use App\Models\PasswordReset;
use Illuminate\Contracts\Validation\Rule;

class ValidResetToken implements Rule
{
    /**
     * Create a new rule instance.
     */
    public function __construct()
    {
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed  $value
     *
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $token = PasswordReset::where('token', $value)->first();

        return ! is_null($token) && ! Carbon::parse($token->created_at)->lt(Carbon::parse(now())->subHour(24));
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Invalid otp';
    }
}
