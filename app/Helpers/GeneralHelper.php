<?php

use App\Models\Coupon;
use App\Models\Currency;
use App\Models\TimeZone;
use App\Models\StoreSetting;

function getVersion()
{
    return '1.23';
}

/*
 * Format the delivery zone.
 *
 * @param  string $param
 * @return string
 */
if (! function_exists('formatDeliveryZone')) {
    function formatDeliveryZone($param)
    {
        $polygon = explode('<br>', $param);
        for ($i = 0; $i < sizeof($polygon) - 1; ++$i) {
            $explode_poly[$i] = explode(',', $polygon[$i]);
        }
        $endpoint = implode(' ', $explode_poly[0]);
        $poly_array = implode(',', array_map('implode', array_fill(0, count($explode_poly), ' '), $explode_poly));
        $coordinates = $poly_array.','.$endpoint;

        return $coordinates;
    }
}

/*
 * Format the currency value.
 *
 * @param float $value
 *
 * @return string
 */
if (! function_exists('format_currency')) {
    function format_currency($value, $with_currency = true)
    {
        $settings = getSettings();

        $decimal_places = $settings['decimal_places'];
        $decimal_separators = ('1' == $settings['decimal_separator']) ? '.' : ',';
        if (1 == $settings['use_thousand_separators']) {
            $thousand_seperators = ('1' == $settings['thousand_separator']) ? '.' : ',';
        } else {
            $thousand_seperators = '';
        }

        $value = number_format($value, $decimal_places, $decimal_separators, $thousand_seperators);

        if (! $with_currency) {
            return $value;
        }

        $currency_symbol = Currency::where('id', $settings['store_currency'])->pluck('symbol')->first();

        return ('left' == $settings['currency_code_position']) ? $currency_symbol.' '.$value : $value.' '.$currency_symbol;
    }
}

/*
 * Get currency symbol.
 *
 * @return array
 */
if (! function_exists('getCurrencySymbol')) {
    function getCurrencySymbol()
    {
        return Currency::where('id', getStoreSettings('store_currency'))->pluck('symbol')->first();
    }
}

/*
 * Get store settings.
 *
 * @return array
 */
if (! function_exists('getSettings')) {
    function getSettings()
    {
        $settings = StoreSetting::all();

        $settingsArray = [];

        foreach ($settings as $key => $settings) {
            $settingsArray[$settings['settings_key']] = $settings['settings_value'];
        }

        return $settingsArray;
    }
}

/*
 * Format date.
 *
 * @param string $date
 *
 * @return string
 */
if (! function_exists('getDateString')) {
    function getDateString($date)
    {
        if (getFormatedDate($date) == getToday()) {
            return 'Today';
        } elseif (getFormatedDate($date) == getYesterday()) {
            return 'Yesterday';
        } else {
            return getFormatedDate($date);
        }
    }
}

/*
 * Format time.
 *
 * @param string $date
 *
 * @return string
 */
if (! function_exists('getTimeString')) {
    function getTimeString($date)
    {
        return \Carbon\Carbon::parse($date)->format('h:i A');
    }
}

/*
 * Format date without day string.
 *
 * @param string $date
 *
 * @return string
 */
if (! function_exists('getFormatedDate')) {
    function getFormatedDate($date)
    {
        return \Carbon\Carbon::parse($date)->format('d M, Y');
    }
}

/*
 * Get Today.
 *
 * @return string
 */
if (! function_exists('getToday')) {
    function getToday()
    {
        return \Carbon\Carbon::today()->format('d M, Y');
    }
}

/*
 * Get Yesterday.
 *
 * @return string
 */
if (! function_exists('getYesterday')) {
    function getYesterday()
    {
        return \Carbon\Carbon::yesterday()->format('d M, Y');
    }
}

/*
 * Get Store setting.
 *
 * @param string $key
 *
 * @return string
 */
if (! function_exists('getStoreSettings')) {
    function getStoreSettings($settingKey)
    {
        $storeSetting = StoreSetting::where('settings_key', $settingKey)->pluck('settings_value')->first();
        
        if($storeSetting)
            return $storeSetting;
    }
}

/*
 * Get Coupon string.
 *
 * @param string $code
 *
 * @return string
 */
if (! function_exists('getCouponString')) {
    function getCouponString($code)
    {
        $coupon = Coupon::where('code', $code)->first();

        return 1 == $coupon->discount_type ? $coupon->discount.' % OFF' : 'Flat '.format_currency($coupon->discount).' OFF';
    }
}

/*
 * Get Coupon string.
 *
 * @param string $code
 *
 * @return string
 */
if (! function_exists('getCoordinates')) {
    function getCoordinates($zipcode)
    {
        $coordinates = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?key='.env('GOOGLE_MAP_KEY').'&address='.$zipcode.'&sensor=true');
        $coordinates = json_decode($coordinates);
        if (! empty($coordinates->results)) {
            $latitude = $coordinates->results[0]->geometry->location->lat;
            $longitude = $coordinates->results[0]->geometry->location->lng;

            return [
                'latitude' => $latitude,
                'longitude' => $longitude,
            ];
        }

        return [
            'latitude' => '',
            'longitude' => '',
        ];
    }
}

if (! function_exists('getDayString')) {
    function getDayString($day)
    {
        switch ($day) {
            case 1: return 'Monday';
            break;
            case 2: return 'Tuesday';
            break;
            case 3: return 'Wednesday';
            break;
            case 4: return 'Thursday';
            break;
            case 5: return 'Friday';
            break;
            case 6: return 'Saturday';
            break;
            case 7: return 'Sunday';
            break;
        }
    }
}

if (! function_exists('getTimeZone')) {
    function getTimeZone()
    {
        $zone = getStoreSettings('timezone');

        $timeZone = TimeZone::where('id', $zone)->first();

        if($timeZone)
            return $timeZone->code;
        
    }
}
