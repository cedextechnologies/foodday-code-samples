<?php

namespace App\Traits;

use App\Models\Coupon;
use App\Models\DeliveryZone;
use App\Models\ItemModifierDetail;
use Illuminate\Http\Request;

trait CartTrait
{
    /**
     * Get total amount of items in cart.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getCartTotals($items, $checkoutType = null)
    {
        $subTotal = 0.0;
        $itemAamount = 0.0;
        $modifierAmount = 0.0;

        foreach ($items as $item) {
            $modifierPrice = 0.0;

            if (! empty($item->size_id)) {
                $itemPrice = $item->size->price;
            } else {
                $itemPrice = $item->item->price;
            }

            if ($item->modifiers->isNotEmpty()) {
                $modifierPrice = ItemModifierDetail::whereIn('id', $item->modifiers->pluck('item_modifier_detail_id'))->sum('price');
            }

            $itemTotal = ($itemPrice * $item->quantity) + ($modifierPrice * $item->quantity);

            $subTotal += $itemTotal;
        }

        $data['subtotal'] = $subTotal;

        $data['tax'] = getStoreSettings('tax');

        $data['tax_amount'] = ($subTotal * $data['tax']) / 100;

        $data['delivery_charge'] = ($checkoutType == '1') ? $this->calculateDeliveryCharge($subTotal) : '0';

        $data['order_amount_with_tax'] = $subTotal + (($subTotal * $data['tax']) / 100);

        $data['order_total'] = $data['order_amount_with_tax'] + $data['delivery_charge'];

        return $data;
    }


    public function calculateDeliveryCharge($subTotal)
    {
        $deliveryZone = DeliveryZone::find(session('deliveryZone'));

        $customDeliveryFees = $deliveryZone->customDeliveryFees()
                                ->where('order_amount', '<=', $subTotal)
                                ->orderBy('order_amount', 'DESC')
                                ->first();
    
        if($customDeliveryFees)                        
            return $customDeliveryFees->delivery_fee_amount;
        else
            return ($deliveryZone->default_delivery_charge) ? $deliveryZone->default_delivery_charge : 0;
    }


    /**
     * Get total amount of an item in cart.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function getItemTotals($cartItem)
    {
        $modifierPrice = 0.0;

        if (! empty($cartItem->size_id)) {
            $itemPrice = $cartItem->size->price;
        } else {
            $itemPrice = $cartItem->item->price;
        }

        if ($cartItem->modifiers->isNotEmpty()) {
            $modifierPrice = ItemModifierDetail::whereIn('id', $cartItem->modifiers->pluck('item_modifier_detail_id'))->sum('price');
        }

        $itemTotal = ($itemPrice * $cartItem->quantity) + ($modifierPrice * $cartItem->quantity);

        return $itemTotal;
    }

    public function getCouponDiscount($cart, $total)
    {
        if (! empty($cart->code)) {
            $today = date('Y-m-d');

            $coupon = Coupon::where('code', $cart->code)->first();

            if (! $coupon->isActive()) {
                return 0;
            }

            if ($today < $coupon->start_date) {
                return 0;
            }

            if ($today > $coupon->end_date) {
                return 0;
            }

            // Percentage
            if (1 == $coupon->discount_type) {
                return  $total * ($coupon->discount / 100);
            }
            // Fixed amount
            else {
                return $coupon->discount;
            }
        }

        return 0;
    }
}
