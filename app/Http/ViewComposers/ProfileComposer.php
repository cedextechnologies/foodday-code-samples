<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class ProfileComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        if (! Auth::check()) {
            return true;
        }

        $user = Auth::user();

        $view->with([
            'user' => $user,
        ]);
    }
}
