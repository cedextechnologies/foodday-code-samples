<?php

namespace App\Http\ViewComposers;

use App\Models\StoreHour;
use Illuminate\View\View;

class FooterComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $view->with([
            'storeHours' => StoreHour::all(),
        ]);
    }
}
