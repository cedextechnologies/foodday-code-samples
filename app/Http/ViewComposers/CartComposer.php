<?php

namespace App\Http\ViewComposers;

use App\Models\Cart;
use Illuminate\View\View;

class CartComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        $token = session()->get('web_cart_token', 'default');

        $cart = Cart::where('token', $token)->first();

        if ($cart) {
            $items = $cart->items->count();

            $view->with([
                'cartItemsCount' => $items,
            ]);
        }
    }
}
