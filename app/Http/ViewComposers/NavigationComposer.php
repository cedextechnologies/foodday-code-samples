<?php

namespace App\Http\ViewComposers;

use Auth;
use Illuminate\View\View;

class NavigationComposer
{
    /**
     * Bind data to the view.
     *
     * @param View $view
     */
    public function compose(View $view)
    {
        if (! Auth::check()) {
            return true;
        }

        $user = Auth::user();

        $view->with([
            'user' => $user,
            'navigations' => $this->navigationMenus(),
        ]);
    }

    public function navigationMenus()
    {
        $user = Auth::user();

        return [
            [
                'label' => 'Dashboard',
                'icon' => 'fas fa-tachometer-alt',
                'url' => route('admin.dashboard'),
            ],
            [
                'label' => 'Orders',
                'icon' => 'fas fa-shopping-cart',
                'url' => route('admin.orders'),
            ],
            [
                'label' => 'Customers',
                'icon' => 'fas fa-users',
                'url' => route('admin.customers'),
            ],
            [
                'label' => 'Reservations',
                'icon' => 'fas fa-clock',
                'url' => route('admin.reservations'),
            ],
            [
                'label' => 'Food Section',
                'icon' => 'fas fa-bars',
                'url' => '#',
                'subMenus' => [
                    [
                        'label' => 'Cuisines',
                        'url' => route('admin.item-categories.index'),
                    ],
                    [
                        'label' => 'Items',
                        'url' => route('admin.items.index'),
                    ],
                ],
            ],
            [
                'label' => 'Admin Users',
                'icon' => 'fas fa-users',
                'url' => route('admin.users.index'),
            ],
            [
                'label' => 'Coupons',
                'icon' => 'fas fa-address-card',
                'url' => route('admin.coupons.index'),
            ],
            [
                'label' => 'Common Modifiers',
                'icon' => 'fas fa-bars',
                'url' => route('admin.common-modifiers.index'),
            ],
            // [
            //     'label' => 'Cities',
            //     'icon' => 'fas fa-map',
            //     'url' => route('admin.cities.index'),
            // ],
            [
                'label' => 'Delivery Zones',
                'icon' => 'fas fa-map',
                'url' => route('admin.zones.index'),
            ],
            [
                'label' => 'Settings',
                'icon' => 'fas fa-cogs',
                'url' => '#',
                'subMenus' => [
                    [
                        'label' => 'Store Settings',
                        'url' => route('admin.store-settings'),
                    ],
                    [
                        'label' => 'Order Settings',
                        'url' => route('admin.order-settings'),
                    ],
                    [
                        'label' => 'Working Hours',
                        'url' => route('admin.working-hours'),
                    ],
                    [
                        'label' => 'Social Media',
                        'url' => route('admin.social-medias'),
                    ],
                    // [
                    //     'label' => 'Email Settings',
                    //     'url' => route('admin.email-settings'),
                    // ],
                                        [
                        'label' => 'Payment Settings',
                        'url' => route('admin.payment-settings'),
                    ],
                    // [
                    //     'label' => 'SMS Settings',
                    //     'url' => route('admin.sms-settings'),
                    // ],
                ],
            ],
            [
                'label' => 'Faq',
                'icon' => 'fas fa-question',
                'url' => route('admin.faqs.index'),
            ],
            // [
            //     'label' => 'Customer Reviews',
            //     'icon' => 'fas fa-star',
            //     'url' => route('admin.reviews'),
            // ],
        ];
    }
}
