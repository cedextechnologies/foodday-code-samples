<?php

namespace App\Http\Controllers\Admin;

use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Services\CouponService;
use App\Http\Requests\CouponForm;
use App\Http\Controllers\Controller;

class CouponController extends Controller
{
    public function __construct(CouponService $couponService)
    {
        $this->couponService = $couponService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.coupons.index', [
            'coupons' => $this->couponService->filter($request)->get(),
        ]);
    }

    public function create()
    {
        return view('admin.coupons.add');
    }

    public function store(CouponForm $request)
    {
        return $this->couponService->create($request);
    }

    public function edit(Coupon $coupon)
    {
        return view('admin.coupons.edit', ['coupon' => $coupon]);
    }

    public function destroy(Coupon $coupon)
    {
        $coupon->delete();

        flash('Coupon removed successfully.', 'success');

        return redirect()->route('admin.coupons.index');
    }

    public function update(CouponForm $request, Coupon $coupon)
    {
        return $this->couponService->update($request, $coupon);
    }

    /**
     * update status of coupon.
     *
     * @param \Illuminate\Http\Request $request,Coupon $coupon
     *
     * @return json array
     */
    public function updateStatus(Request $request, Coupon $coupon)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $coupon->update($validatedData);

        return response()->json(['status' => 'success']);
    }
}
