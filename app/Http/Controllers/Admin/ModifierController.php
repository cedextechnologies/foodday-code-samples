<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\Item;
use App\Models\ItemModifier;
use Illuminate\Http\Request;
use App\Services\ModifierService;
use App\Http\Controllers\Controller;

class ModifierController extends Controller
{
    public function __construct(ModifierService $modifierService)
    {
        $this->modifierService = $modifierService;
    }

    public function create(Item $item)
    {
        $renderedHtml = view('admin.items.add_modifier', compact('item'))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function store(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'minimum' => 'required|numeric|min:0',
            'maximum' => 'required|numeric|min:0',
            'item_name.*' => 'required|min:1',
            'item_price.*' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->modifierService->store($request, $item);
    }

    public function edit(Item $item, ItemModifier $modifier)
    {
        $renderedHtml = view('admin.items.edit_modifier', compact('modifier', 'item'))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function update(Request $request, Item $item, ItemModifier $modifier)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'minimum' => 'required|numeric|min:0',
            'maximum' => 'required|numeric|min:0',
            'item_name.*' => 'required|min:1',
            'item_price.*' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->modifierService->update($request, $item, $modifier);
    }

    public function updateStatus(Request $request, Item $item)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $item->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(Item $item, ItemModifier $modifier)
    {
        $modifier->delete();

        flash('Well Done! Modifier deleted successfully', 'success');

        return redirect()->route('admin.items.edit', [$item]);
    }
}
