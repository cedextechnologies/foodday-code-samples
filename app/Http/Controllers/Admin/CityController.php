<?php

namespace App\Http\Controllers\Admin;

use App\Models\City;
use Illuminate\Http\Request;
use App\Services\CityService;
use App\Http\Requests\CityForm;
use App\Http\Controllers\Controller;

class CityController extends Controller
{
    public function __construct(CityService $cityService)
    {
        $this->cityService = $cityService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.cities.index', [
            'cities' => $this->cityService->filter($request)->get(),
        ]);
    }

    public function create()
    {
        return view('admin.cities.add');
    }

    public function store(CityForm $request)
    {
        return $this->cityService->create($request);
    }

    public function edit(City $city)
    {
        return view('admin.cities.edit', ['city' => $city]);
    }

    public function destroy(City $city)
    {
        $city->delete();

        flash('City removed successfully.', 'success');

        return redirect()->route('admin.cities.index');
    }

    public function update(CityForm $request, City $city)
    {
        return $this->cityService->update($request, $city);
    }

    /**
     * update status of city.
     *
     * @param \Illuminate\Http\Request $request,City $city
     *
     * @return json array
     */
    public function updateStatus(Request $request, City $city)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $city->update($validatedData);

        return response()->json(['status' => 'success']);
    }
}
