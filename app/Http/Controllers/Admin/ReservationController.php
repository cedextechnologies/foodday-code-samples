<?php

namespace App\Http\Controllers\Admin;

use App\Models\Reservation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReservationService;
use App\Http\Requests\ReservationForm;

class ReservationController extends Controller
{
    public function __construct(ReservationService $reservationService)
    {
        $this->reservationService = $reservationService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.reservations.index', [
            'reservations' => $this->reservationService->filter($request)->paginate(10),
        ]);
    }

    public function edit(Request $request, Reservation $reservation)
    {
        return view('admin.reservations.edit', [
            'reservation' => $reservation,
        ]);
    }

    public function update(ReservationForm $request, Reservation $reservation)
    {
        return $this->reservationService->update($request, $reservation);
    }

    public function updateStatus(Request $request, Reservation $reservation)
    {
        return $this->reservationService->updateStatus($request, $reservation);
    }

    public function destroy(Reservation $Reservation)
    {
        $Reservation->delete();

        flash('Reservation removed successfully.', 'success');

        return redirect()->route('admin.reservations');
    }
}
