<?php

namespace App\Http\Controllers\Admin;

use App\Models\DeliveryZone;
use App\Http\Controllers\Controller;
use App\Services\DeliveryZoneService;
use App\Http\Requests\DeliveryZoneForm;
use Illuminate\Http\Request;

class DeliveryZoneController extends Controller
{
    public function __construct(DeliveryZoneService $deliveryZoneService)
    {
        $this->deliveryZoneService = $deliveryZoneService;
    }

    public function index()
    {
        return view('admin.delivery-zone.index', [
            'zones' => DeliveryZone::orderBy('id', 'desc')->get(),
        ]);
    }

    public function create()
    {
        return view('admin.delivery-zone.create');
    }

    public function store(DeliveryZoneForm $request)
    {
        return $this->deliveryZoneService->store($request);
    }

    public function destroy(DeliveryZone $zone)
    {
        $zone->delete();

        flash('Well Done! delivery zone deleted successfully', 'success');

        return redirect()->route('admin.zones.index');
    }

    public function getCustomeDeliveryFee(DeliveryZone $zone)
    {
        $customeDeliveryFees = $zone->customDeliveryFees;
        return view('admin.delivery-zone.custome-delivery-fees.index', compact(['customeDeliveryFees','zone']));
    }

    public function storeCustomeDeliveryFee(DeliveryZone $zone, Request $request)
    {
        $this->deliveryZoneService->storeCustomeDeliveryFee($request, $zone);
    }
}
