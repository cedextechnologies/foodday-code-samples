<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StoreSettingsService;
use App\Services\PaymentSettingsService;
use Illuminate\Support\Facades\Validator;

class PaymentSettingsController extends Controller
{
    public function __construct(PaymentSettingsService $paymentSettingsService, StoreSettingsService $storeSettingsService)
    {
        $this->paymentSettingsService = $paymentSettingsService;
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.payment-settings.index', [
            'settings' => $this->storeSettingsService->getSettings(),
        ]);
    }

    public function updatePaymentSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'accept_cash' => 'sometimes|in:on,off',
                'stripe_payment' => 'sometimes|in:on,off',
                'braintree_payment' => 'sometimes|in:on,off',
                'paypal_payment' => 'sometimes|in:on,off',
                'checkout_delivery' => 'sometimes|in:on,off',
                'checkout_carry_out' => 'sometimes|in:on,off',
                'checkout_dine_in' => 'sometimes|in:on,off',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_basic_details', 'payment');
        }

        return $this->paymentSettingsService->updatePaymentSettings($request);
    }

    public function updateStripeSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'stripe_secret_key' => 'sometimes|max:255',
                'stripe_publishable_key' => 'sometimes|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_basic_details', 'stripe');
        }

        return $this->paymentSettingsService->updateStripeSettings($request);
    }

    public function updateBraintreeSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'braintree_environment' => 'sometimes|in:1,2',
                'marchant_id' => 'sometimes|max:255',
                'braintree_public_key' => 'sometimes|max:255',
                'private_key' => 'sometimes|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_basic_details', 'braintree');
        }

        return $this->paymentSettingsService->updateBraintreeSettings($request);
    }

    public function updatePaypalSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'paypal_environment' => 'sometimes|in:1,2',
                'paypal_cient_id' => 'sometimes|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_basic_details', 'braintree');
        }

        return $this->paymentSettingsService->updatePaypalSettings($request);
    }
}
