<?php

namespace App\Http\Controllers\Admin;

use App\Models\Review;
use Illuminate\Http\Request;
use App\Services\ReviewService;
use App\Http\Requests\ReviewForm;
use App\Http\Controllers\Controller;

class ReviewController extends Controller
{
    public function __construct(ReviewService $reviewService)
    {
        $this->reviewService = $reviewService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.reviews.index', [
            'reviews' => $this->reviewService->filter($request)->get(),
        ]);
    }

    public function edit(Request $request, Review $review)
    {
        return view('admin.reviews.edit', [
            'review' => $review,
        ]);
    }

    public function update(ReviewForm $request, Review $review)
    {
        return $this->reviewService->update($request, $review);
    }

    public function updateStatus(Request $request, Review $review)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $review->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(Review $review)
    {
        $review->delete();

        flash('Review removed successfully.', 'success');

        return redirect()->route('admin.reviews');
    }
}
