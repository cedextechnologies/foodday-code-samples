<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\SmsSettingsService;
use App\Http\Requests\SmsSettingsForm;
use App\Services\StoreSettingsService;

class SmsSettingsController extends Controller
{
    public function __construct(SmsSettingsService $smsSettingsService, StoreSettingsService $storeSettingsService)
    {
        $this->smsSettingsService = $smsSettingsService;
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.sms-settings.index', [
            'settings' => $this->storeSettingsService->getSettings(),
        ]);
    }

    public function update(SmsSettingsForm $request)
    {
        return $this->smsSettingsService->update($request);
    }
}
