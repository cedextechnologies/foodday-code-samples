<?php

namespace App\Http\Controllers\Admin;

use Validator;
use App\Models\Item;
use App\Models\Size;
use App\Models\ItemSize;
use Illuminate\Http\Request;
use App\Services\ItemSizeService;
use App\Http\Controllers\Controller;

class ItemSizeController extends Controller
{
    public function __construct(ItemSizeService $itemSizeService)
    {
        $this->itemSizeService = $itemSizeService;
    }

    public function create(Item $item)
    {
        $sizes = Size::all();

        $renderedHtml = view('admin.items.add_size', compact('item', 'sizes'))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function store(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            'size' => 'required|exists:sizes,id',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->itemSizeService->store($request, $item);
    }

    public function edit(Item $item, ItemSize $size)
    {
        $sizes = Size::all();

        $renderedHtml = view('admin.items.edit_size', compact('item', 'size', 'sizes'))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function update(Request $request, Item $item, ItemSize $size)
    {
        $validator = Validator::make($request->all(), [
            'size' => 'required|exists:sizes,id',
            'price' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->itemSizeService->update($request, $item, $size);
    }

    public function destroy(Item $item, ItemSize $size)
    {
        $size->delete();

        flash('Well Done! Size deleted successfully', 'success');

        return redirect()->route('admin.items.edit', [$item]);
    }
}
