<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\WorkingHoursService;
use App\Http\Requests\WorkingHoursForm;

class workingHoursController extends Controller
{
    public function __construct(WorkingHoursService $workingHoursService)
    {
        $this->workingHoursService = $workingHoursService;
    }

    public function index()
    {
        return view('admin.working-hours.index', [
            'workingHours' => $this->workingHoursService->getWorkingHours(),
        ]);
    }

    public function update(WorkingHoursForm $request)
    {
        return $this->workingHoursService->update($request);
    }
}
