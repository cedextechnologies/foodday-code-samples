<?php

namespace App\Http\Controllers\Admin;

use App\Models\ItemCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ItemCategoryService;
use App\Http\Requests\ItemCategoryForm;

class ItemCategoryController extends Controller
{
    public function __construct(ItemCategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        return view('admin.item-category.index', [
            'categories' => ItemCategory::orderBy('id', 'desc')->get(),
        ]);
    }

    public function create()
    {
        return view('admin.item-category.create');
    }

    public function store(ItemCategoryForm $request)
    {
        return $this->categoryService->store($request);
    }

    public function edit(ItemCategory $itemCategory)
    {
        return view('admin.item-category.edit', [
            'category' => $itemCategory,
        ]);
    }

    public function update(ItemCategoryForm $request, ItemCategory $itemCategory)
    {
        return $this->categoryService->update($request, $itemCategory);
    }

    public function updateStatus(Request $request, ItemCategory $itemCategory)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $itemCategory->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(ItemCategory $itemCategory)
    {
        $itemCategory->delete();

        flash('Well Done! Item category deleted successfully', 'success');

        return redirect()->route('admin.item-categories.index');
    }
}
