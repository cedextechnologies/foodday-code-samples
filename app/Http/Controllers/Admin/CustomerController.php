<?php

namespace App\Http\Controllers\Admin;

use Excel;
use App\Models\City;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Exports\CustomerExport;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index(Request $request, CustomerService $customerService)
    {
        $request->flash();

        return view('admin.customers.index', [
            'customers' => $customerService->filter($request)->paginate(20),
        ]);
    }

    public function edit(Customer $customer)
    {
        return view('admin.customers.edit', [
            'customer' => $customer,
            'cities' => City::all(),
        ]);
    }

    public function update(Request $request, Customer $customer)
    {
        $validatedData = $request->validate([
            'first_name' => 'sometimes|required|max:255',
            'last_name' => 'sometimes|required|max:255',
            'phone' => 'sometimes|required|max:255',
            'zipcode' => 'sometimes|required|max:255',
            'password' => 'sometimes|max:255',
            'email' => 'sometimes|bail|required|email|max:255|unique:customers,email,'.$customer->id,
            'status' => 'sometimes|required|in:1,0',
        ]);

        return $this->customerService->update($request, $customer);
    }

    /**
     * update status of customer.
     *
     * @param \Illuminate\Http\Request $request,Customer $customer
     *
     * @return json array
     */
    public function updateStatus(Request $request, Customer $customer)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $customer->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    /**
     * Export customers to excel.
     *
     * @param \App\Models\Lead $lead
     *
     * @return \Illuminate\Http\Response
     */
    public function export(Request $request)
    {
        $customers = $this->customerService->filter($request);

        $exportData = new CustomerExport($customers->get());

        if ($customers->count() > 0) {
            return Excel::download($exportData, 'customers.xlsx');
        } else {
            flash('No records found to export', 'error');

            return redirect()->back();
        }
    }
}
