<?php

namespace App\Http\Controllers\Admin;

use App\Models\Item;
use App\Models\ItemCategory;
use App\Models\ItemModifier;
use App\Models\ItemCommonModifier;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Http\Requests\ItemForm;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function index()
    {
        return view('admin.items.index', [
            'items' => Item::orderBy('id', 'desc')->with('itemCategory')->get(),
        ]);
    }

    public function create()
    {
        return view('admin.items.create', [
            'categories' => ItemCategory::active()->get(),
        ]);
    }

    public function store(ItemForm $request)
    {
        return $this->itemService->store($request);
    }

    public function edit(Item $item)
    {
        return view('admin.items.edit', [
            'item' => $item,
            'modifiers' => ItemModifier::where('item_id', $item->id)->whereNull('common_modifier_id')->with('ItemModifierDetails')->get(),
            'commonModifiers' => ItemModifier::where('item_id', $item->id)->whereNotNull('common_modifier_id')->with('ItemModifierDetails')->get(),
            'categories' => ItemCategory::active()->get(),
        ]);
    }

    public function update(ItemForm $request, Item $item)
    {
        return $this->itemService->update($request, $item);
    }

    public function updateStatus(Request $request, Item $item)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $item->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(Item $item)
    {
        $item->delete();

        flash('Well Done! Item category deleted successfully', 'success');

        return redirect()->route('admin.items.index');
    }
}
