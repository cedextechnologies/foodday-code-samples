<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Services\DashboardService;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    public function index()
    {
        $data = $this->dashboardService->getCardData();

        return view('admin.dashboard.index', [
            'cards' => $data,
            'orders' => Order::latest()->take(10)->get(),
        ]);
    }

    /**
     * Get data for graph.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return json array
     */
    public function graphData(Request $request)
    {
        $request->validate([
            'type' => 'sometimes|in:six-month,this-year,previous-year',
        ]);

        $data = $this->dashboardService->getGraphData($request);

        return response()->json(['status' => 1, 'data' => $data]);
    }
}
