<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Requests\UserForm;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    public function index(Request $request, UserService $userService)
    {
        $request->flash();

        return view('admin.users.index', [
            'users' => $userService->filter($request)->get(),
        ]);
    }

    public function create()
    {
        return view('admin.users.add');
    }

    public function store(UserForm $request)
    {
        return $this->userService->create($request);
    }

    public function edit(Request $request, User $user)
    {
        return view('admin.users.edit', [
            'adminUser' => $user,
        ]);
    }

    public function update(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'name' => 'required|max:255',
            'password' => 'max:255',
            'confirm_password' => 'same:password',
            'email' => 'bail|required|email|max:255|unique:users,email,'.$user->id,
            'status' => 'required|in:1,0',
        ]);

        return $this->userService->update($request, $user);
    }

    public function updateStatus(Request $request, User $user)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $user->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(User $user)
    {
        if (1 == $user->id) {
            abort(404);
        }

        $user->delete();

        flash('User removed successfully.', 'success');

        return redirect()->route('admin.users.index');
    }
}
