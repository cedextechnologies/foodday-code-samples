<?php

namespace App\Http\Controllers\Admin;

use App\Models\Order;
use App\Services\DashboardService;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = $this->dashboardService->getCardData();

        return view('admin.dashboard.index', [
            'cards' => $data,
            'orders' => Order::latest()->take(10)->get(),
        ]);
    }
}
