<?php

namespace App\Http\Controllers\Admin;

use App\Models\Currency;
use App\Models\TimeZone;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\StoreSettingsService;
use Illuminate\Support\Facades\Validator;

class StoreSettingsController extends Controller
{
    public function __construct(StoreSettingsService $storeSettingsService)
    {
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.store-settings.index', [
            'settings' => $this->storeSettingsService->getSettings(),
            'currencies' => Currency::all(),
            'timeZones' => TimeZone::all(),
        ]);
    }

    public function updateBasicInfo(Request $request)
    {
        $validator = Validator::make($request->all(), [
                'store_name' => 'required|max:255',
                'store_image' => 'sometimes|nullable|sometimes|image|max:2000',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_basic_details', 'basic');
        }

        return $this->storeSettingsService->updateBasicInfo($request);
    }

    public function updateContactDetails(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'contact_person' => 'required|max:255',
            'contact_number' => 'required|max:255',
            'contact_email' => 'required|max:255|email',
            'contact_address' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_settings', 'contact');
        }

        return $this->storeSettingsService->updateContactDetails($request);
    }

    public function updateOtherSettings(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'maintenance_mode' => 'required|in:"1","0"',
            'store_currency' => 'required|exists:currencies,id',
            'timezone' => 'required|exists:time_zones,id',
            'currency_code_position' => 'required|in:"1","0"',
            'decimal_places' => 'required|in:"2","3","4","5","6"',
            'use_thousand_separators' => 'required|in:1,2',
            'thousand_separator' => 'required|in:"1","2"',
            'decimal_separator' => 'required|in:"1","2"',
            'footer_text' => 'sometimes|max:255',
            'comments_plugin_app_id' => 'sometimes|max:255',
            'google_analytics' => 'sometimes|max:255',
            'zopim_chat' => 'sometimes|max:255',
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                        ->withErrors($validator)
                        ->withInput()
                        ->with('active_settings', 'other');
        }

        return $this->storeSettingsService->updateOtherSettings($request);
    }
}
