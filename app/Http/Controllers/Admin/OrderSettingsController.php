<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\OrderSettingsService;
use App\Services\StoreSettingsService;
use App\Http\Requests\OrderSettingsForm;

class OrderSettingsController extends Controller
{
    public function __construct(OrderSettingsService $orderSettingsService, StoreSettingsService $storeSettingsService)
    {
        $this->orderSettingsService = $orderSettingsService;
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.order-settings.index', [
            'settings' => $this->storeSettingsService->getSettings(),
        ]);
    }

    public function update(OrderSettingsForm $request)
    {
        return $this->orderSettingsService->update($request);
    }
}
