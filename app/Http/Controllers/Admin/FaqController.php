<?php

namespace App\Http\Controllers\Admin;

use App\Models\Faq;
use App\Services\FaqService;
use Illuminate\Http\Request;
use App\Http\Requests\FaqForm;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function __construct(FaqService $faqService)
    {
        $this->faqService = $faqService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.faqs.index', [
            'faqs' => $this->faqService->filter($request)->get(),
        ]);
    }

    public function create()
    {
        return view('admin.faqs.add');
    }

    public function store(FaqForm $request)
    {
        return $this->faqService->create($request);
    }

    public function edit(Request $request, Faq $faq)
    {
        return view('admin.faqs.edit', [
            'faq' => $faq,
        ]);
    }

    public function update(FaqForm $request, Faq $faq)
    {
        return $this->faqService->update($request, $faq);
    }

    /**
     * Change faq status.
     *
     * @param \Illuminate\Http\Request $request, Faq $faq
     *
     * @return json array
     */
    public function updateStatus(Request $request, Faq $faq)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:1,0',
        ]);

        $faq->update($validatedData);

        return response()->json(['status' => 'success']);
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();

        flash('FAQ removed successfully.', 'success');

        return redirect()->route('admin.faqs.index');
    }
}
