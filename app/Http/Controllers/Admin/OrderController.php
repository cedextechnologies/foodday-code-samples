<?php

namespace App\Http\Controllers\Admin;

use PDF;
use App\Models\Order;
use App\Mail\OrderStatus;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Requests\OrderForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class OrderController extends Controller
{
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function index(Request $request)
    {
        $request->flash();

        return view('admin.orders.index', [
            'orders' => $this->orderService->filter($request),
        ]);
    }

    public function edit(Request $request, Order $order)
    {
        return view('admin.orders.edit', [
            'order' => $order,
        ]);
    }

    public function update(OrderForm $request, Order $order)
    {
        return $this->orderService->update($request, $order);
    }

    public function updateStatus(Request $request, Order $order)
    {
        $validatedData = $request->validate([
            'status' => 'required|in:0,1,2,3,4',
        ]);

        $order->update($validatedData);

        Mail::to($order->customer->email)->send(new OrderStatus($order));

        flash('Order status updated.', 'success');

        return redirect()->back();
    }

    public function getReceipt(Order $order)
    {
        $pdf = PDF::loadView('customer.order.invoice', compact('order'));

        return $pdf->stream();
    }
}
