<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\SocialMediaService;
use App\Http\Requests\SocialMediaForm;
use App\Services\StoreSettingsService;

class SocialMediaController extends Controller
{
    public function __construct(SocialMediaService $socialMediaService, StoreSettingsService $storeSettingsService)
    {
        $this->socialMediaService = $socialMediaService;
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.social-medias.index', [
            'settings' => $this->storeSettingsService->getSettings(),
        ]);
    }

    public function update(SocialMediaForm $request)
    {
        return $this->socialMediaService->update($request);
    }
}
