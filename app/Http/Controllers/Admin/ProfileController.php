<?php

namespace App\Http\Controllers\Admin;

use Auth;
use App\Http\Requests\ProfileForm;
use App\Http\Requests\PasswordForm;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    public function index()
    {
        return view('admin.profile.index');
    }

    public function update(ProfileForm $request)
    {
        Auth::user()->update($request->all());

        flash('Well Done! Profile updated successfully.', 'success');

        return redirect()->route('admin.dashboard');
    }

    public function changePassword()
    {
        return view('admin.profile.password');
    }

    public function updatePassword(PasswordForm $request)
    {
        $user = Auth::user();

        $user->password = bcrypt($request->input('password'));

        $user->update();

        flash('Well Done! Password updated successfully.', 'success');

        return redirect()->route('admin.dashboard');
    }
}
