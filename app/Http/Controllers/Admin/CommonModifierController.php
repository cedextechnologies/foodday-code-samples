<?php

namespace App\Http\Controllers\Admin;

use App\Models\CommonModifier;
use App\Models\ItemModifier;
use App\Models\Item;
use Illuminate\Http\Request;
use App\Services\CommonModifierService;
use App\Http\Requests\CommonModifierForm;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class CommonModifierController extends Controller
{
    public function __construct(CommonModifierService $commonModifierService)
    {
        $this->commonModifierService = $commonModifierService;
    }

    public function index()
    {
        return view('admin.common-modifiers.index', [
            'modifiers' => CommonModifier::orderBy('id', 'desc')->get(),
        ]);
    }

    public function create()
    {
        return view('admin.common-modifiers.create');
    }

    public function store(CommonModifierForm $request)
    {
        return $this->commonModifierService->store($request);
    }

    public function edit(CommonModifier $commonModifier)
    {
        return view('admin.common-modifiers.edit', [
            'commonModifier' => $commonModifier,
            'modifier_details' => $commonModifier->CommonModifierDetails,
        ]);
    }

    public function update(CommonModifierForm $request, CommonModifier $commonModifier)
    {
        return $this->commonModifierService->updateToItem($request, $commonModifier);
    }


    public function destroy(CommonModifier $commonModifier)
    {
        $commonModifier->delete();

        flash('Well Done! common modifiers deleted successfully', 'success');

        return redirect()->route('admin.common-modifiers.index');
    }


    public function createToItem(Item $item)
    {
        $commonModifiers = CommonModifier::all(['id','title']);
        $renderedHtml = view('admin.items.common-modifiers.add', compact(['item', 'commonModifiers']))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function storeToItem(Request $request, Item $item)
    {
        $validator = Validator::make($request->all(), [
            'common_modifier' => 'required|exists:common_modifiers,id',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->commonModifierService->storeToItem($request, $item);
    }

    public function editToItem(Item $item, ItemModifier $itemModifier)
    {
        $commonModifiers = CommonModifier::all(['id','title']);
        $renderedHtml = view('admin.items.common-modifiers.edit', compact('itemModifier', 'commonModifiers', 'item'))->render();

        return response()->json(['status' => 1, 'html' => $renderedHtml]);
    }

    public function updateToItem(Request $request, Item $item, ItemModifier $itemModifier)
    {
        $validator = Validator::make($request->all(), [
            'common_modifier_id' => 'required|exists:common_modifiers,id',
            'item_name.*' => 'required|min:1',
            'item_price.*' => 'required|numeric',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'errors' => $validator->errors()->toArray(),
                'status' => 422,
            ], 200);
        }

        return $this->commonModifierService->updateToItem($request, $item, $itemModifier);
    }

    public function destroyToItem(Item $item, ItemModifier $itemModifier)
    {
        $itemModifier->delete();
        $itemModifier->itemModifierDetails()->delete();

        flash('Well Done! Modifier deleted successfully', 'success');

        return redirect()->route('admin.items.edit', [$item]);
    }



}
