<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\EmailSettingsService;
use App\Services\StoreSettingsService;
use App\Http\Requests\EmailSettingsForm;

class EmailSettingsController extends Controller
{
    public function __construct(EmailSettingsService $emailSettingsService, StoreSettingsService $storeSettingsService)
    {
        $this->emailSettingsService = $emailSettingsService;
        $this->storeSettingsService = $storeSettingsService;
    }

    public function index()
    {
        return view('admin.email-settings.index', [
            'settings' => $this->storeSettingsService->getSettings(),
        ]);
    }

    public function update(EmailSettingsForm $request)
    {
        return $this->emailSettingsService->update($request);
    }
}
