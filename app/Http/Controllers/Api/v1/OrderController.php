<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\OrderCollection;
use App\Http\Resources\Order as OrderResource;

class OrderController extends Controller
{
    public function index(Request $request)
    {
        $customer = Auth::guard('customer')->user();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'account not found',
                ],
            ]);
        }

        return new OrderCollection(Order::where('customer_id', $customer->id)->latest()->get());
    }

    public function show(Request $request, $order)
    {
        $order = Order::where('order_reference', $order)->first();

        if (empty($order)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Order not found',
                ],
            ]);
        }

        $customer = Auth::guard('customer')->user();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'account not found',
                ],
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'order' => new OrderResource($order),
            ],
        ]);
    }

    public function orderConfirmation(Request $request, $order)
    {
        $order = Order::where('order_reference', $order)->first();

        if (empty($order)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Order not found',
                ],
            ]);
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                'order' => new OrderResource($order),
            ],
        ]);
    }
}
