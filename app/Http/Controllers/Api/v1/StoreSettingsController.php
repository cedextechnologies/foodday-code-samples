<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\StoreHour;
use App\Http\Controllers\Controller;
use App\Services\StoreSettingsService;
use App\Http\Resources\StoreHourCollection;

class StoreSettingsController extends Controller
{
    public function __construct(StoreSettingsService $storeSettingsService)
    {
        $this->storeSettingsService = $storeSettingsService;
    }

    public function getStoreSettings()
    {
        return $this->storeSettingsService->getStoreSettings();
    }

    public function workingHours()
    {
        return new StoreHourCollection(StoreHour::all());
    }
}
