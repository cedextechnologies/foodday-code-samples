<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Cart;
use App\Models\Coupon;
use Illuminate\Http\Request;
use App\Services\CartService;
use App\Http\Controllers\Controller;
use App\Http\Resources\CouponCollection;

class CartController extends Controller
{
    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    /**
     * Get catr token.
     *
     * @return token
     */
    public function getCartToken()
    {
        return $this->cartService->getCartToken();
    }

    public function store(Request $request, $token)
    {
        return $this->cartService->updateCart($request, $token);
    }

    public function updateQuantity(Request $request, $token)
    {
        return $this->cartService->updateQuantity($request, $token);
    }

    public function show(Request $request, $token)
    {
        return $this->cartService->getCart($token);
    }

    public function destroy(Request $request, $token)
    {
        $cart = Cart::where('token', $token)->first();

        if (empty($cart)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'Invalid cart token',
                ],
            ]);
        }

        return $this->cartService->removeCartItem($request, $cart);
    }

    public function showCoupons()
    {
        return new CouponCollection(Coupon::active()->get());
    }

    public function applyCoupon(Request $request, $token)
    {
        return $this->cartService->applyCoupon($request, $token);
    }

    public function removeCoupon(Request $request, $token)
    {
        return $this->cartService->removeCoupon($request, $token);
    }

    public function checkout(Request $request, $token)
    {
        return $this->cartService->checkout($request, $token);
    }

    public function makePayment(Request $request, $token)
    {
        return $this->cartService->makePayment($request, $token);
    }
}
