<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\City;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\CityCollection;

class CityController extends Controller
{
    public function index(Request $request)
    {
        return new CityCollection(City::active()->get());
    }
}
