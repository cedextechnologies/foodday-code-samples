<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Faq;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\FaqCollection;

class FaqController extends Controller
{
    public function index(Request $request)
    {
        return new FaqCollection(Faq::active()->get());
    }
}
