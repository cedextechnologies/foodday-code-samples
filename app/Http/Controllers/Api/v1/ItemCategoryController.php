<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Item;
use App\Models\ItemCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;
use App\Http\Resources\ItemCategoryCollection;

class ItemCategoryController extends Controller
{
    public function index(Request $request)
    {
        return new ItemCategoryCollection(ItemCategory::active()->get());
    }

    public function show(Request $request, $category)
    {
        $category = ItemCategory::where('id', $category)->first();

        if (empty($category)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'category not found',
                ],
            ]);
        }

        return new ItemCollection(Item::active()->where('category_id', $category->id)->get());
    }
}
