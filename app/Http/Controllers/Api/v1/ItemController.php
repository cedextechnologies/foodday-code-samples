<?php

namespace App\Http\Controllers\Api\v1;

use App\Models\Item;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Http\Controllers\Controller;
use App\Http\Resources\ItemCollection;

class ItemController extends Controller
{
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function index(Request $request)
    {
        return new ItemCollection(Item::active()->with('itemCategory')->get());
    }

    public function featured(Request $request)
    {
        return new ItemCollection(Item::active()->featured()->with('itemCategory')->get());
    }

    public function show($item)
    {
        return $this->itemService->show($item);
    }
}
