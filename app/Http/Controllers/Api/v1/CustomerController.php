<?php

namespace App\Http\Controllers\Api\v1;

use Auth;
use Illuminate\Http\Request;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;

class CustomerController extends Controller
{
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function store(Request $request)
    {
        return $this->customerService->store($request);
    }

    public function update(Request $request)
    {
        $customer = Auth::guard('customer')->user();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'account not found',
                ],
            ]);
        }

        return $this->customerService->updateProfile($request, $customer);
    }

    public function profile()
    {
        return $this->customerService->getProfile();
    }

    public function changePassword(Request $request)
    {
        $customer = Auth::guard('customer')->user();

        if (empty($customer)) {
            return response()->json([
                'status' => 'error',
                'data' => [
                    'message' => 'account not found',
                ],
            ]);
        }

        return $this->customerService->changePassword($request, $customer);
    }

    public function forgot(Request $request)
    {
        return $this->customerService->forgot($request);
    }

    public function reset(Request $request)
    {
        return $this->customerService->reset($request);
    }

    public function subscribe(Request $request)
    {
        return $this->customerService->subscribe($request);
    }
}
