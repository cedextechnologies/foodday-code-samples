<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\DeliveryZoneService;

class DeliveryZoneController extends Controller
{
    public function __construct(DeliveryZoneService $deliveryZoneService)
    {
        $this->deliveryZoneService = $deliveryZoneService;
    }

    /**
     * Check delivery zone by zipcode.
     *
     * @param string $zipcode
     *
     * @return JSON
     */
    public function isDeliveryAvailable(Request $request)
    {
        return $this->deliveryZoneService->isDeliveryAvailable($request);
    }
}
