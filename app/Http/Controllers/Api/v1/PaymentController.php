<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PaypalPaymentService;
use App\Services\StripePaymentService;

class PaymentController extends Controller
{
    public function __construct(StripePaymentService $stripePaymentService, PaypalPaymentService $paypalPaymentService)
    {
        $this->stripePaymentService = $stripePaymentService;
        $this->paypalPaymentService = $paypalPaymentService;
    }

    public function stripePayment(Request $request, $token)
    {
        return $this->stripePaymentService->makePayment($request, $token);
    }

    /**
     * Get redirect url.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function paypalPayment(Request $request, $token)
    {
        $successUrl = 'api/v1/paypal-callback-success/'.$token;
        $cancelUrl = 'api/v1/paypal-callback-cancel/'.$token;

        return $this->paypalPaymentService->getPaypalUrl($request, $token, $successUrl, $cancelUrl);
    }

    /**
     * Make Payment.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function makePaypalPayment(Request $request, $token)
    {
        return $this->paypalPaymentService->makePayment($request, $token);
    }

    /**
     * Cancel Payment.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function cancelPaypalPayment(Request $request, $token)
    {
        return $this->paypalPaymentService->cancelPayment($request, $token);
    }
}
