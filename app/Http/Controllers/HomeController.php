<?php

namespace App\Http\Controllers;

use Auth;
use App\Models\Item;
use App\Models\ItemCategory;
use App\Services\DashboardService;

class HomeController extends Controller
{
    public function __construct(DashboardService $dashboardService)
    {
        $this->dashboardService = $dashboardService;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::guard('admin')->check()) {
            return redirect()->route('admin.dashboard');
        }

        return view('customer.home', [
        'categories' => ItemCategory::active()->get(),
        'featuredItems' => Item::active()->featured()->get(),
    ]);
    }
}
