<?php

namespace App\Http\Controllers\Customer;

use Auth;
use App\Models\Cart;
use App\Models\Item;
use App\Traits\CartTrait;
use Illuminate\Http\Request;
use App\Services\CartService;
use App\Http\Controllers\Controller;

class CartController extends Controller
{
    use CartTrait;

    public function __construct(CartService $cartService)
    {
        $this->cartService = $cartService;
    }

    public function index(Request $request)
    {
        $cartTotals=$discount='';

        $token = $request->session()->get('web_cart_token', 'default');
        // $zone = $request->session()->all('deliveryZone');
        // dd($zone);

        $cart = Cart::where('token', $token)->first();

        if ($cart) {
            $cartTotals = $this->getCartTotals($cart->items);
            $discount = $this->getCouponDiscount($cart, $cartTotals['order_total']);
        }

        return view('customer.cart.index', compact('cart', 'cartTotals', 'discount'));
    }

    public function update(Request $request, Item $item)
    {
        $token = $this->cartService->getWebCartToken();

        return $this->cartService->updateCart($request, $token);
    }

    public function proceed(Request $request)
    {
        $location = [];

        $token = $request->session()->get('web_cart_token', 'default');

        $cart = Cart::where('token', $token)->first();

        if (! empty(Auth::user())) {
            $location = getCoordinates(Auth::user()->zipcode);
        }

        if ($cart) {
            $cartTotals = $this->getCartTotals($cart->items);
            $discount = $this->getCouponDiscount($cart, $cartTotals['order_total']);
        }

        return view('customer.cart.checkout', compact('cart', 'cartTotals', 'discount', 'location'));
    }

    public function setLocation(Request $request)
    {
        $location = getCoordinates(request('zipcode'));
        if (! empty($location['latitude']) && ! empty($location['latitude'])) {
            return response()->json([
                'status' => 'success',
                'data' => [
                    'message' => 'Delivery location updated successfully.',
                    'latitude' => $location['latitude'],
                    'longitude' => $location['longitude'],
                ],
            ]);
        }

        return response()->json([
            'status' => 'error',
            'data' => [
                'message' => 'Invalid Zipcode.',
            ],
        ]);
    }

    public function checkout(Request $request)
    {
        $token = $request->session()->get('web_cart_token', 'default');

        return $this->cartService->checkout($request, $token);
    }

    public function delete(Request $request, Cart $cart)
    {
        return $this->cartService->removeCartItem($request, $cart);
    }

    public function updateQuantity(Request $request, $token)
    {
        return $this->cartService->updateQuantity($request, $token);
    }

    public function applyCoupon(Request $request, $token)
    {
        return $this->cartService->applyCoupon($request, $token);
    }

    public function removeCoupon(Request $request, $token)
    {
        return $this->cartService->removeCoupon($request, $token);
    }
}
