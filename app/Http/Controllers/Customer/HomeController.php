<?php

namespace App\Http\Controllers\Customer;

use App\Models\Item;
use App\Models\ItemCategory;
use App\Http\Controllers\Controller;

class HomeController extends Controller
{
    public function index()
    {
        return view('customer.home', [
            'categories' => ItemCategory::active()->get(),
            'featuredItems' => Item::active()->featured()->get(),
        ]);
    }
}
