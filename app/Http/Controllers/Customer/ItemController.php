<?php

namespace App\Http\Controllers\Customer;

use App\Models\Item;
use App\Models\ItemCategory;
use Illuminate\Http\Request;
use App\Services\ItemService;
use App\Http\Controllers\Controller;

class ItemController extends Controller
{
    public function __construct(ItemService $itemService)
    {
        $this->itemService = $itemService;
    }

    public function index(Request $request)
    {
        return view('customer.items.index', [
            'items' => Item::active()->get(),
            'categories' => ItemCategory::active()->get(),
        ]);
    }

    public function items($id)
    {
        $itemCategory = ItemCategory::findOrFail($id);

        return view('customer.items.index', [
           'items' => Item::active()->where('category_id', $itemCategory->id)->get(),
           'categories' => ItemCategory::active()->get(),
           'itemCategory' => $itemCategory,
        ]);
    }

    public function show(Request $request, Item $item)
    {
        $html = view('customer.items.show', compact('item'))->render();

        return response()->json([
            'status' => 'success',
            'html' => $html,
        ]);
    }
}
