<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\ReservationService;

class ReservationController extends Controller
{
    public function __construct(ReservationService $reservationService)
    {
        $this->reservationService = $reservationService;
    }

    public function create(Request $request)
    {
        return view('customer.reservation.create');
    }

    public function reserveTable(Request $request)
    {
        return $this->reservationService->store($request);
    }
}
