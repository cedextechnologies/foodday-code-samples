<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Services\ContactService;
use App\Http\Controllers\Controller;

class ContactController extends Controller
{
    public function __construct(ContactService $service)
    {
        $this->service = $service;
    }

    public function index(Request $request)
    {
        return view('customer.contact.index');
    }

    public function contact(Request $request)
    {
        return $this->service->contact($request);
    }
}
