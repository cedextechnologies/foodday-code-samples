<?php

namespace App\Http\Controllers\Customer;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\PaypalPaymentService;
use App\Services\StripePaymentService;

class PaymentController extends Controller
{
    public function __construct(StripePaymentService $stripePaymentService, PaypalPaymentService $paypalPaymentService)
    {
        $this->stripePaymentService = $stripePaymentService;
        $this->paypalPaymentService = $paypalPaymentService;
    }

    public function stripePayment(Request $request, $token)
    {
        $order = Order::where('payment_token', $token)->first();
        $response = $this->stripePaymentService->makePayment($request, $token);

        if ('success' == json_decode($response->getContent())->status) {
            $request->session()->forget('web_cart_token');

            return redirect()->route('customer.order.confirmation', $order);
        } else {
            return redirect()->route('customer.cart.index');
        }
    }

    /**
     * Get redirect url.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function paypalPayment(Request $request, $token)
    {
        $successUrl = route('cart.paypal.success', $token);
        $cancelUrl = route('cart.paypal.cancel', $token);

        return $this->paypalPaymentService->getPaypalUrl($request, $token, $successUrl, $cancelUrl);
    }

    /**
     * Make Payment.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function makePaypalPayment(Request $request, $token)
    {
        return $this->paypalPaymentService->makePayment($request, $token);
    }

    /**
     * Cancel Payment.
     *
     * @param \Illuminate\Http\Request $request, $token
     *
     * @return json array
     */
    public function cancelPaypalPayment(Request $request, $token)
    {
        return $this->paypalPaymentService->cancelPayment($request, $token);
    }
}
