<?php

namespace App\Http\Controllers\Customer;

use App\Models\Order;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    public function confirmation(Request $request, Order $order)
    {
        return view('customer.order.confirmation', compact('order'));
    }

    public function show(Request $request, Order $order)
    {
        $html = view('customer.order.show', compact('order'))->render();

        return response()->json([
            'status' => 'success',
            'data' => [
                'html' => $html,
            ],
        ]);
    }
}
