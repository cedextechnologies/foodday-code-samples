<?php

namespace App\Http\Controllers\Customer;

use Auth;
use Illuminate\Http\Request;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function index()
    {
        return view('customer.account.index', [
            'customer' => Auth::user(),
            'orders' => Auth::user()->orders->reverse(),
        ]);
    }

    public function update(Request $request)
    {
        $customer = Auth::user();

        return $this->customerService->updateProfile($request, $customer);
    }

    public function changePassword(Request $request)
    {
        $customer = Auth::user();

        return $this->customerService->changePassword($request, $customer);
    }
}
