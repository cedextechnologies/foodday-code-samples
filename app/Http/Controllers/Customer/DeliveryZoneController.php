<?php

namespace App\Http\Controllers\Customer;

use Auth;
use App\Models\DeliveryZone;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\DeliveryZoneService;

class DeliveryZoneController extends Controller
{


    public function getByGeo(Request $request, $longitude, $latitude)
    {
      $deliveryZone = DeliveryZone::whereRaw('ST_Contains(geometry, GeomFromText(?))', ["POINT($latitude $longitude)"])->where('delivery_available', 1)->first();


        return response()->json([
            'status' => 'true',
            'data' => [
                'available' => ($deliveryZone)? 1 : 0,
                'zone' => ($deliveryZone)? $deliveryZone->id : '',
            ],    
        ]);

    }

    public function update(Request $request, DeliveryZoneService $deliveryZoneService)
    {
        return $deliveryZoneService->updateDeliveryLocation($request);
    }

}
