<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;

class ForgotPasswordController extends Controller
{
    public function __construct(CustomerService $customerService)
    {
        $this->customerService = $customerService;
    }

    public function forgot(Request $request)
    {
        return view('auth.passwords.email');
    }

    public function sendOtp(Request $request)
    {
        return $this->customerService->forgot($request);
    }

    public function reset(Request $request)
    {
        return $this->customerService->reset($request);
    }
}
