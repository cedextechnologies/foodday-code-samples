<?php

namespace App\Http\Controllers\Customer;

use App\Models\Faq;
use App\Http\Controllers\Controller;

class FaqController extends Controller
{
    public function index()
    {
        return view('customer.faq.index', [
            'faqs' => Faq::active()->get(),
        ]);
    }
}
