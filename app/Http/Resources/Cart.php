<?php

namespace App\Http\Resources;

use App\Models\CartItem;
use App\Traits\CartTrait;
use Illuminate\Http\Resources\Json\JsonResource;

class Cart extends JsonResource
{
    use CartTrait;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $cartItems = CartItem::where('cart_id', $this->id)->with(['item', 'size'])->orderBy('id', 'desc')->get();

        $cartTotals = $this->getCartTotals($cartItems);

        $discount = $this->getCouponDiscount($this, $cartTotals['order_total']);

        return [
            'items' => new CartItemCollection($cartItems),
            'subtotal' => format_currency($cartTotals['subtotal']),
            'tax' => $cartTotals['tax'].'%',
            'tax_amount' => format_currency($cartTotals['tax_amount']),
            'coupon_applied' => $this->coupon_applied,
            'delivery_charges' => format_currency($cartTotals['delivery_charge']),
            'order_total' => format_currency($cartTotals['order_total']),
            'coupon_code' => $this->code,
            'coupon_discount' => format_currency($discount),
            'grand_total' => format_currency($cartTotals['order_total'] - $discount),
        ];
    }
}
