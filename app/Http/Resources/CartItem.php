<?php

namespace App\Http\Resources;

use App\Traits\CartTrait;
use App\Models\CartItemModifier;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItem extends JsonResource
{
    use CartTrait;

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        $price = ! empty($this->size) ? $this->size->price : $this->item->price;

        return [
            'id' => $this->id,
            'row_id' => $this->row_id,
            'name' => $this->item->name,
            'quantity' => $this->quantity,
            'image' => asset('storage/'.$this->item->image),
            'size' => ! empty($this->size) ? $this->size->size->size : '',
            'item_price' => format_currency($price),
            'price' => format_currency($this->getPrice()),
            'instruction' => $this->instruction,
            'modifiers' => new CartItemModifierCollection(CartItemModifier::where('cart_item_id', $this->id)->get()),
            'item_total' => format_currency($this->getItemTotals($this)),
        ];
    }
}
