<?php

namespace App\Http\Resources;

use App\Models\ItemSize;
use App\Models\ItemModifier;
use Illuminate\Http\Resources\Json\JsonResource;

class Item extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'category' => $this->itemCategory->name,
            'category_id' => $this->category_id,
            'description' => $this->description,
            'image' => asset('storage/'.$this->image),
            'price' => format_currency($this->price),
            'has_different_size' => $this->has_different_size,
            'has_modifiers' => $this->modifiers->isNotEmpty() ? 1 : 0,
            'item_type' => $this->getItemType(),
            'sizes' => new ItemSizeCollection(ItemSize::where('item_id', $this->id)->with('size')->get()),
            'modifiers' => new ItemModifierCollection(ItemModifier::where('item_id', $this->id)->get()),
        ];
    }
}
