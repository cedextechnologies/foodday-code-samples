<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class StoreHour extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'day' => getDayString($this->day),
            'open_hour' => $this->open_hour,
            'close_hour' => $this->close_hour,
            'status' => $this->status,
        ];
    }
}
