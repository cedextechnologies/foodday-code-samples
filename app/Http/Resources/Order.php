<?php

namespace App\Http\Resources;

use App\Models\OrderItem;
use Illuminate\Http\Resources\Json\JsonResource;

class Order extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'order_reference' => $this->order_reference,
            'items' => new OrderItemCollection(OrderItem::where('order_id', $this->id)->get()),
            'item_total' => format_currency($this->subtotal),
            'tax' => $this->tax.'%',
            'tax_amount' => format_currency($this->tax_amount),
            'coupon_applied' => '' != $this->coupon_code ? 1 : 0,
            'coupon_discount' => format_currency($this->coupon_discount),
            'delivery_charges' => format_currency($this->delivery_charge),
            'payed_amount' => format_currency($this->payed_amount),
            'status' => $this->getStatus(),
            'date' => getDateString($this->created_at),
        ];
    }
}
