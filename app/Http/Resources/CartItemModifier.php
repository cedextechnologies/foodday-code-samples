<?php

namespace App\Http\Resources;

use App\Models\CartItemModifierDetail;
use Illuminate\Http\Resources\Json\JsonResource;

class CartItemModifier extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->itemModifier->title,
            'modifier_items' => new CartItemModifierDetailCollection(CartItemModifierDetail::where('cart_item_modifier_id', $this->id)->get()),
        ];
    }
}
