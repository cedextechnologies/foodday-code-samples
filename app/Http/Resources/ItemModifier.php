<?php

namespace App\Http\Resources;

use App\Models\ItemModifierDetail;
use Illuminate\Http\Resources\Json\JsonResource;

class ItemModifier extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'title' => $this->title,
            'minimum' => $this->minimum,
            'maximum' => $this->maximum,
            'modifier_items' => new ItemModifierDetailCollection(ItemModifierDetail::where('modifier_id', $this->id)->get()),
        ];
    }
}
