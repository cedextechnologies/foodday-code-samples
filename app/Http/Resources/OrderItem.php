<?php

namespace App\Http\Resources;

use App\Models\OrderItemModifier;
use Illuminate\Http\Resources\Json\JsonResource;

class OrderItem extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->item->name,
            'image' => asset('storage/'.$this->item->image),
            'quantity' => $this->quantity,
            'modifiers' => new OrderItemModifierCollection(OrderItemModifier::where('order_item_id', $this->id)->get()),
            'price' => format_currency($this->price),
            'item_total' => format_currency($this->item_total),
            'item_price' => format_currency($this->subtotal),
        ];
    }
}
