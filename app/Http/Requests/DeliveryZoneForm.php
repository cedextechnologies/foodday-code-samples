<?php

namespace App\Http\Requests;

use Auth;
use App\Rules\ValidZone;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class DeliveryZoneForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'polygondata' => ['required', new ValidZone()],
                'name' => 'required|max:255',
                'delivery_available' => 'required|in:1,0',
            ];
        }
    }
}
