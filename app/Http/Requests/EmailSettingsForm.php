<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class EmailSettingsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'from_email' => 'required|email|max:255',
                'from_email_name' => 'required|max:255',
                'smtp_host' => 'required|max:255',
                'smtp_port' => 'sometimes|max:255',
                'smtp_username' => 'required|max:255',
                'smtp_password' => 'required|max:255',
                'smtp_password' => 'required|max:255',
            ];
        }
    }
}
