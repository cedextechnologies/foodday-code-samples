<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class UserForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'name' => 'required|max:255',
                'password' => 'required|max:255',
                'confirm_password' => 'same:password',
                'email' => 'bail|required|email|max:255|unique:users,email',
                'status' => 'required|in:1,0',
            ];
        }
    }
}
