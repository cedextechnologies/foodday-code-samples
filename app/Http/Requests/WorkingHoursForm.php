<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class WorkingHoursForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'open_hour_monday' => 'required|max:255',
                'close_hour_monday' => 'required|max:255',
                'open_hour_tuesday' => 'required|max:255',
                'close_hour_tuesday' => 'required|max:255',
                'open_hour_wednesday' => 'required|max:255',
                'close_hour_wednesday' => 'required|max:255',
                'open_hour_thursday' => 'required|max:255',
                'close_hour_thursday' => 'required|max:255',
                'open_hour_friday' => 'required|max:255',
                'close_hour_friday' => 'required|max:255',
                'open_hour_saturday' => 'required|max:255',
                'close_hour_saturday' => 'required|max:255',
                'open_hour_sunday' => 'required|max:255',
                'close_hour_sunday' => 'required|max:255',
            ];
        }
    }
}
