<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class CommonModifierForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'title' => 'required|string|max:80',
                'minimum' => 'required',
                'maximum' => 'required',
            ];
        }

        if ('PUT' == Request::method()) {
            return [
                'title' => 'required|string|max:80',
                'minimum' => 'required',
                'maximum' => 'required',
            ];
        }
    }

    public function messages()
    {
        return [
            'title.required' => 'Modifier title is required',
            'minimum.required' => 'Please enter a minimum amount',
            'maximum.required' => 'Please enter a maximum amount',
        ];
    }
}
