<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class FaqForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'title' => 'required|max:255',
                'description' => 'required',
                'status' => 'required|in:1,0',
            ];
        }

        if ('PUT' == Request::method()) {
            return [
                'title' => 'required|max:255',
                'description' => 'required',
                'status' => 'required|in:1,0',
            ];
        }
    }
}
