<?php

namespace App\Http\Requests;

use Auth;
use App\Rules\CurrentPasswordValidator;
use Illuminate\Foundation\Http\FormRequest;

class PasswordForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        return [
            'current_password' => [
                'required',
                new CurrentPasswordValidator(),
            ],
            'password' => 'required|string|min:6|confirmed',
        ];
    }
}
