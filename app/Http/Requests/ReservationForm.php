<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ReservationForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'name' => 'required|max:255',
                'email' => 'required|email',
                'mobile' => 'sometimes|max:255',
                'status' => 'required|in:1,0,2',
                'booking_time' => 'required:date',
                'party_size' => 'required|numeric',
                'extra_notes' => 'sometimes|max:255',
            ];
        }
    }
}
