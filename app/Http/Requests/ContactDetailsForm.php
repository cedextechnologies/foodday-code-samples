<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ContactDetailsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ('PUT' == Request::method()) {
            return [
                'contact_person' => 'required|max:255',
                'contact_number' => 'required|max:255',
                'contact_email' => 'required|max:255|email',
                'contact_address' => 'required',
            ];
        }
    }
}
