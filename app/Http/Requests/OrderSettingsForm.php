<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderSettingsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'minimum_order' => 'required|numeric|min:1',
                'tax' => 'required|numeric',
                'delivery_charges' => 'required|numeric',
                'allow_pre_order' => 'required|in:1,0',
            ];
        }
    }
}
