<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class CouponForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'code' => 'required|max:255',
                'discount_type' => 'required|in:1,0',
                'discount' => 'required|numeric',
                'min_amount' => 'required|numeric',
                'usage_limit' => 'required|numeric',
                //'discount_per_customer' => 'required|numeric|max:255',
                'status' => 'required|in:1,0',
                'start_date' => 'required|date',
                'end_date' => 'required|date|after:start_date',
            ];
        }

        if ('PUT' == Request::method()) {
            return [
                'code' => 'required|max:255',
                'discount_type' => 'required|in:1,0',
                'discount' => 'required|numeric',
                'min_amount' => 'required|numeric',
                'usage_limit' => 'required|numeric',
                //'discount_per_customer' => 'required|numeric|max:255',
                'status' => 'required|in:1,0',
                'start_date' => 'required|date',
                'end_date' => 'required|date|after:start_date',
            ];
        }
    }
}
