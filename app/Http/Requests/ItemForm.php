<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class ItemForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('POST' == Request::method()) {
            return [
                'name' => 'required|string|max:80',
                'description' => 'required|string',
                'image' => 'required|image|max:5000',
                'item_type' => 'required|in:1,2,3',
                'has_different_size' => 'required|in:1,0',
                'price' => 'required_if:has_different_size,0',
                'category_id' => 'required|exists:item_categories,id',
                'is_featured' => 'required|in:1,0',
                'status' => 'required|in:1,0',
            ];
        }

        if ('PUT' == Request::method()) {
            return [
                'name' => 'required|string|max:80',
                'description' => 'required|string',
                'image' => 'sometimes|image|max:5000',
                'item_type' => 'required|in:1,2,3',
                'has_different_size' => 'required|in:1,0',
                'price' => 'required_if:has_different_size,0',
                'category_id' => 'required|exists:item_categories,id',
                'is_featured' => 'required|in:1,0',
                'status' => 'required|in:1,0',
            ];
        }
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Item category is required',
            'category_id.exists' => 'Please select a valid category',
            'price.required_if' => 'Item price is required',
        ];
    }
}
