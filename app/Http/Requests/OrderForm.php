<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class OrderForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'name' => 'sometimes|required|max:255',
                'email' => 'sometimes|required|email',
                'mobile' => 'sometimes|max:255',
                'status' => 'sometimes|required|in:1,0',
                'booking_time' => 'sometimes|required:date',
                'party_size' => 'sometimes|required|numeric',
                'extra_notes' => 'string|nullable|max:255',
            ];
        }
    }
}
