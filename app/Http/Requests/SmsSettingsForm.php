<?php

namespace App\Http\Requests;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class SmsSettingsForm extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user = Auth::user();

        if ('PUT' == Request::method()) {
            return [
                'twilio_from_email' => 'sometimes|nullable|email|max:255',
                'twilio_from_name' => 'sometimes|max:255',
                'twilio_smtp_host' => 'sometimes|max:255',
                'order_placed_admin_sms' => 'sometimes',
                'order_placed_customer_sms' => 'sometimes',
                'order_confirmed_customer_sms' => 'sometimes',
                'order_cancelled_customer_sms' => 'sometimes',
                'order_delivered_customer_sms' => 'sometimes',
            ];
        }
    }
}
