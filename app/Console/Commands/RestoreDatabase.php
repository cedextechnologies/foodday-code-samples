<?php

namespace App\Console\Commands;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use App\Services\UserService;
use Illuminate\Console\Command;
use App\Jobs\AddPredefinedQuestions;

class RestoreDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'restore:database';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Resore demo data of database';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        DB::unprepared(file_get_contents(asset('database/$%%cXC=6udM7z@p3XE9F.sql')));
        File::copyDirectory(storage_path().'/app/public/items-bkp', storage_path().'/app/public/items');
        File::copyDirectory(storage_path().'/app/public/item-categories-bkp', storage_path().'/app/public/item-categories');
        File::copyDirectory(storage_path().'/app/public/store-image-bkp', storage_path().'/app/public/store-image');
    }
}
