<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot()
    {
        config(['app.timezone' => getTimeZone()]);
    }

    /**
     * Register any application services.
     */
    public function register()
    {
    }
}
