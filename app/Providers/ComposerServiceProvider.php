<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     */
    public function boot()
    {
        View::composer(
            '*', 'App\Http\ViewComposers\ProfileComposer'
        );

        View::composer(
            '*', 'App\Http\ViewComposers\CartComposer'
        );

        View::composer(
            '*', 'App\Http\ViewComposers\NavigationComposer'
        );

        View::composer(
            '*', 'App\Http\ViewComposers\FooterComposer'
        );
    }

    /**
     * Register the application services.
     */
    public function register()
    {
    }
}
