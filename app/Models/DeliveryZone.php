<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DeliveryZone extends Model
{
    public $fillable = [
        'geometry',
        'name',
        'pincode',
        'delivery_available',
        'delivery_charge',
        'default_delivery_charge'
    ];

    public function customDeliveryFees()
    {
    	return $this->hasMany('App\Models\CustomDeliveryFee', 'delivery_zone_id');
    }
}
