<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemModifierDetail extends Model
{
    protected $fillable = [
        'modifier_id',
        'name',
        'price',
    ];

    public function itemModifier()
    {
        return $this->belongsTo('App\Models\itemModifier', 'modifier_id');
    }
}
