<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MainCource extends Model
{
    protected $fillable = [
        'name',
        'status',
    ];
}
