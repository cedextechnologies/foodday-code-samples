<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GuestCustomer extends Model
{
    public function customer()
    {
        return $this->hasOne('App\Models\Customer');
    }
}
