<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $fillable = [
        'order_reference',
        'customer_id',
        'tax',
        'tax_amount',
        'delivery_charge',
        'subtotal',
        'payed_amount',
        'payment_status',
        'payment_mode',
        'payment_token',
        'checkout_type',
        'is_preorder',
        'preorder_date',
        'coupon_code',
        'coupon_discount',
        'order_date',
        'redirect_url',
        'status',
    ];

    public function coupon()
    {
        return $this->hasOne('App\Models\Coupon');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function address()
    {
        return $this->belongsTo('App\Models\OrderAddress', 'address_id');
    }

    public function orderItems()
    {
        return $this->hasMany('App\Models\OrderItem');
    }

    public function isPreOrder()
    {
        return 1 == $this->is_preorder;
    }

    public function getStatus()
    {
        switch ($this->status) {
            case 0: return 'Pending';
            case 1: return 'Confirmed';
            case 2: return 'Cancelled';
            case 3: return 'Delivered';
            case 4: return 'Out for delivery';
        }
    }

    public function getStatusClass()
    {
        switch ($this->status) {
            case 0: return 'badge badge-default';
            case 1: return 'badge badge-primary';
            case 2: return 'badge badge-danger';
            case 3: return 'badge badge-success';
            case 4: return 'badge badge-success';
        }
    }

    public function paymentMethod()
    {
        switch ($this->payment_mode) {
            case 1: return 'Cash';
            case 2: return 'Stripe';
            case 3: return 'Paypal';
        }
    }

    public function paymentStatus()
    {
        if (1 == $this->payment_mode) {
            if (3 == $this->status) {
                return 'Paid';
            }

            return 'Pending';
        } else {
            if (1 == $this->paymant_status) {
                return 'Paid';
            }

            return 'Failed';
        }
    }

    public function statusBadge()
    {
        switch ($this->status) {
            case 0: return 'badge badge-secondary';
            case 1: return 'badge badge-warning';
            case 2: return 'badge badge-danger';
            case 3: return 'badge badge-success';
            case 4: return 'badge badge-info';
        }
    }

    public function rowColor()
    {
        switch ($this->status) {
            case 0: return 'rgb(236, 236, 236)';
            case 1: return 'rgb(255, 208, 193)';
            case 2: return 'rgb(255, 182, 182)';
            case 3: return 'rgba(40, 167, 69, 0.3)';
            case 4: return 'rgba(0, 243, 255, 0.3)';
        }
    }

    public function checkoutType()
    {
        switch ($this->checkout_type) {
            case '1':
            return 'Delivery';
            break;

            case '2':
            return 'Pickup';
            break;
        }
    }

    public function getPrice()
    {
        $modifierPrice = 0;

        foreach ($this->modifiers as $modifier) {
            $modifierPrice += $modifier->itemModifierDetail->price;
        }

        return $modifierPrice + $this->price;
    }
}
