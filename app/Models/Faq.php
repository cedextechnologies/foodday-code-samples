<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Faq extends Model
{
    public $fillable = [
        'title',
        'description',
        'status',
    ];

    public function scopeActive()
    {
        return $this->where('status', 1);
    }

    public function isActive()
    {
        return 1 == $this->status;
    }
}
