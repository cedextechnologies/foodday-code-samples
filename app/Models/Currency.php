<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    public $fillable = [
        'code',
        'name',
        'symbol',
        'flag_image',
        'status',
    ];
}
