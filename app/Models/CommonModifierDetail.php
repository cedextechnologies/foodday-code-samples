<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonModifierDetail extends Model
{
    protected $fillable = [
    	'common_modifier_id',
        'modifier_id',
        'name',
        'price',
    ];

    public function commonModifier()
    {
        return $this->belongsTo('App\Models\commonModifier', 'modifier_id');
    }
}
