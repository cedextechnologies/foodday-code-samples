<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $fillable = [
        'name',
        'status',
    ];

    public function customers()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function scopeActive()
    {
        return $this->where('status', 1);
    }

    public function isActive()
    {
        return 1 == $this->status;
    }
}
