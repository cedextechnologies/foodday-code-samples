<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    protected $table = 'cart';

    public $fillable = [
        'token',
        'coupon_applied',
        'order_reference',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\CartItem');
    }
}
