<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CommonModifier extends Model
{
    protected $fillable = [
        'item_id',
        'title',
        'minimum',
        'maximum',
    ];

    public function CommonModifierDetails()
    {
        return $this->hasMany('App\Models\CommonModifierDetail', 'common_modifier_id');
    }
}
