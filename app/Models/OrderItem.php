<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $fillable = [
        'order_id',
        'item_id',
        'quantity',
        'price',
        'item_total',
        'subtotal',
        'instruction',
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function modifiers()
    {
        return $this->hasMany('App\Models\OrderItemModifier', 'order_item_id');
    }

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }

    public function size()
    {
        return $this->hasOne('App\Models\OrderItemSize');
    }

    public function getSizeText()
    {
        if (! empty($this->size)) {
            return '('.$this->size->size->size.')';
        } else {
            return '';
        }
    }
}
