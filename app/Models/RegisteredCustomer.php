<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RegisteredCustomer extends Model
{
    public $fillable = [
        'email',
        'password',
        'password_reset_token',
        'token_created',
        'last_login',
        'status',
    ];

    public function customer()
    {
        return $this->hasOne('App\Models\Customer');
    }
}
