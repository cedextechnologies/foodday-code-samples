<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $fillable = [
        'name',
        'description',
        'price',
        'image',
        'item_type',
        'status',
        'is_featured',
        'has_different_size',
        'category_id',
    ];

    public function itemCategory()
    {
        return $this->belongsTo('App\Models\ItemCategory', 'category_id');
    }

    public function modifiers()
    {
        return $this->hasMany('App\Models\ItemModifier');
    }

    public function isFeatured()
    {
        return 1 == $this->is_featured ? 'Yes' : 'No';
    }

    public function isActive()
    {
        return 1 == $this->status;
    }

    public function getItemType()
    {
        switch ($this->item_type) {
            case 1:return '';
                    break;
            case 2:return '(Veggy)';
                    break;
            case 3:return '(Spicy)';
                    break;
        }
    }

    public function sizes()
    {
        return $this->hasMany('App\Models\ItemSize');
    }

    public function scopeActive()
    {
        return $this->where('status', 1);
    }

    public function scopeFeatured()
    {
        return $this->where('is_featured', 1);
    }

    public function itemCommonModifiers()
    {
        return $this->hasMany('App\Models\ItemCommonModifier');
    }
}
