<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PasswordReset extends Model
{
    public $fillable = [
        'email',
        'token',
        'created_at',
    ];

    public function customer()
    {
        return $this->hasOne('App\Models\Customer', 'email', 'email');
    }
}
