<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'pincode',
        'address_line_1',
        'address_line_2',
    ];
}
