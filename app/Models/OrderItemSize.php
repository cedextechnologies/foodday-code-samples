<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemSize extends Model
{
    protected $fillable = [
        'order_item_id',
        'size_id',
    ];

    public function orderItem()
    {
        return $this->hasOne('App\Models\OrderItem');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\Size')->withDefault();
    }
}
