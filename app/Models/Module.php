<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Module extends Model
{
    protected $fillable = [
        'name',
        'description',
        'parent',
        'show_on_menu',
        'menu_order',
        'status',
    ];
}
