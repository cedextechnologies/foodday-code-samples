<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    protected $fillable = [
        'code',
        'discount_type',
        'discount',
        'usage_limit',
        'used_count',
        'status',
        'start_date',
        'end_date',
        'min_amount',
    ];

    protected $dates = [
        'start_date',
        'end_date',
    ];

    public function orders()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function isActive()
    {
        return 1 == $this->status;
    }

    public function getDiscount()
    {
        // Fixed amount
        if ('0' == $this->discount_type) {
            $coupon_discount = 'Flat '.format_currency($this->discount).'Off';
        }
        // Percentage
        else {
            $coupon_discount = 'Flat '.$this->discount.'% Off';
        }

        return $coupon_discount;
    }

    public function scopeActive()
    {
        $this->where('status', 1);
    }
}
