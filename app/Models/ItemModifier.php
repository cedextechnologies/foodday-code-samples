<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemModifier extends Model
{
    protected $fillable = [
        'item_id',
        'title',
        'minimum',
        'maximum',
        'common_modifier_id'
    ];

    public function ItemModifierDetails()
    {
        return $this->hasMany('App\Models\ItemModifierDetail', 'modifier_id');
    }
}
