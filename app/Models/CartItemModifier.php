<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItemModifier extends Model
{
    public $fillable = [
        'cart_item_id',
        'item_modifier_id',
    ];

    public function itemModifier()
    {
        return $this->belongsTo('App\Models\ItemModifier');
    }
}
