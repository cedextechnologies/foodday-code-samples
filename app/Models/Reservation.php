<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    protected $fillable = [
        'name',
        'email',
        'mobile',
        'booking_date',
        'booking_time',
        'party_size',
        'extra_notes',
        'status',
    ];

    public function isActive()
    {
        return 1 == $this->status;
    }

    public function getStatus()
    {
        switch ($this->status) {
            case 0:
                return 'Pending';
                break;
            case 1:
                return 'Approved';
                break;
            case 2:
                return 'Rejected';
                break;
        }
    }

    public function getStatusClass()
    {
        switch ($this->status) {
            case 0:
                return 'default';
                break;
            case 1:
                return 'success';
                break;
            case 2:
                return 'danger';
                break;
        }
    }
}
