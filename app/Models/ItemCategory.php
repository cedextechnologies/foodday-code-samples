<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemCategory extends Model
{
    public $fillable = [
        'name',
        'description',
        'image',
        'status',
        'start_showing_menu_at',
        'stop_showing_menu_at',
    ];

    public function items()
    {
        return $this->hasMany('App\Models\Item', 'category_id');
    }

    public function isActive()
    {
        return 1 == $this->status;
    }

    public function scopeActive()
    {
        $this->where('status', 1);
    }
}
