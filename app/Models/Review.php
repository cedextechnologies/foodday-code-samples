<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = [
        'customer_id',
        'order_id',
        'rating',
        'comment',
        'status',
    ];

    public function order()
    {
        return $this->belongsTo('App\Models\Order');
    }

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function isActive()
    {
        return 1 == $this->status;
    }
}
