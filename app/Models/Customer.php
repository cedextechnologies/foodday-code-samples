<?php

namespace App\Models;

use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Customer extends Authenticatable implements JWTSubject
{
    use Notifiable;

    protected $guard_name = 'customer';

    protected $guard = 'web';

    public $fillable = [
        'customer_type',
        'first_name',
        'last_name',
        'zipcode',
        'door',
        'company',
        'email',
        'phone',
        'status',
        'password',
        'address_line_1',
        'address_line_2',
    ];

    public function guestCustomer()
    {
        return $this->belongsTo('App\Models\GuestCustomer');
    }

    public function orders()
    {
        return $this->hasMany('App\Models\Order');
    }

    public function isActive()
    {
        return 1 == $this->status;
    }

    public function getName()
    {
        return $this->first_name.' '.$this->last_name;
    }

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
}
