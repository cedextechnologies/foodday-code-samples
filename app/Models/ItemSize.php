<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ItemSize extends Model
{
    public $fillable = [
        'item_id',
        'size_id',
        'price',
    ];

    public function item()
    {
        return $this->hasOne('App\Models\Item');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\Size');
    }
}
