<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomDeliveryFee extends Model
{
    public $fillable = [
    	'order_amount',
    	'delivery_fee_amount'
    ];
}
