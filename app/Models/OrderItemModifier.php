<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class OrderItemModifier extends Model
{
    protected $fillable = [
        'order_item_id',
        'modifier_name',
        'item_name',
        'item_price',
        'item_modifier_details_id',
    ];

    public function orderItem()
    {
        return $this->belongsTo('App\Models\OrderItems');
    }
}
