<?php

namespace App\Models;

use App\Traits\CartTrait;
use Illuminate\Database\Eloquent\Model;

class CartItem extends Model
{
    use CartTrait;

    public $fillable = [
        'row_id',
        'cart_id',
        'item_id',
        'quantity',
        'instruction',
        'size_id',
    ];

    public function item()
    {
        return $this->belongsTo('App\Models\Item');
    }

    public function size()
    {
        return $this->belongsTo('App\Models\ItemSize');
    }

    public function modifiers()
    {
        return $this->hasManyThrough('App\Models\CartItemModifierDetail', 'App\Models\CartItemModifier');
    }

    public function getItemPrice()
    {
        return ! empty($this->size) ? $this->size->price : $this->item->price;
    }

    public function getPrice()
    {
        $modifierPrice = 0;
        $itemPrice = ! empty($this->size) ? $this->size->price : $this->item->price;

        foreach ($this->modifiers as $modifier) {
            $modifierPrice += $modifier->itemModifierDetail->price;
        }

        return $modifierPrice + $itemPrice;
    }

    public function getSizeText()
    {
        if (! empty($this->size_id)) {
            return '('.$this->size->size->size.')';
        } else {
            return '';
        }
    }
}
