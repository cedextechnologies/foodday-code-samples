<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CartItemModifierDetail extends Model
{
    public $fillable = [
        'cart_item_modifier_id',
        'item_modifier_detail_id',
    ];

    public function itemModifierDetail()
    {
        return $this->belongsTo('App\Models\ItemModifierDetail');
    }

    public function cartItemModifier()
    {
        return $this->belongsTo('App\Models\CartItemModifier');
    }
}
