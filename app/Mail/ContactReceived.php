<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ContactReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $contact;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($contact)
    {
        $this->contact = $contact;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('FROM_EMAIL'), config('app.name'));
        $this->replyTo($this->contact['email'], $this->contact['name']);

        return $this->subject('New Contact Received')->view('emails.contact.index');
    }
}
