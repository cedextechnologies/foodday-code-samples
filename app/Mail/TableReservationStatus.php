<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TableReservationStatus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $reservation;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('FROM_EMAIL'), config('app.name'));

        return $this->subject('Table Reservation Status')->view('emails.reservation.status');
    }
}
