<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class OrderStatus extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        if (1 == $this->order->status) {
            $this->from(env('FROM_EMAIL'), config('app.name'));

            return $this->subject('Order Confirmed')->view('emails.order.confirmed');
        } elseif (2 == $this->order->status) {
            $this->from(env('FROM_EMAIL'), config('app.name'));

            return $this->subject('Order Cancelled')->view('emails.order.cancelled');
        }
    }
}
