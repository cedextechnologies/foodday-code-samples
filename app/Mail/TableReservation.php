<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TableReservation extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $reservation;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($reservation)
    {
        $this->reservation = $reservation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('FROM_EMAIL'), config('app.name'));
        $this->replyTo($this->reservation['email'], $this->reservation['name']);

        return $this->subject('New Table Reservation')->view('emails.reservation.index');
    }
}
