<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewOrderReceived extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $order;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($order)
    {
        $this->order = $order;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('FROM_EMAIL'), config('app.name'));

        return $this->subject('New Order Placed')->view('emails.order.placed');
    }
}
