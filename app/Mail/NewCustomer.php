<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class NewCustomer extends Mailable implements ShouldQueue
{
    use Queueable, SerializesModels;

    /**
     * Contact array.
     *
     * @var [type]
     */
    public $user;

    /**
     * Create a new message instance.
     *
     * @param array $contact [description]
     */
    public function __construct($user)
    {
        $this->user = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $this->from(env('FROM_EMAIL'), config('app.name'));

        return $this->subject('Welcome to '.config('app.name'))->view('emails.user.welcome');
    }
}
